#  -*- coding: cp1252 german encoding -*-
import logging

import wx

from db.DatabaseManager import DatabaseManager as DBM
from db.DatabaseService import CategoryRuleService
from db.DbModelClasses import CategoryRule
from gui.bmp import images
from gui.dlg.CategoryRuleEditDialog import CategoryRuleEditDialog
from gui.listctrl.CategoryRuleListCtrl import CategoryRuleListCtrl
from resources import String

__author__ = "TobiasFochtmann"


class CategoryRuleConfigPanel(wx.Panel):
    """
    Control showing the rules to link a transaction automatically to a category
    """

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window
        """
        wx.Panel.__init__(self, parent)

        self._createControls()
        self._setToolTips()
        self._setLayout()
        self._bindEvents()
        self._initLoad()

    def _createControls(self):
        """
        Creates all used widgets
        """
        self.sizer = wx.GridBagSizer()
        self.list = CategoryRuleListCtrl(self)
        self.btn_add = wx.BitmapButton(self, bitmap=images.ADD_32.GetBitmap())
        self.btn_edit = wx.BitmapButton(self, bitmap=images.PENCIL_32.GetBitmap())
        self.btn_rem = wx.BitmapButton(self, bitmap=images.REMOVE.GetBitmap())

    def _setToolTips(self):
        """
        Set the tool tips of controls
        """
        self.btn_add.SetToolTip("Add a rule")
        self.btn_edit.SetToolTip("Edit the selected rule")
        self.btn_rem.SetToolTip("Remove the selected rule")

    def _setLayout(self):
        """
        Sets the layout of the widgets
        """
        self.sizer.Add(self.list, (0, 0), (3, 1), wx.EXPAND)
        self.sizer.Add(self.btn_add, (0, 1), (1, 1))
        self.sizer.Add(self.btn_edit, (1, 1), (1, 1))
        self.sizer.Add(self.btn_rem, (2, 1), (1, 1))
        self.sizer.AddGrowableCol(0)
        self.sizer.AddGrowableRow(2)

        self.SetSizerAndFit(self.sizer)

    def _bindEvents(self):
        """
        Binds events to actions
        """
        self.btn_add.Bind(wx.EVT_BUTTON, self.__onAddItem)
        self.btn_edit.Bind(wx.EVT_BUTTON, self.__onEditItem)
        self.btn_rem.Bind(wx.EVT_BUTTON, self.__onRemoveItem)

        self.list.Bind(wx.EVT_LIST_ITEM_SELECTED, self.__onSelectItem)
        self.list.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.__onSelectItem)
        self.list.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.__onEditItem)
        self.list.Bind(wx.EVT_KEY_UP, self.__onKeyUp)

    def _initLoad(self):
        """
        Initial load
        """
        self.btn_edit.Enable(False)
        self.btn_rem.Enable(False)

    def load(self):
        """
        Loads the data of the current database for category rules
        """
        self.list.setItems(CategoryRuleService.get())

    # ----- EVENTS -----

    # noinspection PyUnusedLocal
    def __onAddItem(self, evt):
        """
        Triggered when button for adding a rule is clicked
        :param evt:
        """
        dlg = CategoryRuleEditDialog(self)

        if dlg.ShowModal() == wx.ID_OK:
            self.load()

    # noinspection PyUnusedLocal
    def __onEditItem(self, evt):
        """
        Triggered when button for editing a rule is clicked
        :param evt:
        """
        item = self.list.GetFirstSelected()
        if item != -1:
            categoryRule = self.list.getItemByIndex(item)  # type: CategoryRule

            dlg = CategoryRuleEditDialog(self, categoryRule)

            if dlg.ShowModal() == wx.ID_OK:
                self.load()

    # noinspection PyUnusedLocal
    def __onRemoveItem(self, evt):
        """
        Triggered when button for removing a rule is clicked
        :param evt:
        """
        item = self.list.GetFirstSelected()
        if item != -1:
            categoryRule = self.list.getItemByIndex(item)  # type: CategoryRule

            dlg = wx.MessageDialog(self,
                                   message=String.DLG_MSG_RULECAT_DEL,
                                   caption=String.DLG_TITLE_RULECAT_DEL,
                                   style=wx.YES_NO)
            if dlg.ShowModal() == wx.ID_YES:
                DBM().delete(categoryRule)
                self.load()

    # noinspection PyUnusedLocal
    def __onSelectItem(self, evt):
        """
        Triggered when item is selected
        :param evt:
        """
        self.btn_edit.Enable(self.list.GetFirstSelected() != -1)
        self.btn_rem.Enable(self.list.GetFirstSelected() != -1)

    def __onKeyUp(self, evt):
        """
        Triggered when a key is pressed
        :param evt:
        :type evt: wx.TreeEvent
        """
        keyCode = evt.GetKeyCode()

        item = self.list.GetFirstSelected()
        if item != -1:
            if keyCode == wx.WXK_DELETE:
                wx.CallAfter(self.__onRemoveItem, evt)
            elif keyCode == wx.WXK_RETURN:
                wx.CallAfter(self.__onEditItem, evt)
            elif keyCode == wx.WXK_NUMPAD_ADD:
                wx.CallAfter(self.__onAddItem, evt)

    # ----- END EVENTS -----


if __name__ == "__main__":
    logging.getLogger("DB").setLevel(logging.DEBUG)

    app = wx.App()
    mainFrame = wx.Frame(None, size=(1000, 800))
    mainFrame.pnl = CategoryRuleConfigPanel(mainFrame)
    mainFrame.Show()
    mainFrame.pnl.load()
    app.MainLoop()
