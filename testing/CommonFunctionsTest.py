#  -*- coding: cp1252 german encoding -*-
import datetime
import unittest

import wx

import CommonFunctions
from db.DbModelClasses import Transaction


class CommonFunctionsTest(unittest.TestCase):

    def test_buildHashForTransaction_Ok(self):
        transaction = Transaction(iban="DE0123456789",
                                  bic="ABCDEFGH",
                                  date=datetime.datetime(year=2018, month=9, day=10),
                                  purpose="Testtransaction",
                                  src_dest="Source equals destination",
                                  amount=100.0)
        self.assertEqual(CommonFunctions.buildHashForTransaction(transaction),
                         "f06e96f3b0d273a21ad1a6a6e3ff7350",
                         "Hash generation failed")

    def test_buildHashForTransaction_EmptyDate(self):
        transaction = Transaction(amount=100.0)
        self.assertRaises(AssertionError,
                          CommonFunctions.buildHashForTransaction,
                          transaction)

    def test_buildHashForTransaction_EmptyAmount(self):
        transaction = Transaction(date=datetime.datetime(year=2018, month=9, day=10))
        self.assertRaises(AssertionError,
                          CommonFunctions.buildHashForTransaction,
                          transaction)

    def test_buildHashForTransaction_WrongType(self):
        self.assertRaises(AssertionError,
                          CommonFunctions.buildHashForTransaction,
                          None)

    def test_wxDateTimeToDatetime_Ok(self):
        wxdt = wx.DateTime().Set(10, 8, 2018)  # starts with month 0
        dt = datetime.datetime(year=2018, month=9, day=10)
        self.assertEqual(dt,
                         CommonFunctions.wxDateTimeToDatetime(wxdt),
                         "Conversion failed, {} converted, but is not {}".format(wxdt, dt))

    def test_wxDateTimeToDatetime_WrongType(self):
        self.assertRaises(AssertionError,
                          CommonFunctions.wxDateTimeToDatetime,
                          None)

    def test_datetimeToWxDateTime_Ok(self):
        wxdt = wx.DateTime().Set(10, 8, 2018)  # starts with month 0
        dt = datetime.datetime(year=2018, month=9, day=10)
        self.assertEqual(wxdt,
                         CommonFunctions.datetimeToWxDateTime(dt),
                         "Conversion failed, {} converted, but is not {}".format(dt, wxdt))

    def test_datetimeToWxDateTime_WrongType(self):
        self.assertRaises(AssertionError,
                          CommonFunctions.datetimeToWxDateTime,
                          None)

    def test_fromGermanDecimalToFloat_GermanDecimal(self):
        amountStr = "123,45"
        self.assertEqual(123.45,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_InterntlDecimal(self):
        amountStr = "123.45"
        self.assertEqual(123.45,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_GermanBigDecimal(self):
        amountStr = "1.123,45"
        self.assertEqual(1123.45,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_InterntlBigDecimal(self):
        amountStr = "1,123.45"
        self.assertEqual(1123.45,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_NegGermanDecimal(self):
        amountStr = "-123,45"
        self.assertEqual(-123.45,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_NegInterntlDecimal(self):
        amountStr = "-123.45"
        self.assertEqual(-123.45,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_NegGermanBigDecimal(self):
        amountStr = "-1.123,45"
        self.assertEqual(-1123.45,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_NegInterntlBigDecimal(self):
        amountStr = "-1,123.45"
        self.assertEqual(-1123.45,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_NothingToDo(self):
        amountStr = 123.45
        self.assertEqual(123.45,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_NothingToDo2(self):
        amountStr = 123
        self.assertEqual(123,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_NothingToDo3(self):
        amountStr = None
        self.assertEqual(None,
                         CommonFunctions.fromGermanDecimalToFloat(amountStr),
                         "Conversion failed.")

    def test_fromGermanDecimalToFloat_WrongType(self):
        amountStr = {}
        self.assertRaises(AssertionError,
                          CommonFunctions.fromGermanDecimalToFloat,
                          amountStr)


if __name__ == '__main__':
    unittest.main()
