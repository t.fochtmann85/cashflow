#  -*- coding: cp1252 german encoding -*-
from datetime import timedelta, datetime

import wx

from db.DatabaseService import CategoryService, TransactionService
from db.DbModelClasses import Category
from gui import GUIHelper
from gui.droptarget.CategoryDropTarget import CategoryDropTarget

__author__ = "Tobias Fochtmann"


class CategoryLeafListCtrl(wx.ListCtrl):
    """
    List control showing all leaf categories (not having children)
    """

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window 
        """
        wx.ListCtrl.__init__(self, parent,
                             size=(240, 200),
                             style=wx.LC_REPORT | wx.LC_NO_HEADER | wx.LC_HRULES | wx.LC_SINGLE_SEL)

        self.__categoryListItemIdDict = dict()

        # setup
        self.AppendColumn("", width=160)
        self.AppendColumn("", width=60)
        listItem = wx.ListItem()
        listItem.SetAlign(wx.LIST_FORMAT_RIGHT)
        self.SetColumn(1, listItem)
        self.SetDropTarget(CategoryDropTarget(self))
        GUIHelper.Fontfactory.changeFontOfCtrl(self)

        self._initLoad()

    def _initLoad(self):
        """
        Fill list with categories
        """
        self.rebuildList()

    def rebuildList(self):
        """
        Empty list and fill list with categories again
        """
        self.DeleteAllItems()

        for category in CategoryService.getLeaves():
            listItemId = self.Append((category.name, "0.00"))
            self.__categoryListItemIdDict[listItemId] = category

    def getCategoryByItem(self, item):
        """
        :param item: list item index
        :type item: wx.ListItem
        :return: the category of the given list item index
        :rtype: Category
        """
        return self.__categoryListItemIdDict.get(item)

    def load(self, startDate, endDate):
        """
        Setting sum of all categories
        :param startDate: start date
        :type startDate: datetime
        :param endDate: end date
        :type endDate: datetime
        """
        # get first item
        nextItem = self.GetNextItem(-1)

        # iterate through list
        while nextItem != -1:
            # get category of this row
            category = self.__categoryListItemIdDict.get(nextItem)  # type: Category

            # get sum
            childSum = TransactionService.getSumForCategoryBetween(category,
                                                                   startDate - timedelta(minutes=1),
                                                                   endDate)
            self.SetItem(nextItem, 1, "{:.2f}".format(childSum))

            nextItem = self.GetNextItem(nextItem)
