#  -*- coding: cp1252 german encoding -*-
import logging
from pathlib import Path

import alembic.config
import alembic.command
import sqlalchemy
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session

from db.DbModelClasses import Category, Transaction, Wallet, SplitRule, CategoryRule
from filehandler import MarkupHelper

__author__ = "Tobias Fochtmann"

logger = logging.getLogger("DB")


class DatabaseManager(object):
    """
    Handling database access
    """
    _instance = None

    def __new__(cls, *args, **kwargs):
        # if there is no instance yet
        if not cls._instance:
            # instantiate
            cls._instance = super(DatabaseManager, cls).__new__(cls)
            cls._instance.__initSingleton__()
            path_alembic_ini = Path.cwd().parent / "alembic.ini"

            config = alembic.config.Config(path_alembic_ini)

            alembic.command.upgrade(config, "head")

        return cls._instance

    def __initSingleton__(self):
        db_path = MarkupHelper.getDbPath()
        self.__engine = sqlalchemy.create_engine("sqlite:///{}".format(db_path))  # type: Engine
        self.__engine.dispose()
        self.__engine.connect()
        declarative_base().metadata.create_all(bind=self.__engine,
                                               tables=[Category.__table__,
                                                       Wallet.__table__,
                                                       Transaction.__table__,
                                                       SplitRule.__table__,
                                                       CategoryRule.__table__])
        self.session = Session(bind=self.__engine)

    def update(self, item):
        """
        Updates the item
        :param item: a database item represented as a model instance
        :type item:
        """
        self.session.add(item)
        self.commit()

    def commit(self):
        self.session.commit()

    def delete(self, item):
        self.session.delete(item)
        self.commit()

    def insert(self, item):
        self.session.add(item)
        self.commit()


class DatabaseManagerOld(object):
    """
    Handling database access
    """
    _instance = None

    # for result
    @staticmethod
    def dict_factory(cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def __new__(cls, *args, **kwargs):
        # if there is no instance yet
        if not cls._instance:
            # instantiate
            cls._instance = super(DatabaseManagerOld, cls).__new__(cls)
            cls._instance.__initSingleton__()

        return cls._instance

    def __initSingleton__(self):
        self.__connection = None
        self.__cursor = None
        self.__connect()
        self._updateScheme()

    def __connect(self):
        logging.info("Connected to database")
        if self.__connection is None:
            import sqlite3
            self.__connection = sqlite3.connect(MarkupHelper.getDbPath(),
                                                detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
            self.__connection.row_factory = DatabaseManagerOld.dict_factory
        if self.__cursor is None:
            self.__cursor = self.__connection.cursor()

    def __disconnect(self):
        if self.__connection is not None:
            self.__connection.close()

    def __del__(self):
        self.__disconnect()

    def _updateScheme(self):
        pass

    def select(self, query, args):
        if self.__cursor is not None:
            logger.debug("{} % {}".format(query, args))
            self.__cursor.execute(query, args)
            return self.__cursor.fetchall()

    def selectOne(self, query, args):
        if self.__cursor is not None:
            logger.debug("{} % {}".format(query, args))
            self.__cursor.execute(query, args)
            return self.__cursor.fetchone()

    def delete(self, query, args):
        if self.__cursor is not None:
            logger.debug("{} % {}".format(query, args))
            self.__cursor.execute(query, args)
            self.__connection.commit()

    def insert(self, query, args):
        if self.__cursor is not None:
            logger.debug("{} % {}".format(query, args))
            self.__cursor.execute(query, args)
            self.__connection.commit()
            return self.__cursor.lastrowid

    def update(self, query, args):
        if self.__cursor is not None:
            logger.debug("{} % {}".format(query, args))
            self.__cursor.execute(query, args)
            self.__connection.commit()
            return self.__cursor.rowcount
