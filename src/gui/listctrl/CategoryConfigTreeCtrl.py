#  -*- coding: cp1252 german encoding -*-
import logging

import wx
import wx.lib.newevent

from db.DatabaseManager import DatabaseManager as DBM
from db.DatabaseService import CategoryService, TransactionService
from db.DbModelClasses import Category
from gui import GUIHelper
from gui.bmp import images
from gui.droptarget.CategoryShiftDropTarget import CategoryShiftDropTarget
from resources import String

__author__ = "Tobias Fochtmann"


class CategoryConfigTreeCtrl(wx.TreeCtrl):
    """
    Panel for editing the categories
    """
    ID_POPUPMENU_ADD = wx.NewId()
    ID_POPUPMENU_EDIT = wx.NewId()
    ID_POPUPMENU_DEL = wx.NewId()

    def __init__(self, parent):
        """
        The constructor
        :param parent:
        :return:
        """
        wx.TreeCtrl.__init__(self, parent, style=wx.TR_EDIT_LABELS | wx.TR_NO_BUTTONS | wx.TR_ROW_LINES | wx.TR_SINGLE)

        self.__rootItem = None
        self.__categoryId2ItemDict = dict()

        self.SetDropTarget(CategoryShiftDropTarget(self))

        self._bindEvents()

    def _bindEvents(self):
        """
        Binds events to actions
        """
        self.Bind(wx.EVT_TREE_BEGIN_LABEL_EDIT, self.__onStartEditing)
        self.Bind(wx.EVT_TREE_END_LABEL_EDIT, self.__onEditingDone)
        self.Bind(wx.EVT_TREE_ITEM_MENU, self.__onContextMenu)
        self.Bind(wx.EVT_TREE_BEGIN_DRAG, self.__onBeginDrag)
        self.Bind(wx.EVT_KEY_UP, self.__onKeyUp)

        self.Bind(wx.EVT_MENU, self.__onPopupMenuAdd, id=self.ID_POPUPMENU_ADD)
        self.Bind(wx.EVT_MENU, self.__onPopupMenuEdit, id=self.ID_POPUPMENU_EDIT)
        self.Bind(wx.EVT_MENU, self.__onPopupMenuDel, id=self.ID_POPUPMENU_DEL)

    def load(self):
        """
        Loads the tree
        """
        # empty list
        self.DeleteAllItems()
        self.__categoryId2ItemDict = dict()

        # add root
        self.__rootItem = self.AddRoot("Categories")
        self.__categoryId2ItemDict[None] = self.__rootItem

        # append all children recursively
        self.__appendItems(self.__rootItem, CategoryService.getOrphans())

        self.ExpandAll()

    def __appendItems(self, parentItem, categories):
        """
        Appends all categories to the parent item
        :param parentItem: the category parent item
        :type parentItem: wx.TreeItemId
        :param categories: list of
        :type categories: list[Category]
        :return:
        """
        # add categories
        for category in categories:
            item = self.AppendItem(parentItem, category.name, data=category)
            self.__categoryId2ItemDict[category.id] = item
            if len(category.children) == 0:
                self.SetItemFont(item, GUIHelper.Fontfactory.changeFont(self.GetItemFont(item),
                                                                        weight=wx.FONTWEIGHT_BOLD))
            self.__appendItems(item, category.children)

    def _getCategoryIdOfItem(self, item):
        """
        :param item: a tree item
        :type item: wx.TreeItemId
        :return: the category database id of the given tree item
        :rtype: long
        """
        if item.IsOk():
            data = self.GetItemData(item)  # type: Category
            if data is not None:
                return data.id

    def updateCategoryParent(self, categoryId, newParentItem):
        """
        Update the parent id of the given category
        :param categoryId: the category id
        :type categoryId: long
        :param newParentItem: the tree item of the new parent
        :type newParentItem: wx.TreeItemId
        """
        if newParentItem.IsOk():
            # should not be the item itself
            if categoryId != int(self._getCategoryIdOfItem(newParentItem)):
                # should not be a child of itself
                item = self.__categoryId2ItemDict.get(categoryId)
                if self._isChildrenOf(item, newParentItem):
                    logging.warning("Can't set category as a sub category of it's own")
                else:
                    category = self.GetItemData(newParentItem)  # type: Category
                    CategoryService.replaceParentId(categoryId, category.id)
                    self.load()

    def updateCategoryName(self, item):
        """
        Updates the category name in the database of the given item
        :param item: the tree item that experienced a label change
        :type item: wx.TreeItemId
        """
        if item.IsOk():
            category = self.GetItemData(item)  # type: Category
            category.name = self.GetItemText(item)
            DBM().update(category)

    def removeCategory(self, item):
        """
        Removes category from database
        :param item: the item that should be removed
        :type item: wx.TreeItemId
        """
        if item.IsOk():
            if self.GetChildrenCount(item) == 0:
                # sure to delete?
                dlg = wx.MessageDialog(self,
                                       message=String.DLG_MSG_CAT_DEL.format(self.GetItemText(item)),
                                       caption=String.DLG_TITLE_CATEGORY_DEL,
                                       style=wx.YES_NO)
                if dlg.ShowModal() == wx.ID_YES:
                    category = self.GetItemData(item)  # type: Category

                    # check if there are transactions linked to this category
                    if TransactionService.hasWithCategory(category):
                        logging.warning("Category '{}' is linked in transactions".format(self.GetItemText(item)))

                        # all transactions need a step category (to be consistent)
                        stepCatDlg = wx.SingleChoiceDialog(self,
                                                           "Choose a substitute category to assign all previous transaction with (of this category)",
                                                           "Select step category",
                                                           choices=[c.name for c in
                                                                    CategoryService.getLeaves(category)])
                        if stepCatDlg.ShowModal() == wx.ID_OK:
                            substCategory = DBM().session.query(Category).filter(
                                Category.name == stepCatDlg.GetStringSelection()).one()  # type: Category
                            TransactionService.replaceCategory(category, substCategory)
                            DBM().delete(category)
                    else:
                        # remove old category
                        DBM().delete(category)

                    self.load()
            else:
                logging.warning("Deleting is allowed only for categories having no subcategories")

    def addCategory(self, parentItem):
        """
        Appends a child category to the selected category
        :param parentItem: selected item
        """
        if parentItem.IsOk():
            parentName = self.GetItemText(parentItem)

            newCategory = Category(parent_id=self._getCategoryIdOfItem(parentItem))

            value = ""
            while True:
                dlg = wx.TextEntryDialog(self,
                                         "Please enter a (non existing) category name (child of '{}')".format(
                                             parentName),
                                         "Adding new category",
                                         value=value)
                if dlg.ShowModal() == wx.ID_OK:
                    # check if category is present
                    value = dlg.GetValue()

                    if not CategoryService.hasName(value):
                        # insert new category
                        newCategory.name = value
                        DBM().insert(newCategory)
                        self.load()
                        break
                    else:
                        logging.warning("Category with name '{}' already exists, choose another name.".format(value))

    def _isChildrenOf(self, item, itemToCheck):
        """
        :param item: the item which children needs to be checked
        :type item: wx.TreeItemId
        :param itemToCheck: the item that my be a child
        :type itemToCheck: wx.TreeItemId
        :return: Returns True, if itemToCheck is a child of item
        :rtype: bool
        """
        child, cookie = self.GetFirstChild(item)
        isChild = False

        while child.IsOk():
            isChild |= child == itemToCheck
            if self.GetChildrenCount(child) > 0:
                isChild |= self._isChildrenOf(child, itemToCheck)
            child, cookie = self.GetNextChild(child, cookie)

        return isChild

    # ----- EVENTS -----

    def __onStartEditing(self, evt):
        """
        Triggered when a label gets edited
        :param evt:
        :type evt: wx.TreeEvent
        """
        # avoid editing the root item
        if evt.GetItem() == self.__rootItem:
            logging.warning("Editing root item is prohibited")
            evt.Veto()

    def __onEditingDone(self, evt):
        """
        Triggered when editing the label has finished
        :param evt:
        :type evt: wx.TreeEvent
        """
        if CategoryService.hasName(evt.GetLabel()):
            logging.warning("Category with name '{}' already exists, choose another name.".format(evt.GetLabel()))
            evt.Veto()
        else:
            wx.CallAfter(self.updateCategoryName, evt.GetItem())

    def __onContextMenu(self, evt):
        """
        Triggered when item is right clicked
        :param evt:
        :type evt: wx.TreeEvent
        """
        item = evt.GetItem()

        if item.IsOk():
            menu = wx.Menu("Perform on item '{}'".format(self.GetItemText(item)))
            menuItem0 = wx.MenuItem(menu, self.ID_POPUPMENU_ADD, "Add sub category")
            menuItem0.SetBitmap(images.ADD.GetBitmap())
            menu.Append(menuItem0)
            menu.AppendSeparator()

            # allow only non-root items for editing
            if item != self.__rootItem:
                menuItem1 = wx.MenuItem(menu, self.ID_POPUPMENU_EDIT, "Edit category")
                menuItem1.SetBitmap(images.PENCIL.GetBitmap())
                menu.Append(menuItem1)

            # allow deletion only for leaf items
            if self.GetChildrenCount(item) == 0:
                menuItem2 = wx.MenuItem(menu, self.ID_POPUPMENU_DEL, "Delete category")
                menuItem2.SetBitmap(images.DELETE.GetBitmap())
                menu.Append(menuItem2)

            self.SelectItem(item)
            wx.CallAfter(self.PopupMenu, menu)

    def __onBeginDrag(self, evt):
        """
        Triggered when dragging an item starts
        :param evt:
        :type evt: wx.TreeEvent
        """
        data = wx.TextDataObject()
        if evt.GetItem() != self.__rootItem:
            data.SetText(str(self._getCategoryIdOfItem(evt.GetItem())))

            # Create drop source and begin drag-and-drop.
            dropSource = wx.DropSource(self)
            dropSource.SetData(data)
            dropSource.DoDragDrop(flags=wx.Drag_DefaultMove)

    def __onKeyUp(self, evt):
        """
        Triggered when a key is pressed
        :param evt:
        :type evt: wx.TreeEvent
        """
        keyCode = evt.GetKeyCode()

        item = self.GetSelection()
        if item.IsOk():
            if keyCode == wx.WXK_F2:
                wx.CallAfter(self.EditLabel, item)
            elif keyCode == wx.WXK_DELETE:
                wx.CallAfter(self.removeCategory, item)
            elif keyCode == wx.WXK_NUMPAD_ADD:
                wx.CallAfter(self.addCategory, item)

    # noinspection PyUnusedLocal
    def __onPopupMenuAdd(self, evt):
        """
        Triggered when in the pop up menu 'add category' is clicked
        :param evt:
        :return: wx.Event
        """
        item = self.GetSelection()

        if item.IsOk():
            wx.CallAfter(self.addCategory, item)

    # noinspection PyUnusedLocal
    def __onPopupMenuEdit(self, evt):
        """
        Triggered when in the pop up menu 'edit category' is clicked
        :param evt:
        :return: wx.Event
        """
        item = self.GetSelection()

        if item.IsOk():
            wx.CallAfter(self.EditLabel, item)

    # noinspection PyUnusedLocal
    def __onPopupMenuDel(self, evt):
        """
        Triggered when in the pop up menu 'del category' is clicked
        :param evt:
        :return: wx.Event
        """
        self.removeCategory(self.GetSelection())

    # ----- END EVENTS -----


if __name__ == "__main__":
    logging.getLogger("DB").setLevel(logging.DEBUG)

    app = wx.App()
    mainFrame = wx.Frame(None, size=(1000, 800))
    mainFrame.pnl = CategoryConfigTreeCtrl(mainFrame)
    mainFrame.Show()
    app.MainLoop()
