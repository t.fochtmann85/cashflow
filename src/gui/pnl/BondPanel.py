#  -*- coding: cp1252 german encoding -*-
import asyncio
from collections import Iterator

import wx
import wx.dataview
import wx.lib.newevent
from wxasync import StartCoroutine

from db.DatabaseService import BondService

__author__ = "Tobias Fochtmann"

from db.DbModelClasses import Bond
from gui.dlg.BondEditDialog import BondEditDialogImpl
from gui.pnl.AccountPanelBase import AccountPanelBase


class BondPanel(AccountPanelBase):
    """
    Panel giving overview over all wallets
    """

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window
        """
        super(BondPanel, self).__init__(parent, BondEditDialogImpl)
        self._init_load()

    def _init_load(self):
        """
        Load initial values, set up initial states
        """
        # set up tree list
        self._tree_list.AppendColumn("Name", flags=0)
        self._tree_list.AppendColumn("Id", flags=0)
        self._tree_list.AppendColumn("Symbol", flags=0)
        self._tree_list.AppendColumn("Quantity", width=100, align=wx.ALIGN_RIGHT, flags=0)
        self._tree_list.AppendColumn("Paid", width=100, align=wx.ALIGN_RIGHT, flags=0)
        self._tree_list.AppendColumn("Fee", width=100, align=wx.ALIGN_RIGHT, flags=0)
        self._tree_list.AppendColumn("Current price", width=130, align=wx.ALIGN_RIGHT, flags=0)
        self._tree_list.AppendColumn("Current value", width=130, align=wx.ALIGN_RIGHT, flags=0)
        self._tree_list.AppendColumn("Difference", width=130, align=wx.ALIGN_RIGHT, flags=0)
        self._tree_list.AppendColumn("", width=130, align=wx.ALIGN_RIGHT, flags=0)


class BondPanelImpl(BondPanel):
    """
    Panel giving overview over all wallets
    """

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window
        """
        super(BondPanelImpl, self).__init__(parent)
        self._total_node = None

    def _add_item(self, bond, parent_item):
        item = self._tree_list.AppendItem(parent_item, bond.name)

        data = [
            bond.iid,
            bond.symbol or "",
            f"{BondService.get_quantity(bond):.6f}",
            f"{BondService.get_cost(bond):.2f} �",
            f"{BondService.get_fee(bond):.2f} �",
            "... not discovered ...",
            "... not discovered ...",
            "... not discovered ...",
        ]
        for col, value in enumerate(data):
            self._tree_list.SetItemText(item, col + 1, value)

        self._tree_list.SetItemData(item, bond)

    def load(self):
        """
        Adds info about the wallets to the tree list listctrl
        """
        self._tree_list.DeleteAllItems()

        groups = [
            "Fund",
            "ETF",
            "Share",
            "Crypto",
        ]

        for group_name in groups:
            bonds = BondService.get_items_of_type(group_name)
            if len(bonds) > 0:
                group_node = self._tree_list.AppendItem(self._tree_list.GetRootItem(), group_name)
                for bond in bonds:
                    self._add_item(bond, group_node)
                self._tree_list.Expand(group_node)

        self._total_node = self._tree_list.AppendItem(self._tree_list.GetRootItem(), "Total")

        # StartCoroutine(self._do_blocking_stuff, self)

    async def _do_blocking_stuff(self):
        await asyncio.sleep(1)
        await self._get_current_prices()

    def _iter_leaves(self) -> Iterator[wx.dataview.TreeListItem]:
        """
        Iterates through leaf nodes and returns them
        """
        group_item = self._tree_list.GetFirstChild(self._tree_list.GetRootItem())

        while group_item.IsOk():
            bond_item = self._tree_list.GetFirstChild(group_item)
            while bond_item.IsOk():
                yield bond_item
                bond_item = self._tree_list.GetNextSibling(bond_item)
            group_item = self._tree_list.GetNextSibling(group_item)

    async def _get_current_prices(self):
        """
        Determine current prices of all known bonds
        """
        for bond_item in self._iter_leaves():
            self._tree_list.SetItemText(bond_item, 6, "-discovering-")
            self._tree_list.SetItemText(bond_item, 7, "-discovering-")
            self._tree_list.SetItemText(bond_item, 8, "-discovering-")
            await asyncio.sleep(0.1)

            bond = self._tree_list.GetItemData(bond_item)  # type: Bond
            quantity = BondService.get_quantity(bond)
            price = bond.get_price()
            self._tree_list.SetItemText(bond_item, 6, f"{price :.2f} �")
            self._tree_list.SetItemText(bond_item, 7, f"{price * quantity :.2f} �")
            self._tree_list.SetItemText(bond_item, 8, f"{price * quantity - BondService.get_cost(bond):.2f} �")
            await asyncio.sleep(0.1)

        await self._update_group_sums()

    async def _update_group_sums(self):
        total_sums = [0.0, 0.0, 0.0, 0.0]

        group_item = self._tree_list.GetFirstChild(self._tree_list.GetRootItem())
        while group_item.IsOk():
            group_sums = [0.0, 0.0, 0.0, 0.0]

            bond_item = self._tree_list.GetFirstChild(group_item)
            while bond_item.IsOk():
                bond = self._tree_list.GetItemData(bond_item)  # type: Bond

                cost = BondService.get_cost(bond)
                quantity = BondService.get_quantity(bond)
                price = bond.get_price()

                group_sums[0] += cost
                group_sums[1] += BondService.get_fee(bond)
                group_sums[2] += quantity * price
                group_sums[3] += quantity * price - cost

                bond_item = self._tree_list.GetNextSibling(bond_item)

            self._tree_list.SetItemText(group_item, 4, f"{group_sums[0] :.2f} �")
            self._tree_list.SetItemText(group_item, 5, f"{group_sums[1] :.2f} �")
            self._tree_list.SetItemText(group_item, 7, f"{group_sums[2] :.2f} �")
            self._tree_list.SetItemText(group_item, 8, f"{group_sums[3] :.2f} �")

            total_sums[0] += group_sums[0]
            total_sums[1] += group_sums[1]
            total_sums[2] += group_sums[2]
            total_sums[3] += group_sums[3]

            group_item = self._tree_list.GetNextSibling(group_item)

        self._tree_list.SetItemText(self._total_node, 4, f"{total_sums[0] :.2f} �")
        self._tree_list.SetItemText(self._total_node, 5, f"{total_sums[1] :.2f} �")
        self._tree_list.SetItemText(self._total_node, 7, f"{total_sums[2] :.2f} �")
        self._tree_list.SetItemText(self._total_node, 8, f"{total_sums[3] :.2f} �")
