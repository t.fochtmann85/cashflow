#  -*- coding: cp1252 german encoding -*-
from sqlalchemy import Column, Integer, String, UniqueConstraint, Boolean, ForeignKey, Date, Float, Enum, \
    CheckConstraint, Text, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref, validates

import CommonFunctions
from api import webservices

VERSION = 1

RULE_CONDITION_FIELDS = Enum("src_dest",
                             "purpose",
                             "iban")

BOND_TYPE = Enum(
    "Share",
    "Fund",
    "ETF",
    "Crypto"
)

WALLET_TYPES = Enum(
    "Cash",
    "Giro",
    "Credit Card",
    "Fonds",
    "Building society savings",
    "Annuity insurance",
    "Prepaid",
    "Miscellaneous"
)

SECTOR = Enum(
    "Technologie",
    "Handel & Konsum",
    "Chemie, Pharma, Bio- & Medizintechnik",
    "Nahrungs- & Genussmittel",
    "Maschinenbau, Verkehr & Logistik",
    "Finanzen",
    "Sonstiges",
)

COUNTRY = Enum(
    "USA",
    "D",
    "DAN",
    "CH",
    "FRA",
    "AU",
    "GB",
)

INVESTMENT_TYPE = Enum(
    "Buy",
    "Sell",
    "Dividend"
)

meta = MetaData(naming_convention={
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
})

Base = declarative_base(metadata=meta)


class Wallet(Base):
    __tablename__ = 'wallet'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False, unique=True)
    type = Column(WALLET_TYPES, nullable=False)
    fast_accessible = Column(Boolean(), nullable=False, default=True)
    as_format_info = Column(Text())


class Category(Base):
    __tablename__ = 'category'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False, unique=True)
    parent_id = Column(Integer, ForeignKey('category.id'))
    children = relationship("Category", backref=backref('parent', remote_side=[id]))

    __table_args__ = (
        CheckConstraint("id != parent_id", "self_linking"),
    )

    def __repr__(self) -> str:
        return f"Category(name={self.name!r})"


class Transaction(Base):
    __tablename__ = 'transaction'
    id = Column(Integer, primary_key=True, autoincrement=True)
    uniqueid = Column(String(32), nullable=False, unique=True)
    date = Column(Date(), nullable=False)
    amount = Column(Float(precision=2), nullable=False)
    wallet_id = Column(Integer, ForeignKey('wallet.id'), nullable=False)
    wallet = relationship("Wallet")
    category_id = Column(Integer, ForeignKey('category.id'), nullable=False)
    category = relationship("Category")
    src_dest = Column(String())
    purpose = Column(String())
    iban = Column(String(22))
    bic = Column(String(11))
    comment = Column(Text())
    factor = Column(Float(precision=2), nullable=False, default=1.)

    def __repr__(self):
        return f"{self.__tablename__} from {self.date}: {self.amount} �"

    @validates('factor')
    def validate_factor(self, key, value):
        assert 0 <= value <= 1
        return value

    def updateHash(self):
        self.uniqueid = CommonFunctions.buildHashForTransaction(self)


class CategoryRule(Base):
    __tablename__ = 'rule_category'
    id = Column(Integer, primary_key=True, autoincrement=True)
    cond_field = Column(RULE_CONDITION_FIELDS, nullable=False)
    contains = Column(String(100), nullable=False)
    category_id = Column(Integer, ForeignKey('category.id'), nullable=False)
    category = relationship("Category")
    __table_args__ = (
        UniqueConstraint('cond_field', 'contains'),
    )


class SplitRule(Base):
    __tablename__ = 'rule_split'
    id = Column(Integer, primary_key=True, autoincrement=True)
    cond_field = Column(RULE_CONDITION_FIELDS, nullable=False)
    contains = Column(String(100), nullable=False)
    parse_field = Column(RULE_CONDITION_FIELDS, nullable=False)
    iter_regex = Column(String(), nullable=False)


class Bond(Base):
    __tablename__ = "bond"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(100), nullable=False)
    iid = Column(String(100), nullable=False)
    symbol = Column(String(10), nullable=True)
    type = Column(BOND_TYPE, nullable=False)
    sector = Column(SECTOR, nullable=True)
    country = Column(COUNTRY, nullable=False)

    def get_price(self) -> float:
        print(f"Get price for bond {self.name!r}")

        if self.type == "Share":
            if self.symbol:
                return webservices.ShareHook.get_price_for_share_by_symbol(self.symbol)
            else:
                return webservices.ShareHook.get_price_for_share_by_isin(self.iid)
        elif self.type == "Fund":
            return webservices.FundHook.get_price_for_fund_by_isin(self.iid)
        elif self.type == "Crypto":
            return webservices.CryptoHook.get_price_for_crypto(self.iid)
        elif self.type == "ETF":
            if self.symbol:
                return webservices.EtfHook.get_price_for_etf_by_symbol(self.symbol)
            else:
                return webservices.EtfHook.get_price_for_etf_by_name(self.name)


class Investment(Base):
    __tablename__ = "investment"
    id = Column(Integer, primary_key=True, autoincrement=True)
    cost = Column(Float(precision=2), nullable=False)
    fee = Column(Float(precision=2), nullable=False)
    bond_id = Column(Integer, ForeignKey('bond.id'), nullable=False)
    bond = relationship("Bond")
    quantity = Column(Float(precision=6), nullable=False)
    date = Column(Date(), nullable=False)
    platform = Column(String(100), nullable=False)
    stock_exchange = Column(String(100), nullable=False)
    type = Column(INVESTMENT_TYPE, nullable=False, server_default="Buy")

    def __repr__(self):
        return f"Investment(Bond: {self.bond.name})"
