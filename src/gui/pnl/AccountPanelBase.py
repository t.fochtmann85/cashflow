#  -*- coding: cp1252 german encoding -*-

import wx
import wx.dataview
import wx.lib.newevent

from db.DatabaseManager import DatabaseManager as DBM
from db.DbModelClasses import Wallet
from gui import GUIHelper

__author__ = "Tobias Fochtmann"


class AccountPanelBase(wx.Panel):
    """
    Base class for wallets and bonds
    """

    def __init__(self, parent, dialog_class):
        """
        Constructor
        :param parent: parent window
        """
        wx.Panel.__init__(self, parent)
        self._dlg_cls = dialog_class

        self._sizer = wx.GridBagSizer()
        self._tree_list = wx.dataview.TreeListCtrl(self, style= wx.dataview.TL_SINGLE)
        line = 0
        border = 10
        self._sizer.Add(self._tree_list, (line, 0), (1, 1), wx.EXPAND | wx.LEFT | wx.TOP | wx.BOTTOM, border)

        self._sizer.AddGrowableCol(0)
        self._sizer.AddGrowableRow(0)

        self.SetSizerAndFit(self._sizer)
        self._tree_list.GetDataView().GetMainWindow().Bind(wx.EVT_KEY_UP, self._on_key_up)
        self._tree_list.Bind(wx.dataview.EVT_TREELIST_ITEM_ACTIVATED, self._handle_edit)

    def load(self):
        raise NotImplementedError("Implement in subclass")

    # ----- EVENTS ------------

    def _handle_new(self):
        """
        Create a new wallet
        """
        dlg = self._dlg_cls(self)
        if dlg.ShowModal() == wx.ID_OK:
            self.load()

    def _handle_edit(self, evt=None):
        """
        :param evt: Triggered when button 'edit' is clicked
        :type evt: wx.CommandEvent
        """
        item = self._tree_list.GetSelection()
        if item.IsOk():
            obj = self._tree_list.GetItemData(item)
            dlg = self._dlg_cls(self, obj)
            if dlg.ShowModal() == wx.ID_OK:
                self.load()

    def _handle_delete(self):
        """
        Remove selected
        """
        item = self._tree_list.GetSelection()

        if item.IsOk():
            obj = self._tree_list.GetItemData(item)  # type: Wallet
            # asks user to really remove it
            dlg = GUIHelper.DialogFactory.createYesNoDialog(
                self,
                f"Remove selected item",
                f"Do you really want to remove item {obj.name!r}?"
            )

            if dlg.ShowModal() == wx.ID_YES:
                DBM().delete(obj)
                self.load()

    def _on_key_up(self, evt: wx.KeyEvent):
        """
        :param evt: Triggered when key is pressd
        """
        print("KeyCode", evt.GetKeyCode())
        if evt.GetKeyCode() == wx.WXK_NUMPAD_ADD:
            self._handle_new()
        elif evt.GetKeyCode() == wx.WXK_DELETE:
            self._handle_delete()
        elif evt.GetKeyCode() in [wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER]:
            self._handle_edit()
