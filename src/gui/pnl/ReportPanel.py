#  -*- coding: cp1252 german encoding -*-
import wx
from wx.lib.buttons import GenBitmapTextButton

from gui.bmp import ArtProvider
from gui.listctrl.ReportGrid import ReportGrid
from gui.populator.ReportPopulator import ReportPopulatorMonth, ReportPopulatorYear, \
    ReportPopulatorAllTime

__author__ = "Tobias Fochtmann"


class ReportPanel(wx.Panel):
    """
    Panel holding different reports
    """

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window 
        """
        wx.Panel.__init__(self, parent)

        self._createControls()
        self._setToolTips()
        self._setLayout()
        self._bindEvents()
        self._initLoad()

    def _createControls(self):
        """
        Create controls and widgets
        """
        self.sizer = wx.GridBagSizer()

        self.nb = wx.Notebook(self)

        self.report_monthly = ReportGrid(self.nb, ReportPopulatorMonth())
        self.report_yearly = ReportGrid(self.nb, ReportPopulatorYear())
        self.report_allTime = ReportGrid(self.nb, ReportPopulatorAllTime())

        self.nb.AddPage(self.report_monthly, "Monthly")
        self.nb.AddPage(self.report_yearly, "Yearly")
        self.nb.AddPage(self.report_allTime, "All time")

        self.btn_refresh = GenBitmapTextButton(self,
                                               bitmap=ArtProvider.images.DATA_REFRESH.GetBitmap(),
                                               label="Generate report")
        self.btn_refresh.SetBezelWidth(1)

    def _setToolTips(self):
        """
        Set tool tips
        """
        pass

    def _setLayout(self):
        """
        Positioning all controls of the panel
        """
        self.sizer.Add(self.btn_refresh, (0, 0), (1, 1), wx.EXPAND)
        self.sizer.Add(self.nb, (1, 0), (1, 1), wx.EXPAND)
        self.sizer.AddGrowableCol(0)
        self.sizer.AddGrowableRow(1)

        self.SetSizerAndFit(self.sizer)

    def _bindEvents(self):
        """
        Associate events with actions
        """
        self.btn_refresh.Bind(wx.EVT_BUTTON, self.__onGenerateReport)

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        pass

    def load(self):
        """
        Load values
        """
        pass

    def __onGenerateReport(self, evt):
        """
        Triggered when button 'generate' is pressed
        :param evt:
        """
        wx.BeginBusyCursor()
        self.report_monthly.load()
        self.report_yearly.load()
        self.report_allTime.load()
        wx.EndBusyCursor()

    # ----- EVENTS ------------

    # ----- END OF EVENTS -----


if __name__ == "__main__":
    app = wx.App()
    mainFrame = wx.Frame(None, size=(1000, 800))
    mainFrame.pnl = ReportPanel(mainFrame)
    mainFrame.Show()
    mainFrame.pnl.load()
    app.MainLoop()
