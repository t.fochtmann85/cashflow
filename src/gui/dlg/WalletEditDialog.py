#  -*- coding: cp1252 german encoding -*-

import json
import logging

import wx

from db.DatabaseManager import DatabaseManager as DBM
from db.DbModelClasses import Wallet, WALLET_TYPES
from gui import GUIHelper
from gui.bmp import images
from gui.dlg.AccountStatementWizard import AccountStatementWizard
from resources import String

__author__ = "TobiasFochtmann"


class WalletEditDialog(wx.Dialog):
    """
    Dialog for editing a wallet (or adding a new one)
    """
    PADDING = 10

    def __init__(self, parent, wallet=None):
        """
        Constructor
        :param parent: parent window
        :param wallet: the wallet to edit
        :type wallet: Wallet
        """
        if wallet is None:
            title = "Add new wallet..."
            icon = images.ADD_WALLET.GetIcon()
        else:
            assert isinstance(wallet, Wallet), "Argument �wallet� needs to be of type �DbModelClasses.Wallet�"
            title = "Edit wallet {}...".format(wallet.name)
            icon = images.WALLET_CLOSED_EDIT.GetIcon()

        wx.Dialog.__init__(self, parent, title=title, style=wx.CAPTION | wx.SYSTEM_MENU)
        self.SetIcon(icon)

        self.wallet = wallet
        self.walletTypeDict_id2Name = dict()
        self.walletTypeDict_name2Id = dict()

        self._createControls()
        self._setToolTips()
        self._setLayout()
        self._bindEvents()
        self._initLoad()

        self.CenterOnParent()

    def _createControls(self):
        """
        Creates all used widgets
        """
        # sizers
        self.sizer = wx.GridBagSizer(hgap=self.PADDING, vgap=self.PADDING)

        # panel
        self.pnl = wx.Panel(self)

        # labels
        self.lbl_desc = wx.StaticText(self.pnl, label=String.LBL_DESC_WALLET)
        GUIHelper.Fontfactory.changeFontOfCtrl(self.lbl_desc, offset=2)
        self.lbl_type = wx.StaticText(self.pnl, label=String.LBL_TYPE_WALLET)
        GUIHelper.Fontfactory.changeFontOfCtrl(self.lbl_type, offset=2)
        self.lbl_consider = wx.StaticText(self.pnl, label=String.LBL_CONSIDER_WALLET)
        GUIHelper.Fontfactory.changeFontOfCtrl(self.lbl_consider, offset=2)
        self.lbl_regex = wx.StaticText(self.pnl, label=String.LBL_FORMATINFO_WALLET)
        GUIHelper.Fontfactory.changeFontOfCtrl(self.lbl_regex, offset=2)

        # text inputs
        self.txt_desc = wx.TextCtrl(self.pnl, size=(200, 24))
        self.txt_regex = wx.TextCtrl(self.pnl, size=(400, 100),
                                     style=wx.TE_MULTILINE | wx.TE_CHARWRAP | wx.RESIZE_BORDER)

        # combo boxes
        self.cmb_type = wx.ComboBox(self.pnl, size=(200, 24), style=wx.CB_READONLY,
                                    choices=sorted(WALLET_TYPES.enums))

        # check boxes
        self.chb_consider = wx.CheckBox(self.pnl)
        self.chb_consider.SetValue(True)

        # buttons
        self.btn_wizard = wx.BitmapButton(self.pnl, bitmap=images.MAGIC_WAND.GetBitmap())
        self.btn_apply = wx.BitmapButton(self.pnl, bitmap=images.APPLY.GetBitmap())
        self.btn_discard = wx.BitmapButton(self.pnl, bitmap=images.DISCARD.GetBitmap())

    def _setToolTips(self):
        """
        Set tool tips
        """
        self.btn_apply.SetToolTip("Apply changes to database and close dialog")
        self.btn_discard.SetToolTip("Discard changes and close dialog")
        self.txt_regex.SetToolTip("Formatting info to parse account statements in JSON format")
        self.btn_wizard.SetToolTip("Wizard for formatting info")

    def _setLayout(self):
        """
        Sets the layout of the widgets
        """
        line = 0
        self.sizer.Add(self.lbl_desc, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.TOP, self.PADDING)
        self.sizer.Add(self.txt_desc, (line, 1), (1, 1), wx.TOP | wx.RIGHT, self.PADDING)
        line += 1
        self.sizer.Add(self.lbl_type, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.sizer.Add(self.cmb_type, (line, 1), (1, 1), wx.RIGHT, self.PADDING)
        line += 1
        self.sizer.Add(self.lbl_consider, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.sizer.Add(self.chb_consider, (line, 1), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.RIGHT, self.PADDING)
        line += 1
        self.sizer.Add(self.lbl_regex, (line, 0), (1, 1), wx.ALIGN_TOP | wx.LEFT, self.PADDING)
        self.sizer.Add(self.txt_regex, (line, 1), (2, 1), wx.RIGHT, self.PADDING)
        line += 1
        self.sizer.Add(self.btn_wizard, (line, 0), (1, 1), wx.ALIGN_CENTER_HORIZONTAL)
        line += 1
        self.sizer.Add(wx.StaticLine(self.pnl, size=(100, 2)), (line, 0), (1, 2), wx.EXPAND | wx.LEFT | wx.RIGHT,
                       self.PADDING)
        line += 1
        self.sizer.Add(self.btn_apply, (line, 0), (1, 1), wx.ALIGN_RIGHT | wx.BOTTOM, self.PADDING)
        self.sizer.Add(self.btn_discard, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.BOTTOM, self.PADDING)

        self.pnl.SetSizer(self.sizer)
        self.pnl.Fit()
        self.SetSizerAndFit(self.sizer)

    def _bindEvents(self):
        """
        Binds events to actions
        """
        self.btn_apply.Bind(wx.EVT_BUTTON, self.__onApply)
        self.btn_discard.Bind(wx.EVT_BUTTON, self.__onDiscard)
        self.btn_wizard.Bind(wx.EVT_BUTTON, self.__onFormattingWizard)

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        # when editing a wallet
        if self.wallet is not None:
            # description
            self.txt_desc.SetValue(self.wallet.name)

            # wallet type
            self.cmb_type.SetValue(self.wallet.type)

            # checkbox consider
            self.chb_consider.SetValue(self.wallet.fast_accessible)

            # regular expression
            self.txt_regex.SetValue(self.wallet.as_format_info or "")

    def _checkValues(self):
        """
        Validates input values
        :return: True if all values are ok
        :rtype: bool
        """
        if not self.txt_desc.GetValue():
            logging.warning("Description can't be empty")
            return False

        if not self.cmb_type.GetValue():
            logging.warning("Wallet type can't be empty")
            return False

        return True

    def getWallet(self):
        """
        :return: Returns the updated wallet info
        """
        return self.wallet

    # ----- EVENTS -----

    def __onApply(self, evt):
        """
        :param evt: Triggered when button 'apply' is clicked
        :type evt: wx.CommandEvent
        """
        if self._checkValues():
            # differ between update and create new
            if self.wallet is None:
                # create new item
                self.wallet = Wallet()
                self.wallet.name = self.txt_desc.GetValue()
                self.wallet.type = self.cmb_type.GetValue()
                self.wallet.fast_accessible = self.chb_consider.GetValue()
                self.wallet.as_format_info = self.txt_regex.GetValue()

                # insert into database
                DBM().insert(self.wallet)
            else:
                self.wallet.name = self.txt_desc.GetValue()
                self.wallet.type = self.cmb_type.GetValue()
                self.wallet.fast_accessible = self.chb_consider.GetValue()
                self.wallet.as_format_info = self.txt_regex.GetValue()

                # update in database
                DBM().update(self.wallet)

            self.EndModal(wx.ID_OK)

    def __onDiscard(self, evt):
        """
        :param evt: Triggered when button 'discard' is clicked
        :type evt: wx.CommandEvent
        """
        self.EndModal(wx.ID_CANCEL)
        self.Close()

    def __onFormattingWizard(self, evt):
        """
        :param evt: Triggered when button 'wizard' is clicked
        :type evt: wx.CommandEvent
        """
        dlg = AccountStatementWizard(self, self.wallet)
        if dlg.ShowModal() == wx.ID_OK:
            self.txt_regex.SetValue(json.dumps(dlg.getFormatInfo()))
            # self._initLoad()


if __name__ == "__main__":
    logging.getLogger("DB").setLevel(logging.DEBUG)
    from db.DatabaseService import WalletService

    wallet = WalletService.getByName("DB Giro")
    app = wx.App()
    mainFrame = WalletEditDialog(None, wallet)
    mainFrame.Show()
    app.MainLoop()
