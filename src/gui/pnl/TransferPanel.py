#  -*- coding: cp1252 german encoding -*-
import json
import logging

import wx
import wx.lib.newevent

import CommonFunctions
from db.DatabaseManager import DatabaseManager as DBM
from db.DatabaseService import TransactionService, WalletService
from db.DbModelClasses import Transaction, RULE_CONDITION_FIELDS
from filehandler import AccountStatementParser
from gui import GUIHelper
from gui.bmp import images
from gui.dlg.CategoryRuleEditDialog import CategoryRuleEditDialog
from gui.dlg.InvestmentEditDialogImpl import InvestmentEditDialogImpl
from gui.dlg.TransactionEditDialog import TransactionEditDialog
from gui.events import TransactionChangedEvent
from gui.listctrl.CategoryLeafListCtrl import CategoryLeafListCtrl
from gui.listctrl.FilterListView import FilterListView
from resources import String

__author__ = "Tobias Fochtmann"


class TransferPanel(wx.Panel):
    """
    Panel for showing transactions
    """
    ID_POPUPMENU_DELETE_TA = wx.NewId()
    ID_POPUPMENU_CREATE_INVERTED_TA = wx.NewId()
    ID_POPUPMENU_COPY_TA = wx.NewId()
    ID_POPUPMENU_CREATE_RULE_CATEGORY = wx.NewId()
    ID_POPUPMENU_CREATE_INVESTMENT = wx.NewId()

    UpdateCategoryRulesEvent, EVT_UPDATE_CATEGORY_RULES = wx.lib.newevent.NewCommandEvent()

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window 
        """
        wx.Panel.__init__(self, parent)

        self._createControls()
        self._setToolTips()
        self._setLayout()
        self._bindEvents()
        self._initLoad()

        self.startDate = None
        self.endDate = None

    def _createControls(self):
        """
        Create controls and widgets
        """
        self.sizer = wx.GridBagSizer(hgap=5, vgap=5)
        self.sizer_button0 = wx.BoxSizer(wx.VERTICAL)

        self.lbl_periodTotal = wx.StaticText(self, label=String.LBL_PERIOD_TOTAL)
        GUIHelper.Fontfactory.changeFontOfCtrl(self.lbl_periodTotal, offset=5, face="Adobe Devanagari")
        self.lbl_valuePeriodTotal = wx.StaticText(self, label="0.00 �", style=wx.ALIGN_RIGHT)
        GUIHelper.Fontfactory.changeFontOfCtrl(self.lbl_valuePeriodTotal, offset=5, face="Adobe Devanagari")

        self.lst_transactions = FilterListView(self,
                                               itemValueFunc=TransferPanel.getTransactionColumnValue,
                                               filterValueFunc=TransferPanel.getTransactionColumnValue,
                                               sortValueFunc=TransferPanel.getSortValue,
                                               listType=FilterListView.LIST_TYPE_LIST,
                                               columnList=[(String.LBL_COL_DATE, 70, FilterListView.INPUT_TYPE_TEXT,
                                                            wx.TE_PROCESS_ENTER),
                                                           (String.LBL_COL_AMOUNT, 60, FilterListView.INPUT_TYPE_TEXT,
                                                            wx.TE_PROCESS_ENTER),
                                                           (String.LBL_COL_FACTOR, 40, FilterListView.INPUT_TYPE_TEXT,
                                                            wx.TE_PROCESS_ENTER),
                                                           (String.LBL_COL_SRCDEST, 150, FilterListView.INPUT_TYPE_TEXT,
                                                            wx.TE_PROCESS_ENTER),
                                                           (String.LBL_COL_PURPOSE, 150, FilterListView.INPUT_TYPE_TEXT,
                                                            wx.TE_PROCESS_ENTER),
                                                           (String.LBL_COL_WALLET, 100, FilterListView.INPUT_TYPE_COMB,
                                                            wx.TE_PROCESS_ENTER),
                                                           (String.LBL_COL_CAT, 120, FilterListView.INPUT_TYPE_COMB,
                                                            wx.TE_PROCESS_ENTER),
                                                           (String.LBL_COL_COMMENT, 100, FilterListView.INPUT_TYPE_TEXT,
                                                            wx.TE_PROCESS_ENTER)],
                                               itemList=list(),
                                               listMinSize=(700, 200),
                                               listStyle=wx.LC_REPORT | wx.LC_VIRTUAL | wx.LC_HRULES)
        self._setUpTransactionList()

        self.lst_categories = CategoryLeafListCtrl(self)

        self.btn_scan = wx.BitmapButton(self, bitmap=images.SUCHE.GetBitmap())
        self.btn_add = wx.BitmapButton(self, bitmap=images.MONEY_ENVELOPE_ADD.GetBitmap())
        self.btn_edit = wx.BitmapButton(self, bitmap=images.MONEY_ENVELOPE_EDIT.GetBitmap())
        self.btn_rem = wx.BitmapButton(self, bitmap=images.MONEY_ENVELOPE_DELETE.GetBitmap())
        self.btn_import = wx.BitmapButton(self, bitmap=images.DOCUMENT_DOWN.GetBitmap())
        self.btn_set05 = wx.Button(self, label="0.5")

    def _setUpTransactionList(self):
        listItem = wx.ListItem()
        listItem.SetAlign(wx.LIST_FORMAT_RIGHT)
        self.lst_transactions.lst.SetColumn(1, listItem)
        GUIHelper.Fontfactory.changeFontOfCtrl(self.lst_transactions.lst)

    def _setToolTips(self):
        """
        Set tool tips
        """
        self.btn_scan.SetToolTip("Scan local folder for account statement files to process")
        self.btn_add.SetToolTip("Add transaction")
        self.btn_edit.SetToolTip("Edit transaction")
        self.btn_rem.SetToolTip("Remove transaction")
        self.btn_import.SetToolTip("Import account statement")
        self.btn_set05.SetToolTip("Set factor to 0.5")

    def _setLayout(self):
        """
        Positioning all controls of the panel
        """
        padding = 0

        self.sizer_button0.Add(self.btn_add)
        self.sizer_button0.Add(self.btn_edit)
        self.sizer_button0.Add(self.btn_rem)
        self.sizer_button0.Add(self.btn_import)
        self.sizer_button0.Add(self.btn_scan)
        self.sizer_button0.Add(self.btn_set05)

        line = 0
        self.sizer.Add(self.sizer_button0, (line, 0), (1, 1), wx.TOP | wx.EXPAND | wx.ALIGN_RIGHT, padding)
        self.sizer.Add(self.lst_transactions, (line, 1), (2, 1), wx.EXPAND | wx.LEFT | wx.BOTTOM, padding)
        self.sizer.Add(self.lst_categories, (line, 2), (1, 2), wx.EXPAND | wx.RIGHT, padding)
        self.sizer.AddGrowableRow(line)
        line += 1
        self.sizer.Add(self.lbl_periodTotal, (line, 2), (1, 1), wx.ALIGN_BOTTOM)
        self.sizer.Add(self.lbl_valuePeriodTotal, (line, 3), (1, 1), wx.ALIGN_BOTTOM | wx.RIGHT, padding)
        self.sizer.AddGrowableCol(1)

        self.SetSizerAndFit(self.sizer)

    def _bindEvents(self):
        """
        Associate events with actions
        """
        self.btn_scan.Bind(wx.EVT_BUTTON, self.__on_scan)
        self.btn_add.Bind(wx.EVT_BUTTON, self.__onAddTransaction)
        self.btn_edit.Bind(wx.EVT_BUTTON, self.__onEditTransaction)
        self.btn_rem.Bind(wx.EVT_BUTTON, self.__onRemoveTransaction)
        self.btn_import.Bind(wx.EVT_BUTTON, self.__onImportAccountStatement)
        self.btn_set05.Bind(wx.EVT_BUTTON, self._on_set_factor_to_0_5)

        self.lst_categories.Bind(wx.EVT_KEY_UP, self.__onCategoryListKeyUp)

        self.lst_transactions.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.__onEditTransaction)
        self.lst_transactions.Bind(wx.EVT_LIST_KEY_DOWN, self.__onKeyDownOnItem)
        self.lst_transactions.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.__onTransactionRightClick)
        self.lst_transactions.Bind(wx.EVT_MENU, self.__onCreateInvertedTransaction,
                                   id=self.ID_POPUPMENU_CREATE_INVERTED_TA)
        self.lst_transactions.Bind(wx.EVT_MENU, self.__onCreateTransactionCopy, id=self.ID_POPUPMENU_COPY_TA)
        self.lst_transactions.Bind(wx.EVT_MENU, self.__onCreateRuleCategory, id=self.ID_POPUPMENU_CREATE_RULE_CATEGORY)
        self.lst_transactions.Bind(wx.EVT_MENU, self._on_create_investment, id=self.ID_POPUPMENU_CREATE_INVESTMENT)
        self.lst_transactions.Bind(FilterListView.EVT_ITEM_DATA_CHANGED, self.__onItemDataChanged)
        self.lst_transactions.lst.Bind(FilterListView.EVT_FLV_ITEM_SELECTED, self.__onTransactionSelectionChanged)
        self.lst_transactions.lst.Bind(FilterListView.EVT_FLV_ITEM_DESELECTED, self.__onTransactionSelectionChanged)

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        self.btn_edit.Enable(False)
        self.btn_rem.Enable(False)

    def load(self):
        """
        Loads the data of the current database
        """
        startDate = CommonFunctions.wxDateTimeToDatetime(self.startDate)
        endDate = CommonFunctions.wxDateTimeToDatetime(self.endDate)
        transactionList = TransactionService.getBetween(startDate, endDate)

        # get transcations
        self.lst_transactions.setListItems(transactionList)

        sumPeriod = sum([t.amount for t in transactionList])
        self.lbl_valuePeriodTotal.SetLabel("{:.2f} �".format(sumPeriod))

        self.lst_categories.load(CommonFunctions.wxDateTimeToDatetime(self.startDate),
                                 CommonFunctions.wxDateTimeToDatetime(self.endDate))

    def _fire_event_transfer_changed(self):
        """
        Triggers an event for updating the wallet balance (in other view)
        """
        wx.PostEvent(self.GetEventHandler(), TransactionChangedEvent(self.GetId()))

    # ----- EVENTS ------------
    def _on_set_factor_to_0_5(self, evt):
        for transaction in self.lst_transactions.getSelectedListItems():
            transaction.factor = .5
            DBM().update(transaction)
        self._fire_event_transfer_changed()
        self.load()

    def __onImportAccountStatement(self, evt):
        """
        Triggered tool 'import account statement' is clicked
        :param evt:
        """
        # user needs to choose a wallet
        wallets = WalletService.getForParsing()
        wDlg = wx.SingleChoiceDialog(self,
                                     message=String.DLG_MESSAGE_SEL_WALLET,
                                     caption=String.DLG_TITLE_SEL_WALLET,
                                     choices=[item.name for item in wallets])

        # Show dialog
        if wDlg.ShowModal() == wx.ID_OK:
            selIdx = wDlg.GetSelection()
            if selIdx != wx.NOT_FOUND:
                wallet = wallets[selIdx]

                # get filedialog for choosing
                fileDlg = wx.FileDialog(self,
                                        message=String.FILEDLG_MESSAGE_ACCOUNTSTATEMENT,
                                        style=wx.FD_FILE_MUST_EXIST | wx.FD_OPEN)

                # show file dialog
                if fileDlg.ShowModal() == wx.ID_OK:
                    tList = AccountStatementParser.parseAccountStatement(fileDlg.GetPath(),
                                                                         json.loads(wallet.as_format_info),
                                                                         wallet.id)
                    for transaction in tList:
                        if TransactionService.existsWithHash(transaction):
                            logging.warning("Transaction with same details already exists and will be skipped:\n" +
                                            "{}, {}, {}, {}".format(transaction.date.strftime(String.FMT_DATE),
                                                                    transaction.amount,
                                                                    transaction.src_dest,
                                                                    transaction.purpose))
                        else:
                            DBM().insert(transaction)

                    self.load()

                fileDlg.Destroy()
                self._fire_event_transfer_changed()

        wDlg.Destroy()

    def __onTransactionRightClick(self, evt):
        """
        Triggered when right clicked on transaction item
        :return:
        """
        menu = wx.Menu()
        menu.Append(self.ID_POPUPMENU_CREATE_INVERTED_TA, String.LBL_MENU_TA_CREATE)
        menu.Append(self.ID_POPUPMENU_COPY_TA, String.LBL_MENU_TA_COPY)
        menu.Append(self.ID_POPUPMENU_CREATE_RULE_CATEGORY, String.LBL_MENU_CREATE_RULE_CAT)
        menu.Append(self.ID_POPUPMENU_CREATE_INVESTMENT, "Create investment")

        self.lst_transactions.PopupMenu(menu)

    def __onCreateInvertedTransaction(self, evt):
        """
        Triggered when menu item for creating an inverted transcation is clicked
        :param evt:
        """
        transaction = self.lst_transactions.getSelectedListItem()  # type: Transaction

        dlg = TransactionEditDialog(self)
        dlg.setDate(transaction.date)
        dlg.setAmount(transaction.amount * -1)
        dlg.setWallet(transaction.wallet.name)
        dlg.setFactor(transaction.factor)
        dlg.setPurpose(transaction.purpose)
        dlg.setComment("Inverted {}".format(transaction.purpose))
        if dlg.ShowModal() == wx.ID_OK:
            self.load()
        dlg.Destroy()
        self._fire_event_transfer_changed()

    def __onCreateTransactionCopy(self, evt):
        """
        Triggered when menu item for copying a transcation is clicked
        :param evt:
        """
        transaction = self.lst_transactions.getSelectedListItem()  # type: Transaction

        dlg = TransactionEditDialog(self)
        dlg.setDate(transaction.date)
        dlg.setAmount(transaction.amount)
        dlg.setWallet(transaction.wallet.name)
        dlg.setPurpose(transaction.purpose)
        dlg.setComment(transaction.comment)
        dlg.setFactor(transaction.factor)
        dlg.setCategory(transaction.category.name)
        if dlg.ShowModal() == wx.ID_OK:
            self.load()
        dlg.Destroy()
        self._fire_event_transfer_changed()

    def _on_create_investment(self, evt):
        """
        Triggered when menu item for copying a transcation is clicked
        :param evt:
        """
        transaction = self.lst_transactions.getSelectedListItem()  # type: Transaction

        dlg = InvestmentEditDialogImpl(self)
        dlg.set_cost(transaction.amount * -1)
        dlg.set_date(CommonFunctions.datetimeToWxDateTime(transaction.date))

        if dlg.ShowModal() == wx.ID_OK:
            self._fire_event_transfer_changed()
            self.load()
        dlg.Destroy()
        self._fire_event_transfer_changed()

    def __onCreateRuleCategory(self, evt):
        """
        Triggered when menu item for creating category rule from this transaction
        :param evt:
        """
        # get selected transcation
        transaction = self.lst_transactions.getSelectedListItem()  # type: Transaction

        # determine attributes of a transaction and show them in a SingleChoiceDialog
        dlgCondField = wx.SingleChoiceDialog(self,
                                             String.DLG_MSG_COND_FIELD,
                                             String.DLG_TITLE_COND_FIELD,
                                             RULE_CONDITION_FIELDS.enums)

        if dlgCondField.ShowModal() == wx.ID_OK:
            # get selected column
            condFieldName = dlgCondField.GetStringSelection()

            # show category rule edit dialog
            categoryRuleEditDlg = CategoryRuleEditDialog(self)
            categoryRuleEditDlg.setConditionField(condFieldName)
            categoryRuleEditDlg.setContains(getattr(transaction, condFieldName))

            if categoryRuleEditDlg.ShowModal() == wx.ID_OK:
                transaction.category = categoryRuleEditDlg.categoryRule.category
                DBM().update(transaction)
                wx.PostEvent(self.GetEventHandler(), self.UpdateCategoryRulesEvent(self.GetId()))
                self.load()

    def __onItemDataChanged(self, evt):
        """
        Triggered when the date changes
        :param evt:
        :return:
        """
        self.load()

    def __onKeyDownOnItem(self, evt):
        """
        Triggered when a key is pressed with focus on transaction list
        :param evt:
        :return:
        """
        if evt.GetKeyCode() == wx.WXK_DELETE:
            self.__onRemoveTransaction(evt)

    def __on_scan(self, evt):
        with wx.BusyInfo("Please wait while auto importing account statements..."):
            AccountStatementParser.auto_discover()
        self._fire_event_transfer_changed()
        self.load()

    def __onAddTransaction(self, evt):
        """
        Triggered when tool 'add transaction' is clicked
        :param evt:
        """
        dlg = TransactionEditDialog(self)
        if dlg.ShowModal() == wx.ID_OK:
            self._fire_event_transfer_changed()
            self.load()
        dlg.Destroy()

    def __onEditTransaction(self, evt):
        """
        Triggered when a transaction is double clicked
        :param evt:
        """
        if self.lst_transactions.lst.isItemSelected():
            dlg = TransactionEditDialog(self, self.lst_transactions.getSelectedListItem())
            if dlg.ShowModal() == wx.ID_OK:
                self._fire_event_transfer_changed()
                self.load()
            dlg.Destroy()

    def __onRemoveTransaction(self, evt):
        """
        Triggered when button for removing transaction or key [Del] is pressed
        :param evt:
        :return:
        """
        dlg = wx.MessageDialog(self,
                               message=String.DLG_MSG_TA_DEL,
                               caption=String.DLG_TITLE_TA_DEL,
                               style=wx.YES_NO)
        if dlg.ShowModal() == wx.ID_YES:
            for transaction in self.lst_transactions.getSelectedListItems():
                DBM().delete(transaction)
            self._fire_event_transfer_changed()
            self.load()

    def __onTransactionSelectionChanged(self, evt):
        """
        Triggered when item selection in transaction list changes
        :param evt:
        """
        self.btn_edit.Enable(self.lst_transactions.lst.isItemSelected())
        self.btn_rem.Enable(self.lst_transactions.lst.isItemSelected())

    def __onCategoryListKeyUp(self, evt):
        """
        Triggered when a key is pressed
        :param evt:
        """
        if evt.GetKeyCode() == wx.WXK_NUMPAD_ADD:
            dlg = TransactionEditDialog(self)
            dlg.setCategory(self.lst_categories.GetItemText(self.lst_categories.GetFirstSelected(), 0))
            if dlg.ShowModal() == wx.ID_OK:
                self.load()
            dlg.Destroy()

    # ----- END OF EVENTS -----

    @staticmethod
    def getTransactionColumnValue(transaction, column):
        """
        :param transaction: the chosen transaction
        :type transaction: Transaction
        :param column: column number
        :type column: int
        :return: Returns the value of the transaction data for a given column
        :rtype: str
        """
        if column == 0:
            return transaction.date.strftime(String.FMT_DATE)
        elif column == 1:
            return "{:.2f}".format(transaction.amount)
        elif column == 2:
            return "{:.2f}".format(transaction.factor)
        elif column == 3:
            return transaction.src_dest or "-"
        elif column == 4:
            return transaction.purpose or "-"
        elif column == 5:
            if transaction.wallet is not None:
                return transaction.wallet.name
            else:
                return "-"
        elif column == 6:
            if transaction.category is not None:
                return transaction.category.name
            else:
                return "-"
        elif column == 7:
            return transaction.comment or "-"

    @staticmethod
    def getSortValue(transaction, column):
        """
        :param transaction: the chosen transaction
        :type transaction: Transaction
        :param column: column number
        :type column: int
        :return: Returns the value to sort for the transaction data for a given column
        :rtype: str
        """
        if column == 0:
            return transaction.date.strftime(String.FMT_DATE)
        elif column == 1:
            return transaction.amount
        elif column == 2:
            return transaction.factor
        elif column == 3:
            return transaction.src_dest or "-"
        elif column == 4:
            return transaction.purpose or "-"
        elif column == 5:
            if transaction.wallet is not None:
                return transaction.wallet.name
            else:
                return "-"
        elif column == 6:
            if transaction.category is not None:
                return transaction.category.name
            else:
                return "-"
        elif column == 7:
            return transaction.comment or "-"


if __name__ == "__main__":
    app = wx.App()
    mainFrame = wx.Frame(None, size=(1000, 800))
    mainFrame.pnl = TransferPanel(mainFrame)
    mainFrame.Show()
    mainFrame.pnl.load()
    app.MainLoop()
