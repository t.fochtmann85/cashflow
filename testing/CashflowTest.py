import os
import unittest

import mock
import wx

from gui.CashflowMainFrame import CashflowMainFrameImpl


class CashflowTest(unittest.TestCase):

    def setUp(self):
        self.db_path = os.path.join(os.getcwd(), "res", "cashflow_test.db")
        self.app = wx.App()

    def tearDown(self):
        self.app.Destroy()

    @mock.patch('db.DatabaseManager.MarkupHelper.getDbPath')
    def test_gui(self, mock_getDbPath):
        mock_getDbPath.return_value = self.db_path
        mf = CashflowMainFrameImpl()
        mf.Show()
        mf.load()
        self.app.MainLoop()


if __name__ == '__main__':
    unittest.main()
