#  -*- coding: cp1252 german encoding -*-
import datetime

import wx
import wx.grid

from db.DatabaseService import CategoryService, TransactionService
from db.DbModelClasses import Category
from gui import GUIHelper

__author__ = "Tobias Fochtmann"


class ReportPopulator:
    """
    class holding methods for filling a grid
    """

    def __init__(self, colFormatStr):
        """
        Constructor
        :param colFormatStr: formatting string for column header
            (using strftime() on datetime obj)
        :type colFormatStr: basestring
        """
        self.datesList = self.getDatesList()
        self.categoryList = self.getCategoryList()
        self.categoryLeafSumDict = dict()
        self.grid = None
        self.gridCreated = False
        self.colHeaderSet = False
        self.colFormatStr = colFormatStr

    def getDatesList(self):
        raise NotImplementedError("")

    def getCategoryList(self):
        """
        :return: first level of categories
        :rtype: list[dbmodel.DbModelClasses.Category]
        """
        return CategoryService.getOrphans()

    def getCategorySumForPeriod(self, category, startDate, endDate):
        """
        Determines the sum of transactions for a certain period of time for the given category
         Saves into dictionary (sum of parent category is sum children)
        :param category: the category
        :type category: Category
        :param startDate: start date of period
        :type startDate: datetime.datetime
        :param endDate: end date of period
        :type endDate: datetime.datetime
        :return: the sum of transactions
        :rtype: float
        """
        if category.name == "Unbekannt":
            return 0.0
        if len(category.children) > 0:
            return sum([self.getCategorySumForPeriod(child, startDate, endDate) for child in category.children])
        elif startDate in self.categoryLeafSumDict.get(category.name, {}):
            return self.categoryLeafSumDict.get(category.name).get(startDate)
        else:
            sum_ = TransactionService.getSumForCategoryBetween(category, startDate, endDate)
            if category.name not in self.categoryLeafSumDict:
                self.categoryLeafSumDict[category.name] = dict()
            self.categoryLeafSumDict[category.name][startDate] = sum_

            return sum_

    def populate(self, grid):
        """
        fills grid
        :param grid: the grid to fill
        :type grid: wx.grid.Grid
        """
        if not self.gridCreated:
            self.grid = grid
            self.grid.CreateGrid(0, len(self.datesList))

            # set dimensions of grid (+ 1 because of total)
            self.grid.SetColLabelSize(24)
            self.grid.SetColLabelAlignment(wx.ALIGN_CENTER, wx.ALIGN_CENTER)
            self.grid.SetRowLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTER)
            self.gridCreated = True
        else:
            self.grid.DeleteRows(0, self.grid.GetNumberRows())

        # append total line
        row = self.grid.GetNumberRows()
        self.grid.AppendRows(1)

        # set row header
        self.grid.SetRowLabelValue(row, "Total")
        cellAttr = wx.grid.GridCellAttr()
        cellAttr.SetTextColour(wx.WHITE)
        bgCol = wx.BLACK
        cellAttr.SetBackgroundColour(bgCol)
        font = GUIHelper.Fontfactory.changeFont(self.grid.GetCellFont(0, 0), weight=wx.FONTWEIGHT_BOLD)
        cellAttr.SetFont(font)
        self.grid.SetRowAttr(row, cellAttr)

        for i, (startDate, endDate) in enumerate(self.datesList):
            periodSum = TransactionService.getSumOfPeriod(startDate, endDate) or 0.0
            self.grid.SetCellValue(row, i, "{:.2f}".format(periodSum))
            cellAttr = wx.grid.GridCellAttr()
            cellAttr.SetAlignment(wx.ALIGN_RIGHT, wx.ALIGN_CENTER)
            cellAttr.SetReadOnly(True)
            if periodSum < 0:
                cellAttr.SetTextColour(wx.ColourDatabase().Find("RED"))
            else:
                cellAttr.SetTextColour(wx.ColourDatabase().Find("GREEN"))
            self.grid.SetAttr(row, i, cellAttr)

        # add lines
        self._populateLines(self.categoryList, 0)

    def _populateLines(self, categoryList, indent):
        """
        Adds a new line and fill cells
        :param categoryList: list of categories (siblings)
        :type categoryList: list[Category]
        :param indent: indentation before category name (structuring reasons
        :type indent: int
        """
        # iterate through categories
        for category in categoryList:  # type: Category
            wx.Yield()
            if category.name == "Unbekannt":
                continue
            # add row
            row = self.grid.GetNumberRows()
            self.grid.AppendRows(1)
            self.grid.SetRowLabelSize(150)

            # set row header
            self.grid.SetRowLabelValue(row, indent * " " + category.name)
            self._populateLine(category, row)
            self.colHeaderSet = True

            if len(category.children) > 0:
                cellAttr = wx.grid.GridCellAttr()
                cellAttr.SetTextColour(wx.WHITE)
                if indent == 0:
                    bgCol = wx.BLACK
                elif indent == 2:
                    bgCol = wx.ColourDatabase().Find("DIM GREY")
                elif indent == 4:
                    bgCol = wx.ColourDatabase().Find("GREY")
                else:
                    bgCol = wx.ColourDatabase().Find("LIGHT GREY")
                cellAttr.SetBackgroundColour(bgCol)
                font = GUIHelper.Fontfactory.changeFont(self.grid.GetCellFont(0, 0), weight=wx.FONTWEIGHT_BOLD)
                cellAttr.SetFont(font)
                self.grid.SetRowAttr(row, cellAttr)

            # process next row
            self._populateLines(category.children, (indent + 2))

    def _populateLine(self, category, row):
        """
        Fills cells of line
        :param category: the category
        :type category: Category
        :param row: the current row index
        :type row: int
        """
        # iterate through columns
        for i, (startDate, endDate) in enumerate(self.datesList):
            # set column header
            if not self.colHeaderSet:
                self.grid.SetColLabelValue(i, startDate.strftime(self.colFormatStr))
                self.grid.SetColSize(i, 70)

            # get sum for this category
            sum_ = self.getCategorySumForPeriod(category,
                                                startDate - datetime.timedelta(seconds=1),
                                                endDate)
            self.grid.SetCellValue(row, i, "{:.2f}".format(sum_))
            cellAttr = wx.grid.GridCellAttr()
            cellAttr.SetAlignment(wx.ALIGN_RIGHT, wx.ALIGN_CENTER)
            cellAttr.SetReadOnly(True)
            if sum_ < 0:
                cellAttr.SetTextColour(wx.ColourDatabase().Find("CORAL"))
            self.grid.SetAttr(row, i, cellAttr)


class ReportPopulatorMonth(ReportPopulator):
    """
    Class provides information about monthly sum of transactions for each category
    """

    def __init__(self):
        """
        Constructor
        """
        ReportPopulator.__init__(self, "%b %y")

    @staticmethod
    def month_year_iter(start_month, start_year, end_month, end_year):
        """
        Iterates through months of every year
        :return: tuples of year, month
        """
        # get start month as absolute months since A.D
        ym_start = 12 * start_year + start_month - 1
        # get end month as absolute months since A.D
        ym_end = 12 * end_year + end_month - 1

        # iterate over range
        for ym in range(ym_start, ym_end + 1):
            y, m = divmod(ym, 12)
            yield y, m + 1

    def getDatesList(self):
        """
        :return: tuples of (start date, end date,) to sum up categories
        """
        firstDate = TransactionService.getFirstDate()
        firstYear = firstDate.year
        firstMonth = firstDate.month
        today = datetime.datetime.now()

        yearMonthList = list()
        for ym in ReportPopulatorMonth.month_year_iter(firstMonth, firstYear, today.month + 1, today.year):
            yearMonthList.append(ym)

        datesList = list()
        for i, (y, m) in enumerate(yearMonthList):
            if i != len(yearMonthList) - 1:
                startDate = datetime.datetime(year=y, month=m, day=1)
                endDate = datetime.datetime(year=yearMonthList[i + 1][0], month=yearMonthList[i + 1][1],
                                            day=1) - datetime.timedelta(seconds=1)
                datesList.append((startDate, endDate))

        return datesList


class ReportPopulatorYear(ReportPopulator):
    """
    Class provides information about yearly sum of transactions for each category
    """

    def __init__(self):
        """
        Constructor
        """
        ReportPopulator.__init__(self, "%Y")

    def getDatesList(self):
        """
        :return: tuples of (start date, end date,) to sum up categories
        """
        firstDate = TransactionService.getFirstDate()
        today = datetime.datetime.now()
        datesList = list()

        for y in range(firstDate.year, today.year + 1):
            startDate = datetime.datetime(year=y, month=1, day=1)
            endDate = datetime.datetime(year=y + 1, month=1, day=1) - datetime.timedelta(seconds=1)
            datesList.append((startDate, endDate))

        return datesList


class ReportPopulatorAllTime(ReportPopulator):
    """
    Class provides information about sum of transactions for each category
    """

    def __init__(self):
        """
        Constructor
        """
        ReportPopulator.__init__(self, "All time")

    def getDatesList(self):
        """
        :return: tuples of (start date, end date,) to sum up categories
        """
        firstDate = TransactionService.getFirstDate()
        today = datetime.datetime.now()
        datesList = list()
        datesList.append((firstDate, today))

        return datesList
