from db.DatabaseManager import DatabaseManager
from db.DbModelClasses import Investment, Bond


def do():
    session = DatabaseManager().session

    # _delete_crypto_investments(session)
    _delete_crypto_bonds(session)


def _delete_crypto_bonds(session):
    # 2 - delete crypto bonds
    for bond in session.query(Bond).filter(Bond.type == "Crypto").all():
        session.delete(bond)
    session.commit()


def _delete_crypto_investments(session):
    # 1 - crypot investments
    subquery = session.query(Bond.id).filter(Bond.type == "Crypto").subquery()
    for inv in session.query(Investment).filter(Investment.bond_id.in_(subquery)).all():
        session.delete(inv)
    session.commit()


if __name__ == '__main__':
    do()
