#  -*- coding: cp1252 german encoding -*-
from typing import Union

import wx
import wx.dataview
import wx.lib.newevent

from db.DatabaseService import WalletService, BondService
from db.DbModelClasses import Wallet
from gui.dlg.NumCtrlDialog import NumCtrlDialog
from gui.dlg.TransactionEditDialog import TransactionEditDialog
from gui.dlg.WalletEditDialog import WalletEditDialog

__author__ = "Tobias Fochtmann"

from gui.pnl.AccountPanelBase import AccountPanelBase


class WalletPanel(AccountPanelBase):
    """
    Panel giving overview over all wallets
    """
    UpdateTransactionsEvent, EVT_UPDATE_TRANSACTIONS = wx.lib.newevent.NewCommandEvent()

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window
        """
        super(WalletPanel, self).__init__(parent, WalletEditDialog)
        self._init_load()

    def _init_load(self):
        """
        Load initial values, set up initial states
        """
        # set up tree list
        self._tree_list.AppendColumn("Name", flags=0)
        self._tree_list.AppendColumn("Balance", width=100, align=wx.ALIGN_RIGHT, flags=0)
        self._tree_list.AppendColumn("Last transaction", width=100, align=wx.ALIGN_CENTER, flags=0)

    def load(self):
        """
        Adds info about the wallets to the tree list listctrl
        """
        self._tree_list.DeleteAllItems()
        groups = [
            "Cash",
            "Giro",
            "Credit Card",
            "Miscellaneous",
            "Building society savings",
            "Annuity insurance",
        ]

        total_balance = 0.0

        for group_name in groups:  # type: str, Union[WalletService, BondService]
            # get items
            items = WalletService.get_items_of_type(group_name)

            if len(items) > 0:
                # init sum
                group_balance = 0.0

                # create node
                group_node = self._tree_list.AppendItem(self._tree_list.GetRootItem(), group_name)

                # iterate through items
                for item in items:  # type: Wallet
                    # add node
                    node = self._tree_list.AppendItem(group_node, item.name)

                    # get balance
                    balance = WalletService.get_balance(item)
                    group_balance += balance

                    # set balance
                    self._tree_list.SetItemText(node, 1, f"{balance:.2f} �")

                    # set last transaction
                    self._tree_list.SetItemText(node, 2, WalletService.get_last_transaction_date(item))

                    # set reference to object
                    self._tree_list.SetItemData(node, item)

                self._tree_list.SetItemText(group_node, 1, f"{group_balance:.2f} �")
                total_balance += group_balance
                self._tree_list.Expand(group_node)

        total_node = self._tree_list.AppendItem(self._tree_list.GetRootItem(), "Total")
        self._tree_list.SetItemText(total_node, 1, f"{total_balance:.2f} �")

    def _fire_event_update_transactions(self):
        """
        Triggers an event for updating the wallet balance (in other view)
        """
        wx.PostEvent(self.GetEventHandler(), self.UpdateTransactionsEvent(self.GetId()))

    # ----- EVENTS ------------

    def _handle_balance_update(self):
        """
        Updates the current wallet balance (creating an transaction)
        """
        item = self._tree_list.GetSelection()

        if item.IsOk():
            wallet = self._tree_list.GetItemData(item)  # type: Wallet

            # show dialog for inserting a new balance
            new_bal_dlg = NumCtrlDialog(self, f"Current balance for wallet '{wallet.name}'")
            wallet_saldo = WalletService.get_balance(wallet)
            new_bal_dlg.setValue(wallet_saldo)

            # if dialog was terminated with OK
            if new_bal_dlg.ShowModal() == wx.ID_OK:
                diff_balance = new_bal_dlg.getValue() - wallet_saldo
                if diff_balance != 0:
                    # create a new transaction
                    new_ta_dlg = TransactionEditDialog(self)
                    new_ta_dlg.setAmount(diff_balance)
                    new_ta_dlg.setWallet(wallet.name)

                    # if dialog terminates with OK
                    if new_ta_dlg.ShowModal() == wx.ID_OK:
                        self._fire_event_update_transactions()
                        self.load()

    def _on_key_up(self, evt: wx.KeyEvent):
        """
        :param evt: Triggered when key is pressd
        """
        if evt.GetKeyCode() == wx.WXK_F5:
            self._handle_balance_update()
        else:
            super(WalletPanel, self)._on_key_up(evt)
