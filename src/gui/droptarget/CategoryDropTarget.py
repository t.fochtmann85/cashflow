#  -*- coding: cp1252 german encoding -*-
import ast

import wx

from db.DatabaseService import TransactionService
from db.DbModelClasses import Category

__author__ = "Tobias Fochtmann"


class CategoryDropTarget(wx.DropTarget):
    """
    Class for handling drop events at the given window
    """

    def __init__(self, ctrl, *args, **kwargs):
        """
        Constructor
        :param ctrl: target control
        """
        wx.DropTarget.__init__(self, *args, **kwargs)
        self.__ctrl = ctrl

        self.__data = wx.TextDataObject()
        self.SetDataObject(self.__data)

    def OnDragOver(self, x, y, d):
        item = self.__ctrl.HitTest((x, y))[0]
        if item != wx.NOT_FOUND:
            self.__ctrl.Select(item)
        return wx.DragMove

    def OnData(self, x, y, data):
        if self.GetData():
            item = self.__ctrl.HitTest((x, y))[0]

            if item != wx.NOT_FOUND:
                category = self.__ctrl.getCategoryByItem(item)  # type: Category
                for transaction_id in ast.literal_eval(self.__data.GetText()):
                    TransactionService.updateCategoryOfId(transaction_id, category.id)

        return data
