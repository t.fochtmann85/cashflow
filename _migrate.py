from pathlib import Path

import alembic.command
import alembic.config


def create_revision():
    path_alembic_ini = Path.cwd() / "alembic.ini"

    config = alembic.config.Config(path_alembic_ini)
    config.set_main_option("script_location", "alembic")
    config.set_main_option("sqlalchemy.url", r"sqlite:///res\cashflow.db")

    alembic.command.revision(config, autogenerate=True)
    # alembic.command.downgrade(config, 'df42dfa39c94')


if __name__ == '__main__':
    create_revision()
