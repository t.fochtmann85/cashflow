#  -*- coding: cp1252 german encoding -*-
import logging

import wx

from db.DatabaseManager import DatabaseManager as DBM
from db.DatabaseService import SplitRuleService
from db.DbModelClasses import SplitRule, RULE_CONDITION_FIELDS
from gui.bmp import images
from resources import String

__author__ = "Tobias Fochtmann"


class SplitRuleEditDialog(wx.Dialog):
    """
    Dialog for editing a split rule
    """
    PADDING = 10

    def __init__(self, parent, splitRule=None):
        """
        Constructor
        :param parent: parent window
        :param splitRule: rule category
        :type splitRule: SplitRule
        """
        wx.Dialog.__init__(self, parent, style=wx.STAY_ON_TOP | wx.CAPTION | wx.SYSTEM_MENU)
        self.SetIcon(images.SYMBOL_PARAGRAPH.GetIcon())

        if splitRule is None:
            self.SetTitle("Add a new split rule")
        else:
            self.SetTitle("Edit split rule")
            assert isinstance(splitRule, SplitRule), \
                "Argument ´splitRule´ needs to be of type ´DbModelClasses.SplitRule´"

        self.splitRule = splitRule

        self._createControls()
        self._setToolTips()
        self._setLayout()
        self._bindEvents()
        self._initLoad()

        self.CenterOnParent()

    def _createControls(self):
        """
        Creates all used widgets
        """
        # sizers
        self.szr_main = wx.GridBagSizer(hgap=self.PADDING, vgap=self.PADDING)

        # labels
        self.lbl_condField = wx.StaticText(self, label=String.LBL_FIELD_COND)
        self.lbl_cont = wx.StaticText(self, label=String.LBL_FIELD_CONTAINS)
        self.lbl_parseField = wx.StaticText(self, label=String.LBL_FIELD_PARSE)
        self.lbl_regex = wx.StaticText(self, label=String.LBL_FIELD_REGEX)

        # text inputs
        self.cmb_fields = wx.ComboBox(self, style=wx.CB_READONLY, choices=RULE_CONDITION_FIELDS.enums)
        self.txt_content = wx.TextCtrl(self, size=(300, 100), style=wx.TE_MULTILINE)
        self.cmb_fields2 = wx.ComboBox(self, style=wx.CB_READONLY,
                                       choices=RULE_CONDITION_FIELDS.enums)
        self.txt_regex = wx.TextCtrl(self, size=(300, 100), style=wx.TE_MULTILINE)

        # buttons
        self.btn_apply = wx.BitmapButton(self, bitmap=images.APPLY.GetBitmap())
        self.btn_discard = wx.BitmapButton(self, bitmap=images.DISCARD.GetBitmap())

    def _setToolTips(self):
        """
        Set tool tips
        """
        self.btn_apply.SetToolTip("Write changes to database and close the dialog")
        self.btn_apply.SetToolTip("Discard changes and close the dialog")

    def _setLayout(self):
        """
        Sets the layout of the widgets
        """
        line = 0
        self.szr_main.Add(self.lbl_condField, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.TOP,
                          self.PADDING)
        self.szr_main.Add(self.cmb_fields, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.TOP | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_cont, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.txt_content, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_parseField, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.cmb_fields2, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_regex, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.txt_regex, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.btn_apply, (line, 0), (1, 1), wx.ALIGN_RIGHT | wx.BOTTOM, self.PADDING)
        self.szr_main.Add(self.btn_discard, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.BOTTOM, self.PADDING)

        self.SetSizer(self.szr_main)
        self.Fit()

    def _bindEvents(self):
        """
        Binds events to actions
        """
        self.btn_apply.Bind(wx.EVT_BUTTON, self.__onApply)
        self.btn_discard.Bind(wx.EVT_BUTTON, self.__onDiscard)

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        if self.splitRule is not None:
            self.cmb_fields.SetValue(self.splitRule.cond_field)
            self.txt_content.SetValue(self.splitRule.contains)
            self.cmb_fields2.SetValue(self.splitRule.parse_field)
            self.txt_regex.SetValue(self.splitRule.iter_regex)

    def _checkValues(self):
        """
        Validates user data
        :return: True if all necessary data is set
        :rtype: bool
        """
        if not self.cmb_fields.GetValue():
            logging.warning("No field selected")
            return False

        if not self.txt_content.GetValue():
            logging.warning("No containing text")
            return False

        if not self.cmb_fields2.GetValue():
            logging.warning("No valid parsing field selected")
            return False

        if not self.txt_regex.GetValue():
            logging.warning("No valid regular expression set")
            return False

        return True

    def _getUpdatedTransactionFromUI(self):
        """
        :return: Returns a dictionary containing the current values of the widgets
        :rtype: SplitRule
        """
        if self.splitRule is None:
            splitRule = SplitRule()
        else:
            splitRule = self.splitRule

        splitRule.cond_field = self.cmb_fields.GetValue()
        splitRule.contains = self.txt_content.GetValue()
        splitRule.parse_field = self.cmb_fields2.GetValue()
        splitRule.iter_regex = self.txt_regex.GetValue()

        return splitRule

    # ----- EVENTS -----

    def __onApply(self, evt):
        """
        Triggered when button apply is pressed
        :param evt:
        """
        if self._checkValues():
            splitRule = self._getUpdatedTransactionFromUI()

            # INSERT
            if self.splitRule is None:
                # check if rule with the same values already exist
                if SplitRuleService.hasRule(splitRule):
                    logging.info("A split rule for field '{}' with value '{}' already exists. Alter values.".format(
                        splitRule.cond_field,
                        splitRule.contains))
                else:
                    DBM().insert(splitRule)
                    self.splitRule = splitRule
                    self.EndModal(wx.ID_OK)
            # UPDATE
            else:
                DBM().update(splitRule)
                self.EndModal(wx.ID_OK)

    def __onDiscard(self, evt):
        """
        Triggerd when button discard is pressed
        :param evt:
        """
        self.EndModal(wx.ID_CANCEL)


if __name__ == "__main__":
    app = wx.App()
    mainFrame = SplitRuleEditDialog(None)
    mainFrame.Show()
    app.MainLoop()
