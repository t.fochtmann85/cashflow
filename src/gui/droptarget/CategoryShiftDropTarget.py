#  -*- coding: cp1252 german encoding -*-
__author__ = "Tobias Fochtmann"

import wx


class CategoryShiftDropTarget(wx.DropTarget):
    """
    Class for handling drop events at the given window
    """

    def __init__(self, ctrl, *args, **kwargs):
        """
        Constructor
        :param ctrl: target control
        :type ctrl: gui.ctrl.CategoryConfigTreeCtrl.CategoryConfigTreeCtrl
        """
        wx.DropTarget.__init__(self, *args, **kwargs)
        self.__ctrl = ctrl

        self.__data = wx.TextDataObject()
        self.SetDataObject(self.__data)

    def OnDragOver(self, x, y, d):
        item = self.__ctrl.HitTest((x, y))[0]
        if item is not None:
            self.__ctrl.SelectItem(item)
        return wx.DragMove

    def OnData(self, x, y, data):
        if self.GetData():
            item = self.__ctrl.HitTest((x, y))[0]

            if item != wx.NOT_FOUND:
                wx.CallAfter(self.__ctrl.updateCategoryParent, int(self.__data.GetText()), item)

        return data
