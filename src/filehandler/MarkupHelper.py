#  -*- coding: cp1252 german encoding -*-
import codecs
import csv
import os.path

import chardet
import yaml

import constants

__author__ = "Tobias Fochtmann"


def getDbPath():
    """
    Returns the absolute file path of the database
    :return:
    """
    return os.path.join(os.getcwd(), "..", getYamlPathValue(constants.FILE_CFG, ["files", "db"]))


def getYaml(absPathToYaml):
    """
    Reads a yaml file and gives back the content as a dictionary
    :param absPathToYaml: absolute path of the yaml file
    """
    content = dict()

    if os.path.isfile(absPathToYaml):
        with codecs.open(absPathToYaml, "r") as f:
            content = yaml.safe_load(f)

    return content


def getYamlPathValue(absPathToYaml, keyList, default=None):
    """
    Returns the value for a given path (list of keys for surfing through a dictionary)
    :param absPathToYaml: absolute path of the yaml file
    :param keyList: list of keys to find in yaml dictionary
    :param default: the default value to return if the last key was not found
    :return: whatever value is ath the end of the tree
    """
    pathValue = getYaml(absPathToYaml)

    for i, key in enumerate(keyList):
        if i == len(keyList) - 1:
            pathValue = pathValue.get(key, default)
        else:
            pathValue = pathValue.get(key, dict())

    return pathValue


def getFileContent(filePath):
    """
    Returns the full content of the file (if any)
    :param filePath: absolute file path
    """
    fileContent = ""

    if os.path.isfile(filePath):
        with open(filePath, "r") as f:
            encoding = chardet.detect(f.read()).get("encoding")

        with codecs.open(filePath, "r", encoding=encoding) as f:
            fileContent = f.read()

    return fileContent


def getCsvFileContent(filePath, sep):
    with open(filePath, "rb") as f:
        encoding = chardet.detect(f.read()).get("encoding")

    with open(filePath, encoding=encoding) as f:
        lines = list()
        for row in csv.reader(f, delimiter=str(sep)):
            lines.append(row)

    return lines


