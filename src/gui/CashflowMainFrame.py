#  -*- coding: cp1252 german encoding -*-
__author__ = "TobiasFochtmann"

import logging

import wx
from wx import Toolbook
from wx.richtext import RichTextCtrl

from gui.GuiLogHandler import GuiLogHandler
from gui.bmp import images
from gui.events import EVT_TRANSACTION_CHANGED
from gui.pnl.BondPanel import BondPanelImpl
from gui.pnl.ReportPanel import ReportPanel
from gui.pnl.SettingsPanel import SettingsPanel
from gui.pnl.TransactionPanel import TransactionPanel
from gui.pnl.TransferPanel import TransferPanel
from gui.pnl.WalletPanel import WalletPanel
from resources import String


class CashflowMainFrame(wx.Frame):
    """
    Class for setting up the main frame
    """

    def __init__(self) -> None:
        """
        The constructor
        """
        wx.Frame.__init__(
            self,
            parent=None,
            title=String.APP_NAME,
            size=(1920, 1000),
            style=wx.MINIMIZE_BOX | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.CLIP_CHILDREN | wx.RESIZE_BORDER | wx.MAXIMIZE_BOX
        )
        self.SetIcon(images.CASHIER.GetIcon())

        self._create_controls()
        logging.getLogger().addHandler(GuiLogHandler(self.log))
        self._set_tool_tips()
        self._set_layout()
        self._bind_events()
        self._init_load()

        self.SetMinSize(self.GetSize())
        self.CenterOnScreen()
        self.Maximize()

    def _create_controls(self) -> None:
        """
        Creates all used widgets
        """
        il = wx.ImageList(64, 64)
        img_id_accounts = il.Add(images.TOOL_WALLET_OPEN.GetIcon())
        img_id_transactions = il.Add(images.TOOL_TRANSACTION.GetIcon())
        imgIdSettings = il.Add(images.CONTROL_PANEL.GetIcon())
        imgIdReport = il.Add(images.CHART_LINE.GetIcon())

        # sizers
        self.szr = wx.GridBagSizer(vgap=1, hgap=1)

        # panel
        self.pnl = wx.Panel(self)
        self.tb = Toolbook(self.pnl)
        self.tb.SetImageList(il)
        self._pnl_accounts = wx.SplitterWindow(self.tb)
        self._pnl_wallets = WalletPanel(self._pnl_accounts)
        self._pnl_bonds = BondPanelImpl(self._pnl_accounts)
        self._pnl_transaction = TransactionPanel(self.tb)
        self.pnl_settings = SettingsPanel(self.tb)
        self.pnl_report = ReportPanel(self.tb)

        self.tb.AddPage(self._pnl_accounts, "Accounts", imageId=img_id_accounts)
        self.tb.AddPage(self._pnl_transaction, "Transactions", imageId=img_id_transactions)
        self.tb.AddPage(self.pnl_report, "Report", imageId=imgIdReport)
        self.tb.AddPage(self.pnl_settings, "Settings", imageId=imgIdSettings)

        self.log = RichTextCtrl(self.pnl, size=(400, 100), style=wx.richtext.RE_MULTILINE | wx.richtext.RE_READONLY)

    def _set_tool_tips(self):
        """
        Set the tool tips of controls
        """
        pass

    def _set_layout(self):
        """
        Sets the layout of the widgets
        """
        self._pnl_accounts.SplitVertically(self._pnl_wallets, self._pnl_bonds)

        self.szr.Add(self.tb, (0, 0), (1, 1), wx.EXPAND | wx.LEFT | wx.TOP | wx.RIGHT, 10)
        self.szr.Add(self.log, (1, 0), (1, 1), wx.EXPAND | wx.ALL, 10)
        self.szr.AddGrowableCol(0)
        self.szr.AddGrowableRow(0)

        self.pnl.SetSizerAndFit(self.szr)

    def _bind_events(self):
        """
        Binds events to actions
        """
        self._pnl_transaction.Bind(TransferPanel.EVT_UPDATE_CATEGORY_RULES, self._on_update_category_rules)
        self._pnl_transaction.pnl_transfer.Bind(EVT_TRANSACTION_CHANGED, self._on_transaction_changed)
        self._pnl_transaction.pnl_investment.Bind(EVT_TRANSACTION_CHANGED, self._on_transaction_changed)
        self._pnl_wallets.Bind(WalletPanel.EVT_UPDATE_TRANSACTIONS, self._on_update_transactions)

    def _init_load(self):
        """
        Initial stuff to do
        """
        pass

    def _on_update_category_rules(self, evt):
        raise NotImplementedError("implement in subclass")

    def _on_transaction_changed(self, evt):
        raise NotImplementedError("implement in subclass")

    def _on_update_transactions(self, evt):
        raise NotImplementedError("implement in subclass")


class CashflowMainFrameImpl(CashflowMainFrame):
    """
    Class for setting up the main frame
    """

    def load(self):
        """
        Loads the data of the current database
        """
        self._pnl_wallets.load()
        self._pnl_bonds.load()
        self._pnl_transaction.load()
        self.pnl_settings.load()
        self.pnl_report.load()

        self.tb.SetSelection(1)

    def _on_update_category_rules(self, evt):
        """
        Triggered when a new category rule was created in transaction panel
        :param evt: the triggered event
        :type evt: TransactionPanel.UpdateCategoryRulesEvent
        """
        self.pnl_settings.pnl_ruleCategory.load()

    def _on_transaction_changed(self, evt):
        """
        Triggered when a transaction was added, removed or edited
        :param evt: the triggered event
        :type evt: TransactionPanel.UpdateWalletBalanceEvent
        """
        logging.debug("Update wallet balance")
        self._pnl_wallets.load()
        self._pnl_bonds.load()
        self._pnl_transaction.load()

    def _on_update_transactions(self, evt):
        """
        Triggered when a wallet's balance has been updated
        :param evt: the triggered event
        :type evt: WalletPanel.UpdateTransactionsEvent
        """
        self._pnl_transaction.load()


if __name__ == "__main__":
    app = wx.App()
    mainFrame = CashflowMainFrameImpl()
    mainFrame.Show()
    app.MainLoop()
