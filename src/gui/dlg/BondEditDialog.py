#  -*- coding: cp1252 german encoding -*-

import logging

import typeguard as typeguard
import wx

from db.DatabaseManager import DatabaseManager as DBM
from db.DbModelClasses import Bond, BOND_TYPE, COUNTRY, SECTOR
from gui.bmp import images

__author__ = "TobiasFochtmann"


class BondEditDialog(wx.Dialog):
    """
    Dialog for editing a bond (or adding a new one)
    """

    @typeguard.typechecked
    def __init__(self, parent: wx.Window, bond: Bond = None):
        """
        Constructor
        :param parent: parent window
        :param bond: the bond to edit
        """
        wx.Dialog.__init__(
            self,
            parent,
            title="Edit bond"
        )
        self.SetIcon(images.BOND.GetIcon())

        self.bond = bond

        self._create_controls()
        self._set_tool_tips()
        self._set_layout()
        self._bind_events()

        self.CenterOnParent()

    def _create_controls(self):
        """
        Creates all used widgets
        """
        # sizers
        self.sizer = wx.GridBagSizer(hgap=10, vgap=5)

        # panel
        self.pnl = wx.Panel(self)

        # labels
        self._lbl_name = wx.StaticText(self.pnl, label="Name")
        self._lbl_type = wx.StaticText(self.pnl, label="Type")
        self._lbl_id = wx.StaticText(self.pnl, label="ISIN/Identifier")
        self._lbl_symbol = wx.StaticText(self.pnl, label="Symbol (US)")
        self._lbl_sector = wx.StaticText(self.pnl, label="Sector")
        self._lbl_country = wx.StaticText(self.pnl, label="Country")

        # text inputs
        self._txt_name = wx.TextCtrl(self.pnl, size=(200, 24))
        self._txt_id = wx.TextCtrl(self.pnl, size=(200, 24))
        self._txt_symbol = wx.TextCtrl(self.pnl, size=(200, 24))

        # combo boxes
        self._choice_type = wx.Choice(
            self.pnl,
            size=(200, 24),
            choices=sorted(BOND_TYPE.enums)
        )
        self._choice_sector = wx.Choice(
            self.pnl,
            size=(200, 24),
            choices=sorted(SECTOR.enums)
        )
        self._choice_country = wx.Choice(
            self.pnl,
            size=(200, 24),
            choices=sorted(COUNTRY.enums)
        )

        # buttons
        self._btn_apply = wx.BitmapButton(self.pnl, bitmap=images.APPLY.GetBitmap())

    def _set_tool_tips(self):
        """
        Set tool tips
        """
        self._btn_apply.SetToolTip("Apply changes to database and close dialog")

    def _set_layout(self):
        """
        Sets the layout of the widgets
        """
        border = 10
        line = 0
        self.sizer.Add(self._lbl_name, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.TOP, border)
        self.sizer.Add(self._txt_name, (line, 1), (1, 1), wx.TOP | wx.RIGHT, border)
        line += 1
        self.sizer.Add(self._lbl_type, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self.sizer.Add(self._choice_type, (line, 1), (1, 1), wx.RIGHT, border)
        line += 1
        self.sizer.Add(self._lbl_id, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self.sizer.Add(self._txt_id, (line, 1), (1, 1), wx.RIGHT, border)
        line += 1
        self.sizer.Add(self._lbl_symbol, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self.sizer.Add(self._txt_symbol, (line, 1), (1, 1), wx.RIGHT, border)
        line += 1
        self.sizer.Add(self._lbl_sector, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self.sizer.Add(self._choice_sector, (line, 1), (1, 1), wx.RIGHT, border)
        line += 1
        self.sizer.Add(self._lbl_country, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self.sizer.Add(self._choice_country, (line, 1), (1, 1), wx.RIGHT, border)
        line += 1
        self.sizer.Add(wx.StaticLine(self.pnl, size=(100, 2)), (line, 0), (1, 2), wx.EXPAND | wx.LEFT | wx.RIGHT,
                       border)
        line += 1
        self.sizer.Add(self._btn_apply, (line, 0), (1, 1), wx.ALIGN_RIGHT | wx.BOTTOM, border)

        self.pnl.SetSizerAndFit(self.sizer)
        self.SetSizerAndFit(self.sizer)

    def _bind_events(self):
        """
        Binds events to actions
        """
        self._btn_apply.Bind(wx.EVT_BUTTON, self._on_apply)

    # ----- EVENTS -----

    def _on_apply(self, evt):
        raise NotImplementedError("Implement in sub class")


class BondEditDialogImpl(BondEditDialog):
    """
    Dialog for editing a bond (or adding a new one)
    """

    def __init__(self, *args, **kwargs):
        super(BondEditDialogImpl, self).__init__(*args, **kwargs)
        self._init_load()

    def _init_load(self):
        """
        Load initial values, set up initial states
        """
        # when editing a wallet
        if self.bond is not None:
            self._txt_name.SetValue(self.bond.name)
            self._choice_type.SetStringSelection(self.bond.type)
            self._txt_id.SetValue(self.bond.iid)
            if self.bond.symbol is not None:
                self._txt_symbol.SetValue(self.bond.symbol)
            self._choice_sector.SetStringSelection(self.bond.sector)
            self._choice_country.SetStringSelection(self.bond.country)

    def _check_values(self):
        """
        Validates input values
        :return: True if all values are ok
        :rtype: bool
        """
        if not self._txt_name.GetValue():
            logging.warning("Name can't be empty")
            return False

        if not self._txt_id.GetValue():
            logging.warning("Id can't be empty")
            return False

        if not self._choice_type.GetStringSelection():
            logging.warning("Bond type can't be empty")
            return False

        if not self._choice_sector.GetStringSelection():
            logging.warning("Sector can't be empty")
            return False

        if not self._choice_country.GetStringSelection():
            logging.warning("Country can't be empty")
            return False

        return True

    # ----- EVENTS -----

    def _on_apply(self, evt):
        """
        :param evt: Triggered when button 'apply' is clicked
        :type evt: wx.CommandEvent
        """
        if self._check_values():
            # differ between update and create new
            if self.bond is None:
                # create new item
                self.bond = Bond()

            self.bond.name = self._txt_name.GetValue()
            self.bond.iid = self._txt_id.GetValue()
            self.bond.symbol = self._txt_symbol.GetValue()
            self.bond.type = self._choice_type.GetStringSelection()
            self.bond.sector = self._choice_sector.GetStringSelection()
            self.bond.country = self._choice_country.GetStringSelection()

            # update in database
            DBM().update(self.bond)

            self.EndModal(wx.ID_OK)
