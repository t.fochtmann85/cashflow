#  -*- coding: cp1252 german encoding -*-
__author__ = "TobiasFochtmann"

import hashlib
import re
from datetime import datetime, date

import wx

import db.DbModelClasses
from resources import String


def wxDateTimeToDatetime(wxDateTime):
    """
    Converts a wx.DateTime object into a datetime instance
    :param wxDateTime: the date to convert
    :type wxDateTime: wx.DateTime
    :return: Converted date
    :rtype: datetime
    """
    assert isinstance(wxDateTime, wx.DateTime), \
        "Argument �wxDateTime� needs to be of type �wx.DateTime�, but is: {} of type {}.".format(
            wxDateTime,
            type(wxDateTime)
        )

    return datetime.strptime(wxDateTime.Format(String.FMT_DATE), String.FMT_DATE)


def datetimeToWxDateTime(dt):
    """
    Converts a datetime object into a wx.DateTime instance
    :param dt: the date to convert
    :type dt: datetime
    :return: Converted date
    :rtype: wx.DateTime
    """
    assert isinstance(dt, (datetime, date)), \
        "Argument �dt� needs to be of type �datetime� or �date�, but is: {} of type {}.".format(
            dt,
            type(dt)
        )

    wxDt = wx.DateTime()
    wxDt.ParseFormat(dt.strftime(String.FMT_DATE), String.FMT_DATE)
    return wxDt


def buildHashForTransaction(transaction):
    """
    generates a unique hash for the given parsed transaction
    :param transaction: the transaction the hash should be built for
    :type transaction: db.DbModelClasses.Transaction
    :return:
    """
    assert isinstance(transaction, db.DbModelClasses.Transaction), \
        "Argument �transaction� needs to be of type �DbModelClasses.Transaction�, but is: {} of type {}.".format(
            transaction,
            type(transaction)
        )
    assert isinstance(transaction.date, datetime), \
        "Transaction should have a valid date set, but is: {} of type {}".format(transaction.date,
                                                                                 type(transaction.date))
    assert isinstance(transaction.amount, float), \
        "Transaction should have a valid amount set, but is: {} of type {}".format(transaction.amount,
                                                                                   type(transaction.amount))
    # build string for hashing
    eatString = ""
    if transaction.iban is not None:
        eatString += transaction.iban
    if transaction.bic is not None:
        eatString += transaction.bic
    eatString += transaction.date.strftime(String.FMT_DATE)
    if transaction.purpose is not None:
        eatString += transaction.purpose
    if transaction.src_dest is not None:
        eatString += transaction.src_dest
    eatString += str(transaction.amount)

    md5 = hashlib.md5()
    md5.update(eatString.encode('utf-8'))
    return md5.hexdigest()


def fromGermanDecimalToFloat(amountStr):
    """
    Converts the value from decimal string with comma as decimal separator to a python float
    :param amountStr: the amount in german notation
    :type amountStr: basestring, int, float, long
    :return: the converted numeric
    :rtype: float
    """
    if isinstance(amountStr, (float, int)):
        return amountStr
    elif amountStr is None:
        return amountStr
    else:
        assert isinstance(amountStr, str), "Argument �amountStr� needs to be of type �string�"

        amountStr = amountStr.strip()

        regex_german = "^-?\d+(\.\d{3})*,\d+"
        regex_intrntnl = "^-?\d+(\,\d{3})*.\d+"
        regex_float = "^(?P<amount>-?\d+.\d+)"

        # is german string?
        if re.search(regex_german, amountStr) is not None:
            amountStr = amountStr.replace(".", "#").replace(",", ".").replace("#", ",")

        # is international
        if re.search(regex_intrntnl, amountStr) is not None:
            amountStr = amountStr.replace(",", "")

        re_amount = re.search(regex_float, amountStr)
        if re_amount is not None:
            return float(re_amount["amount"])
