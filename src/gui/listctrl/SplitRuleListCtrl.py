#  -*- coding: cp1252 german encoding -*-
__author__ = "Tobias Fochtmann"

import wx

from db.DbModelClasses import SplitRule


class SplitRuleListCtrl(wx.ListCtrl):
    """
    List holding all split rules
    """

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window 
        """
        wx.ListCtrl.__init__(self, parent,
                             style=wx.LC_REPORT | wx.LC_VIRTUAL | wx.LC_SINGLE_SEL | wx.LC_HRULES)

        self.__items = list()

        self._setup()
        self._initLoad()

    def _setup(self):
        """
        Setup the list
        """
        self.AppendColumn("If condition field...", width=150)
        self.AppendColumn("...contains...", width=200)
        self.AppendColumn("...parse field...", width=150)
        self.AppendColumn("...with regular expression", width=500)

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        pass

    def setItems(self, items):
        """
        (Re)sets the item list
        :param items: list of list items
        """
        self.__items = items
        self.SetItemCount(len(self.__items))
        self.RefreshItems(0, self.GetItemCount() - 1)

    def getItemByIndex(self, index):
        """
        Returns the item at the given index
        :param index: the item index
        :return: the data linked to this row
        """
        if 0 <= index < self.GetItemCount():
            return self.__items[index]

    # VIRTUALNESS
    def OnGetItemText(self, item, column):
        """
        Implemnt virtual method
        :param item: the item index (row)
        :type item: long
        :param column: column number
        :type column: long
        :return: the text to be shown
        :rtype: str
        """
        splitRule = self.__items[item]  # type: SplitRule

        if column == 0:
            return splitRule.cond_field
        elif column == 1:
            return splitRule.contains
        elif column == 2:
            return splitRule.parse_field
        elif column == 3:
            return splitRule.iter_regex
        else:
            raise Exception("Unknown column {}".format(column))

    # ----- EVENTS ------------

    # ----- END OF EVENTS -----
