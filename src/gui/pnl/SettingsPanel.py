#  -*- coding: cp1252 german encoding -*-
__author__ = "Tobias Fochtmann"

import wx

from gui.listctrl.CategoryConfigTreeCtrl import CategoryConfigTreeCtrl
from gui.pnl.CategoryRuleConfigPanel import CategoryRuleConfigPanel
from gui.pnl.SplitRuleConfigPanel import SplitRuleConfigPanel


class SettingsPanel(wx.Panel):
    """
    Panel ...
    """

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window 
        """
        wx.Panel.__init__(self, parent)

        self._createControls()
        self._setToolTips()
        self._setLayout()
        self._bindEvents()
        self._initLoad()

    def _createControls(self):
        """
        Create controls and widgets
        """
        self.sizer = wx.GridBagSizer()

        self.nb = wx.Notebook(self)

        self.tree_category = CategoryConfigTreeCtrl(self.nb)
        self.pnl_ruleCategory = CategoryRuleConfigPanel(self.nb)
        self.pnl_ruleSplit = SplitRuleConfigPanel(self.nb)

        self.nb.AddPage(self.tree_category, "Category")
        self.nb.AddPage(self.pnl_ruleCategory, "Category Rule")
        self.nb.AddPage(self.pnl_ruleSplit, "Split Rule")

    def _setToolTips(self):
        """
        Set tool tips
        """
        pass

    def _setLayout(self):
        """
        Positioning all controls of the panel
        """
        self.sizer.Add(self.nb, (0, 0), (1, 1), wx.EXPAND)
        self.sizer.AddGrowableCol(0)
        self.sizer.AddGrowableRow(0)

        self.SetSizerAndFit(self.sizer)

    def _bindEvents(self):
        """
        Associate events with actions
        """
        pass

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        pass

    def load(self):
        """
        Load values
        """
        self.tree_category.load()
        self.pnl_ruleCategory.load()
        self.pnl_ruleSplit.load()

    # ----- EVENTS ------------

    # ----- END OF EVENTS -----


if __name__ == "__main__":
    app = wx.App()
    mainFrame = wx.Frame(None, size=(1000, 800))
    mainFrame.pnl = SettingsPanel(mainFrame)
    mainFrame.Show()
    app.MainLoop()
