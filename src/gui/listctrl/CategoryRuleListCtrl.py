#  -*- coding: cp1252 german encoding -*-
__author__ = "Tobias Fochtmann"

import wx

from db.DbModelClasses import CategoryRule


class CategoryRuleListCtrl(wx.ListCtrl):
    """
    Panel ...
    """

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window 
        """
        wx.ListCtrl.__init__(self, parent,
                             style=wx.LC_REPORT | wx.LC_VIRTUAL | wx.LC_SINGLE_SEL | wx.LC_HRULES)

        self.__items = list()

        self._setup()
        self._initLoad()

    def _setup(self):
        """
        Setup the list
        """
        self.AppendColumn("If condition field...", width=150)
        self.AppendColumn("...contains...", width=300)
        self.AppendColumn("...link to category...", width=200)

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        pass

    def setItems(self, items):
        """
        (Re)sets the item list
        :param items: list of list items
        :tyoe items: list[CategoryRule]
        """
        self.__items = items
        self.SetItemCount(len(self.__items))
        self.RefreshItems(0, self.GetItemCount() - 1)

    def getItemByIndex(self, index):
        """
        Returns the item at the given index
        :param index: the item index
        :return: the category rule to this row
        :rtype: CategoryRule
        """
        if 0 <= index < self.GetItemCount():
            return self.__items[index]

    # VIRTUALNESS
    def OnGetItemText(self, item, column):
        """
        Implemnt virtual method
        :param item: the item index (row)
        :type item: long
        :param column: column number
        :type column: long
        :return: the text to be shown
        :rtype: str
        """
        categoryRule = self.__items[item]  # type: CategoryRule

        if column == 0:
            return categoryRule.cond_field
        elif column == 1:
            return categoryRule.contains
        elif column == 2:
            return categoryRule.category.name
        else:
            raise Exception("Unknown column {}".format(column))

    # ----- EVENTS ------------

    # ----- END OF EVENTS -----
