#  -*- coding: cp1252 german encoding -*-

import logging

import wx

from db.DatabaseManager import DatabaseManager as DBM
from db.DatabaseService import CategoryService, CategoryRuleService
from db.DbModelClasses import CategoryRule, RULE_CONDITION_FIELDS
from gui.bmp import images
from resources import String

__author__ = "TobiasFochtmann"


class CategoryRuleEditDialog(wx.Dialog):
    """
    Dialog for editing a category rule
    """
    PADDING = 10

    def __init__(self, parent, categoryRule=None):
        """
        Constructor
        :param parent: parent window
        :param categoryRule: rule category data
        :type categoryRule: CategoryRule
        """
        wx.Dialog.__init__(self, parent, style=wx.STAY_ON_TOP | wx.CAPTION | wx.SYSTEM_MENU)
        self.SetIcon(images.SYMBOL_PARAGRAPH.GetIcon())

        if categoryRule is None:
            self.SetTitle("Add a new category rule")
        else:
            self.SetTitle("Edit category rule")
            assert isinstance(categoryRule, CategoryRule), \
                "Argument ´categoryRule´ needs to be of type ´DbModelClasses.CategoryRule´"

        self.categoryRule = categoryRule

        self._createControls()
        self._setToolTips()
        self._setLayout()
        self._bindEvents()
        self._initLoad()

        self.CenterOnParent()

    def _createControls(self):
        """
        Creates all used widgets
        """
        # sizers
        self.szr_main = wx.GridBagSizer(hgap=self.PADDING, vgap=self.PADDING)

        # labels
        self.lbl_condField = wx.StaticText(self, label=String.LBL_FIELD_COND)
        self.lbl_cont = wx.StaticText(self, label=String.LBL_FIELD_CONTAINS)
        self.lbl_category = wx.StaticText(self, label=String.LBL_FIELD_CATEGORY)

        # text inputs
        self.cmb_fields = wx.ComboBox(self, style=wx.CB_READONLY,
                                      choices=RULE_CONDITION_FIELDS.enums)
        self.txt_content = wx.TextCtrl(self, size=(300, 100), style=wx.TE_MULTILINE)
        self.cmb_category = wx.ComboBox(self, style=wx.CB_READONLY, choices=CategoryService.getLeafNames())

        # buttons
        self.btn_apply = wx.BitmapButton(self, bitmap=images.APPLY.GetBitmap())
        self.btn_discard = wx.BitmapButton(self, bitmap=images.DISCARD.GetBitmap())

    def _setToolTips(self):
        """
        Set tool tips
        """
        self.btn_apply.SetToolTip("Write changes to database and close the dialog")
        self.btn_apply.SetToolTip("Discard changes and close the dialog")

    def _setLayout(self):
        """
        Sets the layout of the widgets
        """
        line = 0
        self.szr_main.Add(self.lbl_condField, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.TOP,
                          self.PADDING)
        self.szr_main.Add(self.cmb_fields, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.TOP | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_cont, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.txt_content, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_category, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.cmb_category, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.btn_apply, (line, 0), (1, 1), wx.ALIGN_RIGHT | wx.BOTTOM, self.PADDING)
        self.szr_main.Add(self.btn_discard, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.BOTTOM, self.PADDING)

        self.SetSizer(self.szr_main)
        self.Fit()

    def _bindEvents(self):
        """
        Binds events to actions
        """
        self.btn_apply.Bind(wx.EVT_BUTTON, self.__onApply)
        self.btn_discard.Bind(wx.EVT_BUTTON, self.__onDiscard)

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        if self.categoryRule is not None:
            self.setConditionField(self.categoryRule.cond_field)
            self.setContains(self.categoryRule.contains)
            self.setCategory(self.categoryRule.category.name)

    def _checkValues(self):
        """
        Validates user data
        :return: True if all necessary data is set
        :rtype: bool
        """
        if not self.cmb_fields.GetValue():
            logging.warning("No field selected")
            return False

        if not self.txt_content.GetValue():
            logging.warning("No containing text")
            return False

        if not self.cmb_category.GetValue():
            logging.warning("No valid category selected")
            return False

        return True

    def _getUpdatedTransactionFromUI(self):
        """
        :return: transaction with updated user values
        :rtype: Transaction
        """
        if self.categoryRule is None:
            categoryRule = CategoryRule()
        else:
            categoryRule = self.categoryRule

        categoryRule.cond_field = self.cmb_fields.GetValue()
        categoryRule.contains = self.txt_content.GetValue()
        categoryRule.category = CategoryService.get_by_name(self.cmb_category.GetValue())

        return categoryRule

    def setConditionField(self, condField):
        """
        Sets the value of the condition field
        :param condField: the condition field (one of ruleColumns in db.DbModelClasses)
        :type condField: str
        """
        self.cmb_fields.SetValue(condField)

    def setCategory(self, category):
        """
        Sets the category
        :param category: the category
        :type category: str
        """
        self.cmb_category.SetValue(category)

    def setContains(self, contains):
        """
        Sets the value of the content
        :param contains: string the field should contain
        :type contains: str
        """
        self.txt_content.SetValue(contains)

    # ----- EVENTS -----

    # noinspection PyUnusedLocal
    def __onApply(self, evt):
        """
        Triggered when button apply is pressed
        :param evt:
        """
        if self._checkValues():
            categoryRule = self._getUpdatedTransactionFromUI()

            # INSERT
            if self.categoryRule is None:
                # check if rule with same values already exist
                if CategoryRuleService.hasRule(categoryRule):
                    logging.info("A category rule for field '{}' with value '{}' already exists. Alter values.".format(
                        categoryRule.cond_field,
                        categoryRule.contains))
                else:
                    DBM().insert(categoryRule)
                    self.categoryRule = categoryRule
                    self.EndModal(wx.ID_OK)
            # UPDATE
            else:
                DBM().update(categoryRule)
                self.EndModal(wx.ID_OK)

    # noinspection PyUnusedLocal
    def __onDiscard(self, evt):
        """
        Triggerd when button discard is pressed
        :param evt:
        """
        self.EndModal(wx.ID_CANCEL)


if __name__ == "__main__":
    app = wx.App()
    mainFrame = CategoryRuleEditDialog(None)
    mainFrame.Show()
    app.MainLoop()
