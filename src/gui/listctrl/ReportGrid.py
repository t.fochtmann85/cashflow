#  -*- coding: cp1252 german encoding -*-
__author__ = "Tobias Fochtmann"

import wx.grid

from gui.populator.ReportPopulator import ReportPopulator


class ReportGrid(wx.grid.Grid):
    """
    Tree control to show the sums of all categories
    """

    def __init__(self, parent, populator):
        """
        Constructor
        :param parent: parent window
        :param populator: the populator
        :type populator: ReportPopulator
        """
        wx.grid.Grid.__init__(self, parent)
        self.populator = populator

    def load(self):
        """
        Fills the tree control
        :return:
        """
        self.populator.populate(self)

    # ----- EVENTS ------------

    # ----- END OF EVENTS -----
