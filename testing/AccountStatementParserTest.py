#  -*- coding: cp1252 german encoding -*-
import os
import unittest

from db.DbModelClasses import Wallet
from filehandler import AccountStatementParser


class FormatInfoTest(unittest.TestCase):

    def test_read_Ok(self):
        formatinfo = {"row_skipFirst": 0,
                      "row_skipLast": 0,
                      "separator": ";",
                      "col_date": 1,
                      "col_expe": 2,
                      "col_earn": 2,
                      "col_srcDest": 3}
        self.assertIsInstance(AccountStatementParser.FormatInfo.read(formatinfo),
                              AccountStatementParser.FormatInfo)

    def test_read_WrongType(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo.read,
                          None)

    def test_read_InsufficientKeys(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo.read,
                          {})

    def test_Constructor_Ok(self):
        fi = AccountStatementParser.FormatInfo(0, 0, ";", 1, 2, 3, 4)
        self.assertIsInstance(fi, AccountStatementParser.FormatInfo)

    def test_Constructor_WrongType_skipFirst(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo,
                          None, 0, ";", 1, 2, 3, 4)

    def test_Constructor_WrongType_skipLast(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo,
                          0, None, ";", 1, 2, 3, 4)

    def test_Constructor_WrongType_separator(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo,
                          0, 0, None, 1, 2, 3, 4)

    def test_Constructor_WrongType_coldate(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo,
                          0, 0, ";", None, 2, 3, 4)

    def test_Constructor_WrongType_colexpe(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo,
                          0, 0, ";", 1, None, 3, 4)

    def test_Constructor_WrongType_colearn(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo,
                          0, 0, ";", 1, 2, None, 4)

    def test_Constructor_WrongType_colsrcdest(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo,
                          0, 0, ";", 1, 2, 3, None)

    def test_Constructor_NegNumbers(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo,
                          0, 0, ";", -1, 2, 3, 4)

    def test_Constructor_NegNumbers2(self):
        self.assertRaises(AssertionError,
                          AccountStatementParser.FormatInfo,
                          0, 0, ";", -1, 2, 3, 4, -1)


class AccountStatementParserTest(unittest.TestCase):

    def test_parseAccountStatement_Ok(self):
        filePath = os.path.join(os.path.dirname(__file__),
                                "res",
                                "example.csv")
        formatInfo = {"col_date": 1, "col_expe": 15, "separator": ";", "col_iban": 5,
                      "col_srcDest": 3, "row_skipFirst": 5, "row_skipLast": 1,
                      "col_earn": 16, "col_purpose": 4, "col_bic": 6}
        walletId = 0

        transactionList = AccountStatementParser.parseAccountStatement(filePath, formatInfo, walletId)

        self.assertIsInstance(transactionList, list)
        self.assertGreater(len(transactionList), 0)


