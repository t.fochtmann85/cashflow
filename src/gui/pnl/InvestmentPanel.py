#  -*- coding: cp1252 german encoding -*-

import wx
import wx.lib.newevent

import CommonFunctions
from db.DatabaseManager import DatabaseManager as DBM
from db.DatabaseService import InvestmentService
from db.DbModelClasses import Investment
from gui import GUIHelper
from gui.bmp import images
from gui.dlg.InvestmentEditDialogImpl import InvestmentEditDialogImpl
from gui.events import TransactionChangedEvent
from gui.listctrl.FilterListView import FilterListView
from resources import String

__author__ = "Tobias Fochtmann"


class InvestmentPanel(wx.Panel):
    """
    Panel for showing transactions
    """
    ID_POPUPMENU_DELETE_TA = wx.NewId()
    ID_POPUPMENU_COPY_INV = wx.NewId()

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window 
        """
        wx.Panel.__init__(self, parent)

        self._create_controls()
        self._set_tool_tips()
        self._set_layout()
        self._bind_events()

    def _create_controls(self):
        """
        Create controls and widgets
        """
        self._sizer = wx.GridBagSizer(hgap=5, vgap=5)

        self._list_investments = FilterListView(
            self,
            itemValueFunc=InvestmentPanelImpl.get_col_value,
            filterValueFunc=InvestmentPanelImpl.get_col_value,
            sortValueFunc=InvestmentPanelImpl.get_sort_value,
            listType=FilterListView.LIST_TYPE_LIST,
            columnList=[
                (String.LBL_COL_DATE, 70, FilterListView.INPUT_TYPE_TEXT,
                 wx.TE_PROCESS_ENTER),
                ("Type", 100, FilterListView.INPUT_TYPE_TEXT,
                 wx.TE_PROCESS_ENTER),
                ("Price", 60, FilterListView.INPUT_TYPE_TEXT,
                 wx.TE_PROCESS_ENTER),
                ("Fee", 60, FilterListView.INPUT_TYPE_TEXT,
                 wx.TE_PROCESS_ENTER),
                ("Netto", 60, FilterListView.INPUT_TYPE_TEXT,
                 wx.TE_PROCESS_ENTER),
                ("Bond", 200, FilterListView.INPUT_TYPE_COMB,
                 wx.TE_PROCESS_ENTER),
                ("Bond type", 100, FilterListView.INPUT_TYPE_COMB,
                 wx.TE_PROCESS_ENTER),
                ("Quantity", 100, FilterListView.INPUT_TYPE_TEXT,
                 wx.TE_PROCESS_ENTER),
                ("Platform", 100, FilterListView.INPUT_TYPE_TEXT,
                 wx.TE_PROCESS_ENTER),
                ("Stock exchange", 150, FilterListView.INPUT_TYPE_TEXT,
                 wx.TE_PROCESS_ENTER),
                ("", 100, FilterListView.INPUT_TYPE_TEXT,
                 wx.TE_PROCESS_ENTER),
            ],
            itemList=list(),
            listMinSize=(700, 200),
            listStyle=wx.LC_REPORT | wx.LC_VIRTUAL | wx.LC_HRULES
        )
        self._set_up_list()

        self._btn_add = wx.BitmapButton(self, bitmap=images.MONEY_ENVELOPE_ADD.GetBitmap())
        self._btn_edit = wx.BitmapButton(self, bitmap=images.MONEY_ENVELOPE_EDIT.GetBitmap())
        self._btn_rem = wx.BitmapButton(self, bitmap=images.MONEY_ENVELOPE_DELETE.GetBitmap())

    def _set_up_list(self):
        list_item = wx.ListItem()
        list_item.SetAlign(wx.LIST_FORMAT_RIGHT)
        self._list_investments.lst.SetColumn(2, list_item)
        self._list_investments.lst.SetColumn(3, list_item)
        self._list_investments.lst.SetColumn(4, list_item)
        self._list_investments.lst.SetColumn(7, list_item)
        GUIHelper.Fontfactory.changeFontOfCtrl(self._list_investments.lst)

    def _set_tool_tips(self):
        """
        Set tool tips
        """
        self._btn_add.SetToolTip("Add investment")
        self._btn_edit.SetToolTip("Edit investment")
        self._btn_rem.SetToolTip("Remove investment")

    def _set_layout(self):
        """
        Positioning all controls of the panel
        """
        padding = 0

        line = 0
        self._sizer.Add(self._btn_add, (line, 0), (1, 1))
        self._sizer.Add(self._list_investments, (line, 1), (3, 1), wx.EXPAND)
        line += 1
        self._sizer.Add(self._btn_edit, (line, 0), (1, 1))
        line += 1
        self._sizer.Add(self._btn_rem, (line, 0), (1, 1))
        self._sizer.AddGrowableRow(line)
        self._sizer.AddGrowableCol(1)

        self.SetSizerAndFit(self._sizer)

    def _bind_events(self):
        """
        Associate events with actions
        """
        self._btn_add.Bind(wx.EVT_BUTTON, self._on_add)
        self._btn_edit.Bind(wx.EVT_BUTTON, self._on_edit)
        self._btn_rem.Bind(wx.EVT_BUTTON, self._on_remove)

        self._list_investments.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self._on_edit)
        self._list_investments.Bind(wx.EVT_LIST_KEY_DOWN, self._on_key_down_on_item)
        self._list_investments.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self._on_item_right_click)
        self._list_investments.Bind(FilterListView.EVT_ITEM_DATA_CHANGED, self._on_content_changed)
        self._list_investments.lst.Bind(FilterListView.EVT_FLV_ITEM_SELECTED, self._on_selection_changed)
        self._list_investments.lst.Bind(FilterListView.EVT_FLV_ITEM_DESELECTED, self._on_selection_changed)

        self._list_investments.Bind(wx.EVT_MENU, self._on_create_investment_copy, id=self.ID_POPUPMENU_COPY_INV)

    def _fire_event_investment_changed(self):
        """
        Triggers an event for updating the wallet balance (in other view)
        """
        wx.PostEvent(self.GetEventHandler(), TransactionChangedEvent(self.GetId()))

    # ----- EVENTS ------------
    def _on_item_right_click(self, evt):
        raise NotImplementedError("Implement in sub class")

    def _on_date_changed(self, evt):
        raise NotImplementedError("Implement in sub class")

    def _on_content_changed(self, evt):
        raise NotImplementedError("Implement in sub class")

    def _on_key_down_on_item(self, evt):
        raise NotImplementedError("Implement in sub class")

    def _on_add(self, evt):
        raise NotImplementedError("Implement in sub class")

    def _on_edit(self, evt):
        raise NotImplementedError("Implement in sub class")

    def _on_remove(self, evt):
        raise NotImplementedError("Implement in sub class")

    def _on_selection_changed(self, evt):
        raise NotImplementedError("Implement in sub class")

    def _on_create_investment_copy(self, evt):
        raise NotImplementedError("Implement in sub class")

    # ----- END OF EVENTS -----


class InvestmentPanelImpl(InvestmentPanel):
    """
    Panel for showing transactions
    """

    def __init__(self, *args, **kwargs):
        super(InvestmentPanelImpl, self).__init__(*args, **kwargs)

        self.startDate = None
        self.endDate = None
        self._init_load()

    def _init_load(self):
        """
        Load initial values, set up initial states
        """
        self._btn_edit.Enable(False)
        self._btn_rem.Enable(False)

    def load(self):
        """
        Loads the data of the current database
        """
        investments = InvestmentService.get_investments(
            CommonFunctions.wxDateTimeToDatetime(self.startDate),
            CommonFunctions.wxDateTimeToDatetime(self.endDate)
        )
        self._list_investments.setListItems(investments)

    # ----- EVENTS ------------

    def _on_item_right_click(self, evt):
        menu = wx.Menu()
        menu.Append(self.ID_POPUPMENU_COPY_INV, String.LBL_MENU_TA_INV)

        self._list_investments.PopupMenu(menu)

    def _on_content_changed(self, evt):
        """
        Triggered when the date changes
        :param evt:
        :return:
        """
        self.load()

    def _on_key_down_on_item(self, evt):
        """
        Triggered when a key is pressed with focus on transaction list
        :param evt:
        :return:
        """
        if evt.GetKeyCode() == wx.WXK_DELETE:
            self._on_remove(evt)

    def _on_add(self, evt):
        """
        Triggered when tool 'add transaction' is clicked
        :param evt:
        """
        dlg = InvestmentEditDialogImpl(self)
        if dlg.ShowModal() == wx.ID_OK:
            self._fire_event_investment_changed()
            self.load()
        dlg.Destroy()

    def _on_edit(self, evt):
        """
        Triggered when a transaction is double clicked
        :param evt:
        """
        if self._list_investments.lst.isItemSelected():
            dlg = InvestmentEditDialogImpl(self, self._list_investments.getSelectedListItem())
            if dlg.ShowModal() == wx.ID_OK:
                self._fire_event_investment_changed()
                self.load()
            dlg.Destroy()

    def _on_remove(self, evt):
        """
        Triggered when button for removing transaction or key [Del] is pressed
        :param evt:
        :return:
        """
        dlg = wx.MessageDialog(self,
                               message=String.DLG_MSG_INV_DEL,
                               caption=String.DLG_TITLE_INV_DEL,
                               style=wx.YES_NO)
        if dlg.ShowModal() == wx.ID_YES:
            for investment in self._list_investments.getSelectedListItems():
                DBM().delete(investment)
            self._fire_event_investment_changed()
            self.load()

    def _on_create_investment_copy(self, evt):
        investment = self._list_investments.getSelectedListItem()  # type: Investment

        dlg = InvestmentEditDialogImpl(self)
        dlg.set_date(investment.date)
        dlg.set_cost(investment.cost)
        dlg.set_type(investment.type)
        dlg.set_fee(investment.fee)
        dlg.set_bond(investment.bond.name)
        dlg.set_quantity(investment.quantity)
        dlg.set_platform(investment.platform)
        dlg.set_exchange(investment.stock_exchange)
        if dlg.ShowModal() == wx.ID_OK:
            self.load()
        dlg.Destroy()

    def _on_selection_changed(self, evt):
        """
        Triggered when item selection in transaction list changes
        :param evt:
        """
        self._btn_edit.Enable(self._list_investments.lst.isItemSelected())
        self._btn_rem.Enable(self._list_investments.lst.isItemSelected())

    # ----- END OF EVENTS -----

    @staticmethod
    def get_col_value(investment: Investment, col: int) -> str:
        """
        :param investment: the chosen investment
        :param col: column number
        :return: Returns the value of the transaction data for a given column
        """
        data = [
            investment.date.strftime(String.FMT_DATE),
            investment.type,
            "{:.2f}".format(investment.cost),
            "{:.2f}".format(investment.fee),
            "{:.2f}".format(investment.cost - investment.fee),
            investment.bond.name,
            investment.bond.type,
            "{:.6f}".format(investment.quantity),
            investment.platform,
            investment.stock_exchange,
        ]

        if col < len(data):
            return data[col]
        else:
            return ""

    @staticmethod
    def get_sort_value(investment: Investment, col: int) -> str:
        """
        :param investment: the chosen investment
        :param col: column number
        :return: Returns the value to sort for the transaction data for a given column
        """

        data = [
            investment.date.strftime(String.FMT_DATE),
            investment.type,
            investment.cost,
            investment.fee,
            investment.cost - investment.fee,
            investment.bond.name,
            investment.bond.type,
            investment.quantity,
            investment.platform,
            investment.stock_exchange,
        ]

        if col < len(data):
            return data[col]
        else:
            return ""
