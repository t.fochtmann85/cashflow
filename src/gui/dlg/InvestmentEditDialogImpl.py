#  -*- coding: cp1252 german encoding -*-

import logging

import wx
import wx.adv
from wx.lib.masked import NumCtrl

import CommonFunctions
from db.DatabaseManager import DatabaseManager as DBM
from db.DatabaseService import BondService, InvestmentService
from db.DbModelClasses import Transaction, Investment, INVESTMENT_TYPE
from gui.bmp import images
from resources import String

__author__ = "Tobias Fochtmann"


class InvestmentEditDialog(wx.Dialog):
    """
    Dialog for editing transactions
    """

    def __init__(self, parent: wx.Window):
        """
        Constructor
        :param parent: parent window
        :param investment: transaction data
        """
        wx.Dialog.__init__(self, parent, title="Edit investment")
        self.SetIcon(images.TOOL_TRANSACTION.GetIcon())

        self._create_controls()
        self._setToolTips()
        self._setLayout()
        self._bind_events()

        self.CenterOnParent()

    def _create_controls(self):
        """
        Creates all used widgets
        """
        # sizers
        self._sizer = wx.GridBagSizer(hgap=10, vgap=5)

        # labels
        self._lbl_date = wx.StaticText(self, label=String.LBL_COL_DATE)
        self._lbl_type = wx.StaticText(self, label="Type")
        self._lbl_cost = wx.StaticText(self, label="Price")
        self._lbl_fee = wx.StaticText(self, label="Fee")
        self._lbl_netto = wx.StaticText(self, label="Netto")
        self._lbl_quantity = wx.StaticText(self, label="Quantity")
        self._lbl_bond = wx.StaticText(self, label="Bond")
        self._lbl_platform = wx.StaticText(self, label="Platform")
        self._lbl_stock_exchange = wx.StaticText(self, label="Stock exchange")

        # text inputs
        self._txt_date = wx.adv.DatePickerCtrl(self, style=wx.adv.DP_DROPDOWN)
        self._choice_type = wx.Choice(self, choices=sorted(INVESTMENT_TYPE.enums))
        self._txt_cost = NumCtrl(self, integerWidth=6, fractionWidth=2, autoSize=False, signedForegroundColour=wx.RED)
        self._txt_cost.SetMinSize(self._txt_date.GetSize())
        self._txt_fee = NumCtrl(self, integerWidth=6, fractionWidth=2, autoSize=False, signedForegroundColour=wx.RED)
        self._txt_fee.SetMinSize(self._txt_date.GetSize())
        self._txt_netto = NumCtrl(self, integerWidth=6, fractionWidth=2, autoSize=False, signedForegroundColour=wx.RED)
        self._txt_netto.SetMinSize(self._txt_date.GetSize())
        self._txt_netto.Enable(False)
        self._txt_quantity = NumCtrl(self, integerWidth=6, fractionWidth=6, autoSize=False, min=0)
        self._txt_quantity.SetMinSize(self._txt_date.GetSize())
        self._choice_bond = wx.Choice(self, choices=BondService.get_names())
        self._choice_platform = wx.ComboBox(self, choices=InvestmentService.get_platforms())
        self._choice_platform.SetMinSize(self._txt_date.GetSize())
        self._choice_stock_exchange = wx.ComboBox(self, choices=InvestmentService.get_stock_exchanges())
        self._choice_stock_exchange.SetMinSize(self._txt_date.GetSize())

        # buttons
        self._btn_apply = wx.BitmapButton(self, bitmap=images.APPLY.GetBitmap())

    def _setToolTips(self):
        """
        Set tool tips
        """
        self._btn_apply.SetToolTip("Write changes to database and close the dialog")

    def _setLayout(self):
        """
        Sets the layout of the widgets
        """
        border = 10
        line = 0
        self._sizer.Add(self._lbl_date, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.TOP, border)
        self._sizer.Add(self._txt_date, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.TOP | wx.RIGHT, border)
        line += 1
        self._sizer.Add(self._lbl_bond, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self._sizer.Add(self._choice_bond, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, border)
        line += 1
        self._sizer.Add(self._lbl_type, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self._sizer.Add(self._choice_type, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.RIGHT, border)
        line += 1
        self._sizer.Add(self._lbl_cost, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self._sizer.Add(self._txt_cost, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.RIGHT, border)
        line += 1
        self._sizer.Add(self._lbl_fee, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self._sizer.Add(self._txt_fee, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, border)
        line += 1
        self._sizer.Add(self._lbl_netto, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self._sizer.Add(self._txt_netto, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, border)
        line += 1
        self._sizer.Add(self._lbl_quantity, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self._sizer.Add(self._txt_quantity, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, border)
        line += 1
        self._sizer.Add(self._lbl_platform, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self._sizer.Add(self._choice_platform, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, border)
        line += 1
        self._sizer.Add(self._lbl_stock_exchange, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, border)
        self._sizer.Add(self._choice_stock_exchange, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, border)
        line += 1
        self._sizer.Add(self._btn_apply, (line, 0), (1, 2), wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL,
                        border)

        self.SetSizerAndFit(self._sizer)

    def _bind_events(self):
        """
        Binds events to actions
        """
        self._choice_bond.Bind(wx.EVT_CHOICE, self._on_bond_selected)
        self._txt_fee.Bind(wx.EVT_KILL_FOCUS, self._on_leave_fee)
        self._btn_apply.Bind(wx.EVT_BUTTON, self._on_apply)

    # ----- EVENTS -----

    def _on_bond_selected(self, evt):
        raise NotImplementedError("Implement in sub class")

    def _on_leave_fee(self, evt):
        raise NotImplementedError("Implement in sub class")

    def _on_apply(self, evt):
        raise NotImplementedError("Implement in sub class")


class InvestmentEditDialogImpl(InvestmentEditDialog):
    """
    Dialog for editing transactions
    """

    def __init__(self, parent, investment: Investment = None):
        """
        Constructor
        :param parent: parent window
        :param investment: transaction data
        :type investment: Transaction
        """
        super(InvestmentEditDialogImpl, self).__init__(parent)
        self._investment = investment
        self._init_load()

    def _init_load(self):
        """
        Load initial values, set up initial states
        """
        # new transaction
        if self._investment is not None:
            self._txt_date.SetValue(CommonFunctions.datetimeToWxDateTime(self._investment.date))
            self._choice_type.SetStringSelection(self._investment.type)
            self._txt_cost.SetValue(self._investment.cost)
            self._txt_fee.SetValue(self._investment.fee)
            self._txt_netto.SetValue(self._investment.cost - self._investment.fee)
            self._choice_bond.SetStringSelection(self._investment.bond.name)
            self._txt_quantity.SetValue(self._investment.quantity)
            self._choice_platform.SetValue(self._investment.platform)
            self._choice_stock_exchange.SetValue(self._investment.stock_exchange)

    def _check_values(self):
        """
        Validates user data
        :return: True if all necessary data is set
        :rtype: bool
        """
        if not self._choice_bond.GetStringSelection():
            logging.warning("No bond selected")
            return False
        if not self._choice_type.GetStringSelection():
            logging.warning("No type selected")
            return False

        return True

    def _get_updated_investment_from_ui(self):
        """
        :return: investment with updated user values
        :rtype: Transaction
        """
        quantity = self._txt_quantity.GetValue()
        bond = BondService.get_by_name(self._choice_bond.GetStringSelection())
        type = self._choice_type.GetStringSelection()

        if self._investment is None:
            investment = Investment()

            # bond is of type 'Fund' we are working with differences
            if bond.type == "Fund" and type == "Buy":
                quantity = quantity - BondService.get_quantity(bond)

        else:
            investment = self._investment

        investment.date = CommonFunctions.wxDateTimeToDatetime(self._txt_date.GetValue())
        investment.type = type
        investment.cost = self._txt_cost.GetValue()
        investment.fee = self._txt_fee.GetValue()
        investment.bond = bond
        investment.quantity = quantity
        investment.platform = self._choice_platform.GetValue()
        investment.stock_exchange = self._choice_stock_exchange.GetValue()

        return investment

    def set_cost(self, value: float) -> None:
        self._txt_cost.SetValue(value)

    def set_date(self, dt: wx.DateTime) -> None:
        self._txt_date.SetValue(dt)

    def set_type(self, type_: str) -> None:
        self._choice_type.SetStringSelection(type_)

    def set_fee(self, fee: float) -> None:
        self._txt_fee.SetValue(fee)

    def set_bond(self, bond: str) -> None:
        self._choice_bond.SetStringSelection(bond)

    def set_quantity(self, quantity: float) -> None:
        self._txt_quantity.SetValue(quantity)

    def set_platform(self, platform: str) -> None:
        self._choice_platform.SetStringSelection(platform)

    def set_exchange(self, exchange: str) -> None:
        self._choice_stock_exchange.SetStringSelection(exchange)

    # ----- EVENTS -----

    def _on_bond_selected(self, evt):
        """
        Triggered when bond was selected
        :param evt:
        """
        evt.Skip()
        bond = BondService.get_by_name(self._choice_bond.GetStringSelection())

        if bond.type == "Fund":
            self._lbl_quantity.SetLabel("New quantity")
        else:
            self._lbl_quantity.SetLabel("Quantity")

    def _on_leave_fee(self, evt):
        """
        Triggered when control for fee was left
        :param evt:
        """
        evt.Skip()
        self._txt_netto.SetValue(self._txt_cost.GetValue() - self._txt_fee.GetValue())

    def _on_apply(self, evt):
        """
        Triggered when button apply is pressed
        :param evt:
        """
        if self._check_values():
            investment = self._get_updated_investment_from_ui()
            DBM().update(investment)
            self.EndModal(wx.ID_OK)
