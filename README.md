# Cashflow

Small application for handling expenses and earnings (transactions) based on different wallets (cash, bank account, credit card, ...) and categories, import account statements and see your monthly (yearly, all time) report.