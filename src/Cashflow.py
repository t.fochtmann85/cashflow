#  -*- coding: cp1252 german encoding -*-
__author__ = "TobiasFochtmann"

import logging
from asyncio.events import get_event_loop

from wxasync import WxAsyncApp

from gui.CashflowMainFrame import CashflowMainFrameImpl

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    app = WxAsyncApp()
    loop = get_event_loop()
    mainFrame = CashflowMainFrameImpl()
    mainFrame.Show()

    mainFrame.load()
    loop.run_until_complete(app.MainLoop())
    app.MainLoop()
