#  -*- coding: cp1252 german encoding -*-
import wx


class Fontfactory:
    @staticmethod
    def changeFontSize(font, offset):
        font.SetPointSize(font.GetPointSize() + offset)

    @staticmethod
    def changeFontOfCtrl(ctrl, offset=None, face=None, pointSize=None, weight=None, style=None):
        font = ctrl.GetFont()

        Fontfactory.changeFont(font, offset, face, pointSize, weight, style)

        ctrl.SetFont(font)

    @staticmethod
    def changeFont(font, offset=None, face=None, pointSize=None, weight=None, style=None):
        if offset is not None:
            Fontfactory.changeFontSize(font, offset)

        if face is not None:
            font.SetFaceName(face)

        if pointSize is not None:
            font.SetPointSize(pointSize)

        if weight is not None:
            font.SetWeight(weight)

        if style is not None:
            font.SetStyle(style)

        return font


class DialogFactory:
    @staticmethod
    def createYesNoDialog(parent, caption, message):
        return wx.MessageDialog(parent, message, caption, style=wx.STAY_ON_TOP | wx.YES_NO)
