#  -*- coding: cp1252 german encoding -*-
import logging
from datetime import timedelta, datetime
from typing import List, Optional

from sqlalchemy import func, or_

from db.DatabaseManager import DatabaseManager
from db.DbModelClasses import Transaction, Category, CategoryRule, SplitRule, Wallet, Bond, Investment
from resources import String


class BondService:
    @staticmethod
    def get_items_of_type(bond_type: str) -> List[Bond]:
        return DatabaseManager().session. \
            query(Bond). \
            filter(Bond.type == bond_type). \
            order_by(Bond.name). \
            all()

    @staticmethod
    def get_quantity(bond: Bond) -> float:
        return round(DatabaseManager().session.
                     query(func.sum(Investment.quantity)).
                     filter(Investment.bond == bond).
                     one()[0] or 0.0, 6)

    @staticmethod
    def get_cost(bond: Bond) -> float:
        filters = [Investment.bond == bond]
        if bond.type != "Fund":
            filters.append(Investment.type.in_(("Buy", "Sell",)))

        return round(DatabaseManager().session.
                     query(func.sum(Investment.cost)).
                     filter(*filters).
                     one()[0] or 0.0, 2)

    @staticmethod
    def get_fee(bond: Bond) -> float:
        return round(DatabaseManager().session.
                     query(func.sum(Investment.fee)).
                     filter(Investment.bond == bond).
                     one()[0] or 0.0, 2)

    @staticmethod
    def get_last_transaction_date(bond: Bond) -> str:
        return "Implement!"

    @staticmethod
    def get_names() -> List[str]:
        return untuple(DatabaseManager().session.
                       query(Bond.name).
                       order_by(Bond.name).
                       all())

    @staticmethod
    def get_by_name(bond_name: str) -> Optional[Bond]:
        return DatabaseManager().session. \
            query(Bond). \
            filter(Bond.name == bond_name). \
            one_or_none()


class WalletService:
    """
    Provides methods regarding wallets
    """

    @staticmethod
    def get_last_transaction_date(wallet: Wallet) -> str:
        last_transaction_date = TransactionService.getLastOfWallet(wallet)
        if last_transaction_date is None:
            return "-"
        else:
            return last_transaction_date.date.strftime(String.FMT_DATE)

    @staticmethod
    def get_items_of_type(wallet_type: str) -> List[Wallet]:
        return DatabaseManager().session. \
            query(Wallet). \
            filter(Wallet.type == wallet_type). \
            order_by(Wallet.name). \
            all()

    @staticmethod
    def get_balance(wallet: Wallet) -> float:
        """
        :param wallet: wallet the sum should be calculated
        :type wallet: Wallet
        :return: Sum of all transactions for this wallet
        :rtype: float
        """
        return DatabaseManager().session. \
                   query(func.sum(Transaction.amount)). \
                   filter(Transaction.wallet == wallet). \
                   one()[0] or 0.0

    @staticmethod
    def get(considered):
        """
        :param considered: compare value for column 'consider'
        :type considered: bool
        :return: all wallets matching considered column
        :rtype: list[Wallet]
        """
        return DatabaseManager().session. \
            query(Wallet). \
            filter(Wallet.fast_accessible == considered). \
            order_by(Wallet.name). \
            all()

    @staticmethod
    def getNames():
        """
        :return: list of all wallet names
        :rtype: list[tuple(str,)]
        """
        return untuple(DatabaseManager().session.
                       query(Wallet.name).
                       order_by(Wallet.name).
                       all())

    @staticmethod
    def getByName(walletName):
        """
        :param walletName: name of the wallet
        :type walletName: str
        :return: the wallet
        :rtype: Wallet
        """
        return DatabaseManager().session. \
            query(Wallet). \
            filter(Wallet.name == walletName). \
            one()

    @staticmethod
    def getForParsing():
        """
        :return: list of wallets having a valid value for parsing account statements
        :rtype: list[Wallet]
        """
        return DatabaseManager().session. \
            query(Wallet). \
            filter(Wallet.as_format_info != None,
                   Wallet.as_format_info != ""). \
            order_by(Wallet.name.asc()). \
            all()


class InvestmentService:
    @staticmethod
    def get_first_transaction_date() -> datetime:
        item = DatabaseManager().session.query(Investment).order_by(Investment.date.asc()).first()
        if item is not None:
            return item.date

    @staticmethod
    def get_investments(start_date: datetime, end_date: datetime) -> List[Investment]:
        return DatabaseManager().session. \
            query(Investment). \
            filter(Investment.date.between(start_date - timedelta(minutes=1), end_date)). \
            order_by(Investment.date.asc()). \
            all()

    @staticmethod
    def get_platforms() -> List[str]:
        return sorted({
            item.platform for item in DatabaseManager().session. \
                query(Investment). \
                group_by(Investment.platform). \
                all()
        })

    @staticmethod
    def get_stock_exchanges() -> List[str]:
        return sorted({
            item.stock_exchange for item in DatabaseManager().session. \
                query(Investment). \
                group_by(Investment.stock_exchange). \
                all()
        })


class TransactionService:
    """
    Provides methods regarding transactions
    """

    @staticmethod
    def existsWithHash(transaction):
        """
        :param transaction: transaction that should be inserted
        :type transaction: Transaction
        :return: True if a transaction with same hash already exists
        :rtype: bool
        """
        return DatabaseManager().session. \
                   query(Transaction). \
                   filter(Transaction.uniqueid == transaction.uniqueid). \
                   count() > 0

    @staticmethod
    def getLastOfWallet(wallet):
        """
        :param wallet: the wallet to get the last transaction of
        :type wallet: Wallet
        :return: the last transaction of this wallet
        :rtype: Transaction
        """
        return DatabaseManager().session. \
            query(Transaction). \
            filter(Transaction.wallet == wallet). \
            order_by(Transaction.date.desc()). \
            first()

    @staticmethod
    def hasWithCategory(category):
        """
        Checks for transactions of this category
        :param category: the category to check
        :type category: Category
        :return: True if there are transactions
        :rtype: bool
        """
        return DatabaseManager().session. \
                   query(Transaction). \
                   filter(Transaction.category == category). \
                   count() > 0

    @staticmethod
    def getSumForCategoriesBetween(startDate, endDate):
        """
        :param startDate: start date
        :type startDate: datetime
        :param endDate: end date
        :type endDate: datetime
        :return: list of sums for the given period of time
        :rtype: list[(str, float,)]
        """
        categorySums = list()

        # iterate through category leaves
        for category in CategoryService.getLeaves():
            # get the sum for this category
            categorySum = TransactionService.getSumForCategoryBetween(category,
                                                                      startDate - timedelta(minutes=1),
                                                                      endDate)
            categorySums.append((category.name, categorySum))

        return categorySums

    @staticmethod
    def getSumForCategoryBetween(category, startDate=datetime(1970, 1, 1), endDate=datetime.now()):
        """
        :param category:
        :param startDate: start date
        :type startDate: datetime
        :param endDate: end date
        :type endDate: datetime
        :return: sum of all transactions with the linked category between start and end date
        :rtype: float
        """
        return round(DatabaseManager().session.
                     query(func.sum(Transaction.amount * Transaction.factor)).
                     filter(Transaction.date.between(startDate, endDate),
                            or_(Transaction.comment == None, Transaction.comment != "Initialisierung"),
                            Transaction.category == category).
                     one()[0] or 0.0, 2)

    @staticmethod
    def getBetween(startDate=datetime(1970, 1, 1), endDate=datetime.now()):
        """
        :param startDate: start date
        :type startDate: datetime
        :param endDate: end date
        :type endDate: datetime
        :return: All transaction between the given time stamps
        :rtype: list[Transaction]
        """
        return DatabaseManager().session. \
            query(Transaction). \
            filter(Transaction.date.between(startDate - timedelta(minutes=1), endDate),
                   or_(Transaction.comment == None, Transaction.comment != "Initialisierung")). \
            order_by(Transaction.date.asc()). \
            all()

    @staticmethod
    def getFirstDate():
        """
        :return: date of the first transaction, that was not 'Initialisierung'
        :rtype: datetime
        """
        taList = DatabaseManager().session. \
            query(Transaction.date). \
            filter(or_(Transaction.comment == None, Transaction.comment != "Initialisierung")). \
            order_by(Transaction.date.asc()). \
            limit(1). \
            all()

        if taList:
            return taList[0][0]
        else:
            return datetime.now()

    @staticmethod
    def updateCategoryOfId(transactionId, categoryId):
        """
        Updates the category of the given transaction
        :param transactionId: id of the transaction
        :type transactionId: long
        :param categoryId: id of the category
        :type categoryId: long
        """
        transaction = DatabaseManager().session. \
            query(Transaction). \
            get(transactionId)  # type: Transaction
        transaction.category_id = categoryId
        DatabaseManager().update(transaction)

    @staticmethod
    def replaceCategory(oldCategory, newCategory):
        """
        Updates the linked category in all transactions with category categoryToRemove
        :param oldCategory: the category that should be removed
        :type oldCategory: Category
        :param newCategory: the substituting category
        :type newCategory: Category
        """
        itemsUpdated = DatabaseManager().session. \
            query(Transaction). \
            filter(Transaction.category == oldCategory) \
            .update({"category_id": newCategory.id}, synchronize_session='fetch')

        DatabaseManager().commit()
        logging.info("Updated {} transactions".format(itemsUpdated))

    @staticmethod
    def getSumOfPeriod(startDate, endDate):
        return untuple(DatabaseManager().session. \
                       query(func.sum(Transaction.amount * Transaction.factor)). \
                       filter(Transaction.date.between(startDate - timedelta(minutes=1), endDate),
                              or_(Transaction.comment == None, Transaction.comment != "Initialisierung"),
                              Transaction.category_id != 77). \
                       order_by(Transaction.date.asc()). \
                       all())[0]


class CategoryService:
    """
    Provides methods regarding categories
    """

    @staticmethod
    def get_by_name(categoryName):
        """
        :param categoryName: name of the category
        :type categoryName: str
        :return: the Category
        :rtype: Category
        """
        return DatabaseManager().session. \
            query(Category). \
            filter(Category.name == categoryName). \
            one()

    @staticmethod
    def getOrphans():
        """
        :return: Get all categories that do not have a parent (id)
        :rtype: list[Category]
        """
        return DatabaseManager().session. \
            query(Category). \
            filter(Category.parent == None). \
            all()

    @staticmethod
    def getLeaves(ignoreCategory=None):
        """
        :param ignoreCategory: the category that should be ignored --> remove this category from result
        :type ignoreCategory: Category
        :return: List of categories that don't have children
        :rtype: list[Category]
        """
        parentIdList = DatabaseManager().session. \
            query(Category.parent_id). \
            filter(Category.parent_id != None). \
            distinct(). \
            all()

        args = [Category.id.notin_(untuple(parentIdList))]
        if ignoreCategory is not None:
            args.append(Category.id != ignoreCategory.id)

        return DatabaseManager().session. \
            query(Category). \
            filter(*args). \
            order_by(Category.name). \
            all()

    @staticmethod
    def getLeafNames():
        """
        :return: list of all category names that have no children
        """
        return [category.name for category in CategoryService.getLeaves()]

    @staticmethod
    def replaceParentId(catId, parentCatId):
        """
        Updates the parent id for the given category item
        :param catId: id of the category
        :type catId: long
        :param parentCatId: id of the parent category
        :type parentCatId: long
        """
        category = DatabaseManager().session. \
            query(Category). \
            get(catId)
        category.parent_id = parentCatId
        DatabaseManager().update(category)

    @staticmethod
    def hasName(categoryName):
        """
        :param categoryName: name of the category
        :type categoryName: str
        :return: True if there is a category with same name already present
        :rtype: bool
        """
        return DatabaseManager().session. \
                   query(Category). \
                   filter(Category.name == categoryName). \
                   count() > 0

    @staticmethod
    def count():
        """
        :return: number of categories
        :rtype: int
        """
        return DatabaseManager().session. \
            query(Category). \
            count()


class CategoryRuleService:
    """
    Provides methods regarding category rules
    """

    @staticmethod
    def get():
        """
        :return: list of all category rules
        :rtype: list[CategoryRule]
        """
        return DatabaseManager().session. \
            query(CategoryRule). \
            all()

    @staticmethod
    def hasRule(categoryRule):
        """
        :param categoryRule: the category rule to check
        :type categoryRule: CategoryRule
        :return: True if a category rule with this condition field and content already exist
        :rtype: bool
        """
        return DatabaseManager().session. \
                   query(CategoryRule). \
                   filter(CategoryRule.cond_field == categoryRule.cond_field,
                          CategoryRule.contains == categoryRule.contains). \
                   count() > 0


class SplitRuleService:
    """
    Provides methods regarding split rules
    """

    @staticmethod
    def get():
        """
        :return: list of all split rules
        :rtype: list[SplitRule]
        """
        return DatabaseManager().session. \
            query(SplitRule). \
            all()

    @staticmethod
    def hasRule(splitRule):
        """
        :param splitRule: the split rule to check
        :type splitRule: SplitRule
        :return: True if a split rule with this condition field and content already exist
        :rtype: bool
        """
        return DatabaseManager().session. \
                   query(SplitRule). \
                   filter(SplitRule.cond_field == splitRule.cond_field,
                          SplitRule.contains == splitRule.contains). \
                   count() > 0


def untuple(listOf1Tuples):
    """
    Converting list of 1-tuples [(something,), (something2,), ... ]
    into a list of objects [something, something2, ... ]
    :param listOf1Tuples:
    :return:
    """
    return [item for (item,) in listOf1Tuples]
