import wx
from wx.adv import DatePickerCtrl

from gui import GUIHelper
from gui.pnl.InvestmentPanel import InvestmentPanelImpl
from gui.pnl.TransferPanel import TransferPanel
from resources import String


class TransactionPanel(wx.Panel):

    def __init__(self, parent):
        """
        Constructor
        :param parent: parent window
        """
        wx.Panel.__init__(self, parent)

        self._createControls()
        self._setLayout()
        self._bindEvents()
        self._initLoad()

    def _createControls(self):
        """
        Create controls and widgets
        """
        self.sizer = wx.GridBagSizer(hgap=5, vgap=5)

        self.txt_date_begin = DatePickerCtrl(self, style=wx.adv.DP_DROPDOWN)
        GUIHelper.Fontfactory.changeFontOfCtrl(self.txt_date_begin, offset=5, face="Adobe Devanagari")
        self.txt_date_end = DatePickerCtrl(self, style=wx.adv.DP_DROPDOWN)
        GUIHelper.Fontfactory.changeFontOfCtrl(self.txt_date_end, offset=5, face="Adobe Devanagari")

        self._splitter = wx.SplitterWindow(self)
        self.pnl_transfer = TransferPanel(self._splitter)
        self.pnl_investment = InvestmentPanelImpl(self._splitter)

    def _setLayout(self):
        """
        Positioning all controls of the panel
        """
        padding = 0
        self._splitter.SplitVertically(self.pnl_transfer, self.pnl_investment)

        line = 0
        self.sizer.Add(self.txt_date_begin, (line, 0), (1, 1), wx.LEFT | wx.TOP | wx.EXPAND, padding)
        self.sizer.Add(self.txt_date_end, (line, 1), (1, 1), wx.TOP, padding)
        line += 1
        self.sizer.Add(self._splitter, (line, 0), (1, 2), wx.EXPAND, padding)
        self.sizer.AddGrowableRow(line)
        self.sizer.AddGrowableCol(1)

        self.SetSizerAndFit(self.sizer)

    def _bindEvents(self):
        """
        Associate events with actions
        """
        self.txt_date_begin.Bind(wx.adv.EVT_DATE_CHANGED, self.__onDateChanged)
        self.txt_date_end.Bind(wx.adv.EVT_DATE_CHANGED, self.__onDateChanged)

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        dateBegin = wx.DateTime.Today()
        dateEnd = wx.DateTime.Today()
        dateBegin.SetDay(1)
        self.txt_date_begin.SetValue(dateBegin)
        self.txt_date_end.SetValue(dateEnd)
        self.load()

    def load(self):
        self.pnl_transfer.startDate = self._getDateBegin()
        self.pnl_transfer.endDate = self._getDateEnd()
        self.pnl_investment.startDate = self._getDateBegin()
        self.pnl_investment.endDate = self._getDateEnd()
        self.pnl_transfer.load()
        self.pnl_investment.load()

    def _getDateBegin(self):
        date_start = self.txt_date_begin.GetValue()
        date_start.SetHour(0)
        date_start.SetMinute(0)
        date_start.SetSecond(0)
        date_start.SetMillisecond(0)
        return date_start

    def _getDateEnd(self):
        dateEnd = self.txt_date_end.GetValue()
        dateEnd.SetHour(23)
        dateEnd.SetMinute(59)
        dateEnd.SetSecond(59)
        dateEnd.SetMillisecond(999)
        return dateEnd

    def __onDateChanged(self, evt):
        self.load()
