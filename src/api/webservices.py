import copy
import datetime
import json
import logging
from collections import namedtuple
from functools import cache
from pathlib import Path
from pprint import pprint
from typing import Optional, Dict, Callable, Any

import requests
import yfinance

RequestConfig = namedtuple("RequestConfig", "url headers")

RAPID_API_KEY = "aa37236eb8msh86aa240f30accfep1dc05cjsn89536eb0b95b"

ALPHA_VANTAGE = RequestConfig(
    "https://alpha-vantage.p.rapidapi.com/query",
    {
        'x-rapidapi-key': RAPID_API_KEY,
        'x-rapidapi-host': "alpha-vantage.p.rapidapi.com"
    },
)

FUNDS = RequestConfig(
    "https://funds.p.rapidapi.com/v1/fund/{isin}",
    {
        'x-rapidapi-key': RAPID_API_KEY,
        'x-rapidapi-host': "funds.p.rapidapi.com"
    },
)

CURRENCY = RequestConfig(
    "https://currencyapi-net.p.rapidapi.com/rates",
    {
        'x-rapidapi-key': RAPID_API_KEY,
    'x-rapidapi-host': "currencyapi-net.p.rapidapi.com"
    }
)

PATH_CACHE = Path(__file__).parent / "cache.json"

response_cache = None

KEY_RATES = "rates"
KEY_ISIN_SYMB = "isin_symbol"
KEY_ETF_SYMB = "etf_symbol"
KEY_ISIN_PRICE = "isin_price"
KEY_SYMB_PRICE = "symbol_price"

DEFAULT_CACHE_DICT = {
    KEY_RATES: dict(),
    KEY_ISIN_SYMB: dict(),
    KEY_ETF_SYMB: dict(),
    KEY_ISIN_PRICE: dict(),
    KEY_SYMB_PRICE: dict(),
}


def _get(config: RequestConfig, query: Dict[str, str], url_format_dict: Dict[str, str]) -> dict:
    response = requests.get(
        config.url.format(**url_format_dict),
        headers=config.headers,
        params=query
    )
    return response.json()


@cache
def _get_currency_factor(source_currency: str, target_currency: str = "EUR"):
    resp_cache = _get_cache()
    if not resp_cache[KEY_RATES]:
        response_data = _get(
            CURRENCY,
            {"output":"JSON","base":"USD"},
            {}
        )

        resp_cache[KEY_RATES] = response_data.get("rates")

    return resp_cache[KEY_RATES].get(target_currency) / resp_cache[KEY_RATES].get(source_currency)


class ShareHook:

    @classmethod
    def __get_symbol_from_isin(cls, isin: str) -> Optional[str]:
        url = 'https://api.openfigi.com/v1/mapping'
        headers = {'Content-Type': 'text/json'}
        payload = [{"idType": "ID_ISIN", "idValue": isin}]

        r = requests.post(url, headers=headers, data=json.dumps(payload))
        response_data = r.json()

        if len(response_data) > 0:
            item = response_data[0]
            if "data" in item:
                data_items = item.get("data")
                if len(data_items) > 0:
                    stock_data = data_items[0]
                    symbol = stock_data.get("ticker")
                    logging.info(f"Symbol is {symbol!r}")
                    return symbol

    @classmethod
    def _get_symbol_from_isin(cls, isin: str) -> Optional[str]:
        return _get_or_create_from_cache(KEY_ISIN_SYMB, isin, cls.__get_symbol_from_isin)

    @classmethod
    def get_price_for_share_by_isin(cls, isin: str) -> float:
        symbol = cls._get_symbol_from_isin(isin)
        if symbol is None:
            logging.warning(f"Unable to determine symbol for ISIN {isin!r}")
        else:
            return cls.get_price_for_share_by_symbol(symbol)

        return 0.0

    @classmethod
    def __get_price_for_share_by_symbol(cls, symbol: str) -> float:
        logging.info(f"Determine price for share with symbol {symbol!r}")
        try:
            t = yfinance.Ticker(symbol)
            price = t.info.get("open")
            currency = t.info.get("currency")
            return price * _get_currency_factor(currency)
        except Exception as e:
            logging.warning(f"Unable to determine current price for ISIN {symbol!r}.\n{e}")

    @classmethod
    def get_price_for_share_by_symbol(cls, symbol: str) -> Optional[float]:
        return _get_or_create_from_cache(KEY_SYMB_PRICE, symbol, cls.__get_price_for_share_by_symbol)


class FundHook:

    @classmethod
    def __get_price_for_fund_by_isin(cls, isin: str) -> float:
        logging.info(f"Determine price for fund with ISIN {isin!r}")
        data = _get(FUNDS, {}, {"isin": isin})
        value = data.get('price', 0) * _get_currency_factor(data.get('market', 'EUR'))
        return value

    @classmethod
    def get_price_for_fund_by_isin(cls, isin: str) -> Optional[float]:
        return _get_or_create_from_cache(KEY_ISIN_PRICE, isin, cls.__get_price_for_fund_by_isin)


class CryptoHook:
    @classmethod
    def __get_price_for_crypto(cls, currency: str) -> float:
        logging.info(f"Determine exchange rate for crypto with currency {currency!r}")

        data = _get(
            ALPHA_VANTAGE,
            {"from_currency": currency, "function": "CURRENCY_EXCHANGE_RATE", "to_currency": "EUR"},
            {}
        )
        print("*", data)
        value = data.get('Realtime Currency Exchange Rate', {}).get('5. Exchange Rate')
        if value is not None:
            return float(value)

    @classmethod
    def get_price_for_crypto(cls, crypto_currency: str) -> Optional[float]:
        return _get_or_create_from_cache(KEY_SYMB_PRICE, crypto_currency, cls.__get_price_for_crypto)


class EtfHook:

    @classmethod
    def __get_symbol_for_etf(cls, etf_name: str) -> Optional[str]:
        logging.info(f"Determine symbol for ETF with name {etf_name!r}")
        data = _get(ALPHA_VANTAGE, {"keywords": etf_name, "function": "SYMBOL_SEARCH", "datatype": "json"}, {})
        matches = data.get('bestMatches', [])
        if matches:
            return matches[0].get("1. symbol")

    @classmethod
    def __get_price_for_etf_by_symbol(cls, etf_symbol: str) -> Optional[float]:
        logging.info(f"Determine price for ETF with symbol {etf_symbol!r}")
        data = _get(ALPHA_VANTAGE, {"function": "GLOBAL_QUOTE", "symbol": etf_symbol, "datatype": "json"}, {})
        price = float(data.get('Global Quote', {}).get('02. open', 0))

        if price == 0:
            try:
                t = yfinance.Ticker(etf_symbol)
                price = t.info.get("open")
                currency = t.info.get("currency")
                price = price * _get_currency_factor(currency)
            except Exception as e:
                logging.warning(f"Unable to determine current price for ISIN {etf_symbol!r}.\n{e}")

        return price

    @classmethod
    def _get_symbol_for_etf(cls, etf_name: str) -> Optional[str]:
        return _get_or_create_from_cache(KEY_ETF_SYMB, etf_name, cls.__get_symbol_for_etf)

    @classmethod
    def get_price_for_etf_by_name(cls, etf_name: str) -> Optional[float]:
        symbol = cls._get_symbol_for_etf(etf_name)
        if symbol:
            return cls.get_price_for_etf_by_symbol(symbol)

    @classmethod
    def get_price_for_etf_by_symbol(cls, etf_symbol: str) -> Optional[float]:
        if etf_symbol:
            return _get_or_create_from_cache(KEY_SYMB_PRICE, etf_symbol, cls.__get_price_for_etf_by_symbol)


def _get_or_create_from_cache(group: str, key: str, func: Callable) -> Any:
    """
    Generic function to ask response cache for value first, before sending http requests. If something was requested
        put into the cache
    :param group: the group key to use
    :param key: the actual id to search for
    :param func: the function to call (with they key as argument) if no nothing exists in cache
    :return: whatever the func would return
    """
    resp_cache = _get_cache()
    if key not in resp_cache[group]:
        r = func(key)
        if r is None:
            return None
        else:
            resp_cache[group][key] = r
            _dump_cache()
    return resp_cache[group][key]


def _get_cache() -> dict:
    global response_cache

    # not initialized yet
    if response_cache is None:
        # is there a cache from today?
        if PATH_CACHE.exists():
            # is the last data from today?
            if datetime.date.fromtimestamp(PATH_CACHE.stat().st_mtime) == datetime.date.today():
                with PATH_CACHE.open() as f:
                    response_cache = json.load(f)
            else:
                logging.info(f"Cache file outdatet. Create new cache.")
                response_cache = copy.deepcopy(DEFAULT_CACHE_DICT)
        else:
            logging.info(f"No cache file exists. Create new cache.")
            response_cache = copy.deepcopy(DEFAULT_CACHE_DICT)

    return response_cache


def _dump_cache() -> None:
    with PATH_CACHE.open("w") as f:
        json.dump(_get_cache(), f, indent="  ")


if __name__ == '__main__':
    dws_investa = "DE0008474008"
    dws_balance = "LU0309483435"
    isin_rize = "IE00BLRPQH31"

    # print(get_symbol_from_isin(dws_investa)) # DWS Investa
    # print(get_price_for_fund(dws_balance))  # DWS Balance
    # print(get_symbol_from_isin(isin_rize))
    # print(get_price_for_crypto("XLM"))
    logging.basicConfig(level=logging.DEBUG)

    # print(FundHook.get_price_for_fund_by_isin('LU0309483435'))  # DWS Balance
    # print(CryptoHook.get_price_for_crypto("XLM"))
    # print(FundHook.get_price_for_fund_by_isin(dws_investa))  # DWS Investa

    rize = 'FOOD.L'
    t = yfinance.Ticker(rize)
    pprint(t.info)


