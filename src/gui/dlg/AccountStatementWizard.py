#  -*- coding: cp1252 german encoding -*-
import json
import logging

import wx
from wx.grid import Grid
from wx.lib.masked import NumCtrl

from db.DatabaseManager import DatabaseManager as DBM
from db.DbModelClasses import Wallet
from filehandler import MarkupHelper, AccountStatementParser
from gui.bmp import images
from resources import String

__author__ = "Tobias Fochtmann"


class AccountStatementWizard(wx.Dialog):
    """
    Dialog for editing the account statement format; setting columns of interest
    """
    ID_POPUP_ROW_FIRST = wx.NewId()
    ID_POPUP_ROW_LAST = wx.NewId()
    ID_POPUP_COL_DATE = wx.NewId()
    ID_POPUP_COL_EARNING = wx.NewId()
    ID_POPUP_COL_EXPENSE = wx.NewId()
    ID_POPUP_COL_SRCDEST = wx.NewId()
    ID_POPUP_COL_PURPOSE = wx.NewId()
    ID_POPUP_COL_IBAN = wx.NewId()
    ID_POPUP_COL_BIC = wx.NewId()

    def __init__(self, parent, wallet):
        """
        Constructor
        :param parent: parent window
        :param wallet: the wallet
        :type wallet: Wallet
        """
        wx.Dialog.__init__(self, parent)
        self.SetIcon(images.MAGIC_WAND.GetIcon())

        self.__wallet = wallet
        self.SetTitle("Configuration of account statement format of wallet {}".format(self.__wallet.name))
        self.__rightClickCol = None
        self.__rightClickRow = None
        self.__filePath = None

        self._createControls()
        self._setToolTips()
        self._setLayout()
        self._bindEvents()
        self._initLoad()

        self.CenterOnParent()

    def _createControls(self):
        """
        Create controls and widgets
        """
        self.sizer = wx.GridBagSizer(hgap=10, vgap=10)
        self.sizer_btn = wx.BoxSizer(wx.HORIZONTAL)

        self.panel = wx.Panel(self)

        self.btn_open = wx.BitmapButton(self.panel, bitmap=images.FOLDER_DOCUMENT.GetBitmap())
        self.btn_test = wx.BitmapButton(self.panel, bitmap=images.TEXT_FIND.GetBitmap())
        self.btn_apply = wx.BitmapButton(self.panel, bitmap=images.APPLY.GetBitmap())
        self.btn_discard = wx.BitmapButton(self.panel, bitmap=images.DISCARD.GetBitmap())

        self.lbl_sep = wx.StaticText(self.panel, label="Separator")
        self.lbl_skipFirstRows = wx.StaticText(self.panel, label="Skip ... first rows")
        self.lbl_skipLastRows = wx.StaticText(self.panel, label="Skip ... last rows")
        self.lbl_dateCol = wx.StaticText(self.panel, label="Date column")
        self.lbl_earnCol = wx.StaticText(self.panel, label="Earnings column")
        self.lbl_expenseCol = wx.StaticText(self.panel, label="Expense column")
        self.lbl_srcDestCol = wx.StaticText(self.panel, label="Source/Destination column")
        self.lbl_purposeCol = wx.StaticText(self.panel, label="Purpose column")
        self.lbl_ibanCol = wx.StaticText(self.panel, label="IBAN column")
        self.lbl_bicCol = wx.StaticText(self.panel, label="BIC column")
        self.lbl_explanation = wx.StaticText(self.panel, label="Set format by right clicking column or row headers")
        self.lbl_explanation.SetForegroundColour(wx.RED)

        numCtrlKw = {"allowNone": True, "min": 0, "style": wx.TE_READONLY, "integerWidth": 2, "value": None}
        self.txt_sep = wx.TextCtrl(self.panel, size=(30, 24), style=wx.TE_READONLY)
        self.txt_skipFirstRows = NumCtrl(self.panel, **numCtrlKw)
        self.txt_skipLastRows = NumCtrl(self.panel, **numCtrlKw)
        self.txt_dateCol = NumCtrl(self.panel, **numCtrlKw)
        self.txt_earnCol = NumCtrl(self.panel, **numCtrlKw)
        self.txt_expenseCol = NumCtrl(self.panel, **numCtrlKw)
        self.txt_srcDestCol = NumCtrl(self.panel, **numCtrlKw)
        self.txt_purposeCol = NumCtrl(self.panel, **numCtrlKw)
        self.txt_ibanCol = NumCtrl(self.panel, **numCtrlKw)
        self.txt_bicCol = NumCtrl(self.panel, **numCtrlKw)

        self.list_results = wx.ListCtrl(self.panel, style=wx.LC_REPORT)

        self.grid_file = Grid(self.panel, size=(1000, 400))

    def _setToolTips(self):
        """
        Set tool tips
        """
        self.btn_open.SetToolTip("Open account statement to parse")
        self.btn_test.SetToolTip("Test current configuration and read")
        self.btn_apply.SetToolTip("Apply changes to database and close dialog")
        self.btn_discard.SetToolTip("Discard changes and close dialog")

    def _setLayout(self):
        """
        Positioning all controls of the panel
        """
        PADDING = 10

        self.sizer_btn.Add(self.btn_open)
        self.sizer_btn.Add(self.btn_test)

        line = 0
        self.sizer.Add(self.sizer_btn, (line, 0), (1, 1), wx.LEFT | wx.TOP, PADDING)
        line += 1
        self.sizer.Add(self.lbl_sep, (line, 0), (1, 1), wx.LEFT | wx.ALIGN_CENTER_VERTICAL, PADDING)
        self.sizer.Add(self.txt_sep, (line, 1), (1, 1))
        self.sizer.Add(self.lbl_skipFirstRows, (line, 2), (1, 1), wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 3 * PADDING)
        self.sizer.Add(self.txt_skipFirstRows, (line, 3), (1, 1))
        self.sizer.Add(self.lbl_skipLastRows, (line, 4), (1, 1), wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 3 * PADDING)
        self.sizer.Add(self.txt_skipLastRows, (line, 5), (1, 1))
        self.sizer.Add(self.lbl_dateCol, (line, 6), (1, 1), wx.LEFT | wx.ALIGN_CENTER_VERTICAL, PADDING)
        self.sizer.Add(self.txt_dateCol, (line, 7), (1, 1))
        line += 1
        self.sizer.Add(self.lbl_earnCol, (line, 0), (1, 1), wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 3 * PADDING)
        self.sizer.Add(self.txt_earnCol, (line, 1), (1, 1))
        self.sizer.Add(self.lbl_expenseCol, (line, 2), (1, 1), wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 3 * PADDING)
        self.sizer.Add(self.txt_expenseCol, (line, 3), (1, 1))
        self.sizer.Add(self.lbl_srcDestCol, (line, 4), (1, 1), wx.LEFT | wx.ALIGN_CENTER_VERTICAL, PADDING)
        self.sizer.Add(self.txt_srcDestCol, (line, 5), (1, 1))
        self.sizer.Add(self.lbl_purposeCol, (line, 6), (1, 1), wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 3 * PADDING)
        self.sizer.Add(self.txt_purposeCol, (line, 7), (1, 1))
        line += 1
        self.sizer.Add(self.lbl_ibanCol, (line, 0), (1, 1), wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 3 * PADDING)
        self.sizer.Add(self.txt_ibanCol, (line, 1), (1, 1))
        self.sizer.Add(self.lbl_bicCol, (line, 2), (1, 1), wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 3 * PADDING)
        self.sizer.Add(self.txt_bicCol, (line, 3), (1, 1))
        line += 1
        self.sizer.Add(self.lbl_explanation, (line, 0), (1, 8), wx.LEFT, PADDING)
        line += 1
        self.sizer.Add(self.grid_file, (line, 0), (1, 8), wx.EXPAND | wx.LEFT | wx.RIGHT, PADDING)
        line += 1
        self.sizer.Add(self.list_results, (line, 0), (1, 8), wx.EXPAND | wx.LEFT | wx.RIGHT, PADDING)
        line += 1
        self.sizer.Add(self.btn_apply, (line, 3), (1, 1), wx.ALIGN_RIGHT | wx.BOTTOM, PADDING)
        self.sizer.Add(self.btn_discard, (line, 4), (1, 1), wx.ALIGN_LEFT | wx.BOTTOM, PADDING)

        self.panel.SetSizerAndFit(self.sizer)
        self.SetSize(self.GetBestSize())

    def _bindEvents(self):
        """
        Associate events with actions
        """
        self.btn_open.Bind(wx.EVT_BUTTON, self.__onOpenAS)
        self.btn_test.Bind(wx.EVT_BUTTON, self.__onTestFormat)
        self.btn_apply.Bind(wx.EVT_BUTTON, self.__onApply)
        self.btn_discard.Bind(wx.EVT_BUTTON, self.__onDiscard)

        self.grid_file.Bind(wx.grid.EVT_GRID_LABEL_RIGHT_CLICK, self.__onLabelRightClick)
        self.grid_file.Bind(wx.EVT_MENU, self.__onSetRowFirst, id=self.ID_POPUP_ROW_FIRST)
        self.grid_file.Bind(wx.EVT_MENU, self.__onSetRowLast, id=self.ID_POPUP_ROW_LAST)
        self.grid_file.Bind(wx.EVT_MENU, self.__onSetColDate, id=self.ID_POPUP_COL_DATE)
        self.grid_file.Bind(wx.EVT_MENU, self.__onSetColEarning, id=self.ID_POPUP_COL_EARNING)
        self.grid_file.Bind(wx.EVT_MENU, self.__onSetColExpense, id=self.ID_POPUP_COL_EXPENSE)
        self.grid_file.Bind(wx.EVT_MENU, self.__onSetColSrcDest, id=self.ID_POPUP_COL_SRCDEST)
        self.grid_file.Bind(wx.EVT_MENU, self.__onSetColPurpose, id=self.ID_POPUP_COL_PURPOSE)
        self.grid_file.Bind(wx.EVT_MENU, self.__onSetColIban, id=self.ID_POPUP_COL_IBAN)
        self.grid_file.Bind(wx.EVT_MENU, self.__onSetColBic, id=self.ID_POPUP_COL_BIC)

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        self.btn_test.Enable(self.__filePath is not None)

        self.list_results.AppendColumn("Date")
        self.list_results.AppendColumn("Amount", wx.LIST_FORMAT_RIGHT)
        self.list_results.AppendColumn("Purpose", width=300)
        self.list_results.AppendColumn("Source/Destination", width=300)
        self.list_results.AppendColumn("IBAN", width=150)
        self.list_results.AppendColumn("BIC", width=150)

        if self.__wallet.as_format_info:
            try:
                formatInfo = json.loads(self.__wallet.as_format_info)
                if formatInfo:
                    self.txt_sep.SetValue(formatInfo.get("separator"))
                    self.txt_skipFirstRows.SetValue(formatInfo.get("row_skipFirst"))
                    self.txt_skipLastRows.SetValue(formatInfo.get("row_skipLast"))
                    self.txt_dateCol.SetValue(formatInfo.get("col_date"))
                    self.txt_earnCol.SetValue(formatInfo.get("col_earn"))
                    self.txt_expenseCol.SetValue(formatInfo.get("col_expe"))
                    self.txt_srcDestCol.SetValue(formatInfo.get("col_srcDest"))
                    self.txt_purposeCol.SetValue(formatInfo.get("col_purpose"))
                    self.txt_ibanCol.SetValue(formatInfo.get("col_iban"))
                    self.txt_bicCol.SetValue(formatInfo.get("col_bic"))
            except ValueError:
                logging.warning("Formatting info is no json and will be ignored")

    def _fillGrid(self):
        """
        (Re-) fills the grid
        """
        # get lines
        sep = self.txt_sep.GetValue()
        if sep == "\\t":
            sep = "\t"

        lines = MarkupHelper.getCsvFileContent(self.__filePath, sep)
        columns = max([len(lineCells) for lineCells in lines])

        self.grid_file.CreateGrid(len(lines), columns)
        self.grid_file.SetRowLabelSize(30)
        self.grid_file.SetColLabelSize(20)

        # iterate through lines
        for rowId, lineCells in enumerate(lines):
            self.grid_file.SetRowLabelValue(rowId, str(rowId))
            # iterate through columns
            for colId, cell in enumerate(lineCells):
                self.grid_file.SetColLabelValue(colId, str(colId))
                self.grid_file.SetCellValue(rowId, colId, cell)

        self.panel.Fit()
        self.SetSize(self.GetBestSize())

    def _fillList(self):
        """
        Fills the list
        """
        # Clear list
        self.list_results.DeleteAllItems()

        # get transactions from given file
        tList = AccountStatementParser.parseAccountStatement(self.__filePath,
                                                             self.getFormatInfo(),
                                                             self.__wallet.id)
        # append to list
        for transaction in tList:
            self.list_results.Append((transaction.date.strftime(String.FMT_DATE),
                                      transaction.amount,
                                      transaction.purpose,
                                      transaction.src_dest,
                                      transaction.iban or "",
                                      transaction.bic or ""), )

    def _checkValues(self):
        """
        :return: True if all values are set correctly
        :rtype: bool
        """
        if not self.txt_sep.GetValue():
            logging.warning("A separator needs to be set. Import the account statement again.")
            return False

        if not isinstance(self.txt_skipFirstRows.GetValue(), (int, float)):
            logging.warning("Please set the first row")
            return False

        if not isinstance(self.txt_skipLastRows.GetValue(), (int, float)):
            logging.warning("Please set the last row")
            return False

        if not isinstance(self.txt_dateCol.GetValue(), (int, float)):
            logging.warning("Please set a column containing the date")
            return False

        if not isinstance(self.txt_earnCol.GetValue(), (int, float)):
            logging.warning("Please set a column containing earnings")
            return False

        if not isinstance(self.txt_expenseCol.GetValue(), (int, float)):
            logging.warning("Please set a column containing expenses")
            return False

        if not isinstance(self.txt_srcDestCol.GetValue(), (int, float)):
            logging.warning("Please set a column containing the source of the transaction")
            return False

        return True

    def getFormatInfo(self):
        """
        :return: Returns the configured formatting info
        :rtype: dict
        """
        return {
            "separator": self.txt_sep.GetValue(),
            "row_skipFirst": self.txt_skipFirstRows.GetValue(),
            "row_skipLast": self.txt_skipLastRows.GetValue(),
            "col_date": self.txt_dateCol.GetValue(),
            "col_earn": self.txt_earnCol.GetValue(),
            "col_expe": self.txt_expenseCol.GetValue(),
            "col_srcDest": self.txt_srcDestCol.GetValue(),
            "col_purpose": self.txt_purposeCol.GetValue(),
            "col_iban": self.txt_ibanCol.GetValue(),
            "col_bic": self.txt_bicCol.GetValue(),
        }

    # ----- EVENTS ------------
    # noinspection PyUnusedLocal
    def __onOpenAS(self, evt):
        """
        Triggered when button to open an account statement is clicked
        :param evt:
        """
        # get filedialog for choosing
        fileDlg = wx.FileDialog(self,
                                message=String.FILEDLG_MESSAGE_ACCOUNTSTATEMENT,
                                style=wx.FD_FILE_MUST_EXIST | wx.FD_OPEN)

        # show file dialog
        if fileDlg.ShowModal() == wx.ID_OK:
            self.__filePath = fileDlg.GetPath()
            self.btn_test.Enable()

            if not self.txt_sep.GetValue():
                sepDlg = wx.TextEntryDialog(self,
                                            message=String.DLG_MSG_SEPARATOR,
                                            caption=String.DLG_TITLE_SEPARATOR)

                if sepDlg.ShowModal() == wx.ID_OK:
                    self.txt_sep.SetValue(sepDlg.GetValue())

            self._fillGrid()

    # noinspection PyUnusedLocal
    def __onTestFormat(self, evt):
        """
        Triggeredn when button to test the configured format is clicked
        :param evt:
        """
        if self._checkValues():
            self._fillList()

    # noinspection PyUnusedLocal
    def __onApply(self, evt):
        """
        :param evt: Triggered when button 'apply' is clicked
        :type evt: wx.CommandEvent
        """
        if self._checkValues():
            # self.__wallet.as_format_info = json.dumps(self.getFormatInfo())
            # DBM().update(self.__wallet)
            self.EndModal(wx.ID_OK)

    # noinspection PyUnusedLocal
    def __onDiscard(self, evt):
        """
        :param evt: Triggered when button 'discard' is clicked
        :type evt: wx.CommandEvent
        """
        self.EndModal(wx.ID_CANCEL)
        self.Close()

    def __onLabelRightClick(self, evt):
        """
        Shows popup menu for setting format
        :param evt: Triggered when left clicked on label (row or column headers)
        :type evt: wx.grid.GridEvent
        """
        menu = None
        # if clicked on row headers
        if evt.GetCol() == -1 and evt.GetRow() != -1:
            menu = self.__getRowContextMenu(evt.GetRow())
        elif evt.GetRow() == -1 and evt.GetCol() != -1:
            menu = self.__getColContextMenu(evt.GetCol())
        wx.CallAfter(self.grid_file.PopupMenu, menu)

    # noinspection PyUnusedLocal
    def __onSetRowFirst(self, evt):
        """
        Triggered when menu item for setting the first row is clicked
        :param evt: the menu item event
        :param evt: wx.MenuEvent
        """
        self.txt_skipFirstRows.SetValue(self.__rightClickRow)

    # noinspection PyUnusedLocal
    def __onSetRowLast(self, evt):
        """
        Triggered when menu item for setting the last row is clicked
        :param evt: the menu item event
        :param evt: wx.MenuEvent
        """
        self.txt_skipLastRows.SetValue(self.grid_file.GetNumberRows() - 1 - self.__rightClickRow)

    # noinspection PyUnusedLocal
    def __onSetColDate(self, evt):
        """
        Triggered when menu item for setting the date column is clicked
        :param evt: the menu item event
        :param evt: wx.MenuEvent
        """
        self.txt_dateCol.SetValue(self.__rightClickCol)

    # noinspection PyUnusedLocal
    def __onSetColEarning(self, evt):
        """
        Triggered when menu item for setting the earnings column is clicked
        :param evt: the menu item event
        :param evt: wx.MenuEvent
        """
        self.txt_earnCol.SetValue(self.__rightClickCol)

    # noinspection PyUnusedLocal
    def __onSetColExpense(self, evt):
        """
        Triggered when menu item for setting expense column is clicked
        :param evt: the menu item event
        :param evt: wx.MenuEvent
        """
        self.txt_expenseCol.SetValue(self.__rightClickCol)

    # noinspection PyUnusedLocal
    def __onSetColSrcDest(self, evt):
        """
        Triggered when menu item for setting the source/destination column is clicked
        :param evt: the menu item event
        :param evt: wx.MenuEvent
        """
        self.txt_srcDestCol.SetValue(self.__rightClickCol)

    # noinspection PyUnusedLocal
    def __onSetColPurpose(self, evt):
        """
        Triggered when menu item for setting the purpose column is clicked
        :param evt: the menu item event
        :param evt: wx.MenuEvent
        """
        self.txt_purposeCol.SetValue(self.__rightClickCol)

    # noinspection PyUnusedLocal
    def __onSetColIban(self, evt):
        """
        Triggered when menu item for setting the iban column is clicked
        :param evt: the menu item event
        :param evt: wx.MenuEvent
        """
        self.txt_ibanCol.SetValue(self.__rightClickCol)

    # noinspection PyUnusedLocal
    def __onSetColBic(self, evt):
        """
        Triggered when menu item for setting the bic column is clicked
        :param evt: the menu item event
        :param evt: wx.MenuEvent
        """
        self.txt_bicCol.SetValue(self.__rightClickCol)

    # ----- END OF EVENTS -----

    def __getRowContextMenu(self, row):
        """
        :param row: row index
        :type row: int
        :return: popup menu for right clicked row
        :rtype: wx.Menu
        """
        self.__rightClickCol = None
        self.__rightClickRow = row

        # create menu
        menu = wx.Menu("Set row {} as...".format(row))
        menu.Append(wx.MenuItem(menu, self.ID_POPUP_ROW_FIRST, "First row"))
        menu.Append(wx.MenuItem(menu, self.ID_POPUP_ROW_LAST, "Last row"))

        return menu

    def __getColContextMenu(self, col):
        """
        :param col: column index
        :type col: int
        :return: popup menu for right clicked column
        :rtype: wx.Menu
        """
        self.__rightClickCol = col
        self.__rightClickRow = None

        # create menu
        menu = wx.Menu("Set column {} as...".format(col))
        menu.Append(wx.MenuItem(menu, self.ID_POPUP_COL_DATE, "Date"))
        menu.Append(wx.MenuItem(menu, self.ID_POPUP_COL_EARNING, "Earning"))
        menu.Append(wx.MenuItem(menu, self.ID_POPUP_COL_EXPENSE, "Expense"))
        menu.Append(wx.MenuItem(menu, self.ID_POPUP_COL_SRCDEST, "Source/Destination"))
        menu.Append(wx.MenuItem(menu, self.ID_POPUP_COL_PURPOSE, "Purpose"))
        menu.Append(wx.MenuItem(menu, self.ID_POPUP_COL_IBAN, "IBAN"))
        menu.Append(wx.MenuItem(menu, self.ID_POPUP_COL_BIC, "BIC"))

        return menu


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    app = wx.App()

    from db.DatabaseService import WalletService

    wallet = WalletService.getByName("DB Giro")
    # wallet.as_format_info = '{"row_skipFirst": 1, "row_skipLast": 0, "separator": "\\t", "col_date": 0,"col_expe": 7, "col_earn": 7, "col_srcDest": 3}'
    mainFrame = AccountStatementWizard(None, wallet)
    mainFrame.Show()
    app.MainLoop()
