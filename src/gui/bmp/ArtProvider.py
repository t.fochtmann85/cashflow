#  -*- coding: cp1252 german encoding -*-
__author__ = "Tobias Fochtmann"

import os

import wx
from wx.tools.img2py import img2py

import constants
from gui.bmp import images


def getAllBitmapNames():
    return list(images.catalog.keys())


def getBitmap(name, size=None):
    bitmap = images.catalog.get(name).getBitmap()
    if size is not None:
        image = bitmap.ConvertToImage()
        image = image.Scale(size[0], size[1], wx.IMAGE_QUALITY_HIGH)
        bitmap = wx.Bitmap(image)
    return bitmap


def setBitmapSize(bitmap, size):
    image = wx.ImageFromBitmap(bitmap)
    image = image.Scale(size[0], size[1], wx.IMAGE_QUALITY_HIGH)
    bitmap = wx.BitmapFromImage(image)
    return bitmap


def generateBinaryFromBitmaps():
    target = os.path.join(constants.DIR_PRJ, "src", "gui", "bmp", "images.py")

    idx = 0
    for imgFile in os.listdir(os.path.join(constants.DIR_PRJ, "res", "img")):
        print(imgFile)
        imgName, imgExt = os.path.splitext(imgFile)
        imgName = imgName.upper()
        imgExt = imgExt.lower()
        if imgExt not in [".png", ".ico", ".jpg"]:
            continue

        img2py(os.path.join(constants.DIR_PRJ, "res", "img", imgFile),
               target, append=idx > 0, imgName=imgName, icon=True, compressed=True, catalog=True)
        idx += 1


if __name__ == "__main__":
    generateBinaryFromBitmaps()
