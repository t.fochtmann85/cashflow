from typing import List

from db.DatabaseManager import DatabaseManager
from db.DatabaseService import CategoryService, TransactionService
from db.DbModelClasses import Category, Transaction


def funds():
    target_category = CategoryService.get_by_name("Wertpapier")

    for fund_name in [
        "Fonds Investa",
        "Fonds Euroland",
        "Fonds Top Dividende",
        "Fonds Vermoeg.",
        "Fonds INHABER-ANT EILE LC"
    ]:
        category = CategoryService.get_by_name(fund_name)
        print(category, "->", target_category)
        TransactionService.replaceCategory(category, target_category)


def _get_transactions_with_category(category: Category) -> List[Transaction]:
    return DatabaseManager().session. \
        query(Transaction). \
        filter(Transaction.category == category).\
        all()

if __name__ == '__main__':
    funds()