#  -*- coding: cp1252 german encoding -*-
import wx
import wx.lib.newevent
from wx.gizmos import TreeListCtrl
from wx.lib.mixins.listctrl import ListCtrlAutoWidthMixin
from wx.lib.mixins.treemixin import VirtualTree

from db.DbModelClasses import Transaction
from resources import String

__author__ = "Tobias Fochtmann"


class FilterTreeListCtrl(VirtualTree, TreeListCtrl, ListCtrlAutoWidthMixin):

    def __init__(self, parent, **kwargs):
        """
        The constructor
        :param parent: parent window
        :param columnList: list of column definitions
        :param itemList: list of items
        :param listMinSize: minimum size of the list
        :param listStyle: style of the list (has to be virtual!)
        :param itemValueFunc: callable object for getting the value for the desired column from the (object) item
        :param itemChildrenCountFunc: callable object for getting the amount of children for a given item
        :param filterValueFunc: callable object for getting the value which is used for filtering
        :param sortValueFunc: callable object for getting the value which is used for sorting
        :param preSelectedTuple: set a preselected item or None
        """
        self.__itemValueFunc = kwargs.get("itemValueFunc")
        self.__itemChildrenCountFunc = kwargs.get("itemChildrenCountFunc")
        self.__filterValueFunc = kwargs.get("filterValueFunc")
        self.__sortValueFunc = kwargs.get("sortValueFunc")
        self.__columnList = kwargs.get("columnList")
        self.__itemList = kwargs.get("itemList")
        self.__filteredItemList = kwargs.get("itemList")
        self.__listMinSize = kwargs.get("listMinSize")
        self.__listStyle = kwargs.get("listStyle")
        self.__preSelectedTuple = kwargs.get("preSelectedTuple")

        super(FilterTreeListCtrl, self).__init__(parent, size=self.__listMinSize, style=self.__listStyle)
        ListCtrlAutoWidthMixin.__init__(self)
        self.SetMinSize(self.__listMinSize)

        self.__selIndex = -1
        self.__sortColumn = -1
        self.__sortOrder = 0

        # events
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self._onItemSelectionChange)

        # insert columns
        for (colHeader, initWidth, inputType, inputStyle) in self.__columnList:
            self.AddColumn(colHeader, initWidth)

    def GetListCtrl(self):
        return self

    # --- EVENTS ---

    def _onItemSelectionChange(self, event):
        """
        Triggered when a list item is selected
        :param event:
        """
        treeItemId = event.GetItem()
        if event.GetItem().IsOk() and self.GetRootItem() != treeItemId:
            self.EnsureVisible(event.GetItem())
            self.__selIndex = self.GetIndexOfItem(event.GetItem())[0]
        else:
            self.__selIndex = -1
        wx.PostEvent(self, FilterListView.FilterListViewSelectionChangedEvent(self.GetId()))

    # --- END EVENTS ---

    # --- VIRTUALNESS OF THE LIST ---

    def OnGetItemText(self, index, column=0):
        """
        Overwrite super method for virtualness
        :param index:
        :param column:
        :return: the text to be shown
        """
        # root
        if len(index) == 0:
            return "Root item, nobody will see this"
        else:
            return self.__itemValueFunc(self.getListItemByIndex(index[0]), index, column)

    def OnGetChildrenCount(self, index):
        """
        Overwrite super method for virtualness
        :param index: index of the item to get the text for (is a tuple)
        :return: the children count
        """
        # root
        if len(index) == 0:
            return len(self.__filteredItemList)
        else:
            return self.__itemChildrenCountFunc(self.getListItemByIndex(index[0]), index)

    # --- +++++++++++++++++++++++ ---

    def getListItemByIndex(self, itemIndex):
        """
        Returns the list item by its index
        :param itemIndex:
        """
        return self.__filteredItemList[itemIndex]

    def getSelectedListItem(self):
        """
        Returns the selected list item
        """
        # get currently selected item
        treeItemId = self.GetSelection()

        # check if it really selected
        if treeItemId.IsOk() and self.GetRootItem() != treeItemId:
            # get the index behind this tree item
            index = self.GetIndexOfItem(treeItemId)

            # get the corresponding real item object
            return self.__filteredItemList[index[0]]

    def isItemSelected(self):
        """
        Determines if any item is selected
        """
        return self.GetSelection().IsOk()

    def getSelectedIndex(self):
        """
        Returns the selected index
        """
        return self.__selIndex

    def setListItems(self, itemList):
        """
        Sets the data basis of the list items
        :param itemList: list of tuples, one tuple for each item
        """
        # set list of items
        self.__itemList = itemList

        # select the preselected item if any
        treeItemId = self.getTreeItemIdByItem(self.__preSelectedTuple)

        if treeItemId is not None:
            self.SelectItem(treeItemId)

    def getListItems(self):
        """
        Returns the list of items
        """
        return self.__itemList

    def setPreSelected(self, preSelectedTuple):
        """
        Sets the preselected tuple
        :param preSelectedTuple:
        """
        self.__preSelectedTuple = preSelectedTuple

    def filterAndSort(self, filterValueList, clickedAtCol):
        """
        Filter and sort the list item by the given parameters

        :param filterValueList: list of filter values
        :param clickedAtCol: when clicked on a column -> its column number; else None
        """
        # get the currently selected tuple
        curSelItem = self.getSelectedListItem()

        # when clicked at a column, the sorting order has to change
        if clickedAtCol is not None:

            # when it is the same column
            if self.__sortColumn == clickedAtCol:

                # generate the sorting order
                self.__sortOrder = (self.__sortOrder + 1 + 1) % 3 - 1

            # when another column is clicked
            else:

                # set the sorting order to ascending
                self.__sortColumn = clickedAtCol
                self.__sortOrder = 1

        # if no column is clicked, keep the sorting order (do nothing)
        else:
            pass

        # now, apply the filter
        whereFilterApplies = []

        # iterate through original list
        for listItem in self.__itemList:

            # init logic value
            applies = True

            # iterate through filter values
            for (col, filterValue) in enumerate(filterValueList):
                applies &= filterValue.lower() in self.__filterValueFunc(listItem, col).lower()

            # if still applies
            if applies:
                whereFilterApplies.append(listItem)

        # sort by column
        if self.__sortColumn is None or self.__sortOrder == 0:
            self.__filteredItemList = whereFilterApplies
        else:
            self.__filteredItemList = sorted(whereFilterApplies,
                                             key=lambda item: self.__sortValueFunc(item, self.__sortColumn),
                                             reverse=True if self.__sortOrder == -1 else False)
        self.RefreshItems()

        # assure that the currently selected items stays selected
        treeItemId = self.getTreeItemIdByItem(curSelItem)
        if treeItemId is not None:
            self.SelectItem(treeItemId)

    def getTreeItemIdByItem(self, item):
        """
        Returns the tree item id by a given list object
        :param item:
        """

        # check valid value
        if item is not None and item in self.__filteredItemList:

            # get index of preselection
            index = (self.__filteredItemList.index(item),)

            # get tree item id by index
            treeItemId = self.GetItemByIndex(index)

            # check valid
            if treeItemId.IsOk():
                return treeItemId

    def getItemCount(self):
        """
        Returns the length of the original item list
        """
        return len(self.__itemList)

    def getFilteredItemCount(self):
        """
        Returns the length of the filtered item list
        """
        return len(self.__filteredItemList)


class FilterListCtrl(wx.ListCtrl, ListCtrlAutoWidthMixin):

    def __init__(self, parent, **kwargs):
        """
        The constructor
        :param parent: parent window
        :param columnList: list of column definitions
        :param itemList: list of items
        :param listMinSize: minimum size of the list
        :param listStyle: style of the list (has to be virtual!)
        :param itemValueFunc: callable object for getting the value for the desired column from the (object) item
        :param filterValueFunc: callable object for getting the value which is used for filtering
        :param sortValueFunc: callable object for getting the value which is used for sorting
        :param preSelectedTuple: set a preselected item or None
        """
        self.__itemValueFunc = kwargs.get("itemValueFunc")
        self.__filterValueFunc = kwargs.get("filterValueFunc")
        self.__sortValueFunc = kwargs.get("sortValueFunc")
        self.__columnList = kwargs.get("columnList")
        self.__itemList = kwargs.get("itemList")
        self.__filteredItemList = kwargs.get("itemList")
        self.__listMinSize = kwargs.get("listMinSize")
        self.__listStyle = kwargs.get("listStyle")
        self.__preSelectedTuple = kwargs.get("preSelectedTuple")

        self.__itemAttrDict = dict()
        self.__createItemAttr()

        # check list style
        if self.__listStyle & wx.LC_VIRTUAL != wx.LC_VIRTUAL:
            raise AttributeError("List has to be virtual! Add wx.LC_VIRTUAL to your style definition.")

        wx.ListCtrl.__init__(self, parent, size=self.__listMinSize, style=self.__listStyle)
        ListCtrlAutoWidthMixin.__init__(self)
        self.SetMinSize(self.__listMinSize)

        self.__selIndex = -1
        self.__sortColumn = -1
        self.__sortOrder = 0

        # events
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self._onItemSelected)
        self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self._onItemDeselected)
        self.Bind(wx.EVT_LIST_BEGIN_DRAG, self._startDrag)

        # insert columns
        for (colHeader, initWidth, inputType, inputStyle) in self.__columnList:
            self.InsertColumn(self.GetColumnCount(), colHeader, width=initWidth)

    def __createItemAttr(self):
        """
        """
        # unselected known
        attr1 = wx.ItemAttr()
        # attr1.SetBackgroundColour(String.COL_THEME_2)
        # attr1.SetTextColour(String.COL_THEME_5)

        # unselected unknown
        attr2 = wx.ItemAttr()
        # attr2.SetBackgroundColour(String.COL_THEME_2)
        attr2.SetTextColour(String.COL_THEME_6)

        # selected known
        attr3 = wx.ItemAttr()
        # attr3.SetBackgroundColour(String.COL_THEME_1)
        # attr3.SetTextColour(String.COL_THEME_3)

        # selected unknown
        attr4 = wx.ItemAttr()
        # attr4.SetBackgroundColour(String.COL_THEME_1)
        attr4.SetTextColour(String.COL_THEME_6)

        self.__itemAttrDict["uk"] = attr1
        self.__itemAttrDict["uu"] = attr2
        self.__itemAttrDict["sk"] = attr3
        self.__itemAttrDict["su"] = attr4

    def GetListCtrl(self):
        return self

    # --- EVENTS ---

    def _onItemSelected(self, event):
        """
        Triggered when a list item is selected
        :param event:
        """
        self.EnsureVisible(event.GetIndex())
        self.__selIndex = event.GetIndex()
        wx.PostEvent(self, FilterListView.FilterListViewItemSelectedEvent(self.GetId()))

    def _onItemDeselected(self, event):
        """
        Triggered when a list item is deselected
        :param event:
        """
        self.__selIndex = -1
        wx.PostEvent(self, FilterListView.FilterListViewItemDeselectedEvent(self.GetId()))

    def _startDrag(self, event):
        """
        Triggered when an item is dragged
        :param event:
        """
        data = wx.TextDataObject()
        data.SetText(str([transaction.id for transaction in self.getSelectedListItems()]))

        # Create drop source and begin drag-and-drop.
        dropSource = wx.DropSource(self)
        dropSource.SetData(data)
        res = dropSource.DoDragDrop(flags=wx.Drag_DefaultMove)

        if res == wx.DragMove:
            wx.PostEvent(self, FilterListView.ItemDataChanged(self.GetId()))

    # --- END EVENTS ---

    # --- VIRTUALNESS OF THE LIST ---

    def OnGetItemText(self, item, col):
        """
        Overwrite super method for virtualness
        :param item: index of the item to get the text for
        :param col: index of the column to be filled
        :return: the text to be shown
        """
        return self.__itemValueFunc(self.getListItemByIndex(item), col)

    def OnGetItemAttr(self, item):
        item_obj = self.getListItemByIndex(item)
        if self.IsSelected(item):
            if isinstance(item_obj, Transaction) and item_obj.category_id == 77:
                attr_name = "su"
            else:
                attr_name = "sk"
        else:
            if isinstance(item_obj, Transaction) and item_obj.category_id == 77:
                attr_name = "uu"
            else:
                attr_name = "uk"

        return self.__itemAttrDict[attr_name]

    # --- +++++++++++++++++++++++ ---

    def getListItemByIndex(self, itemIndex):
        """
        Returns the list item by its index
        :param itemIndex:
        """
        return self.__filteredItemList[itemIndex]

    def getSelectedListItem(self):
        """
        Returns the selected list item
        """
        if self.getSelectedIndex() != -1:
            return self.getListItemByIndex(self.getSelectedIndex())

    def getSelectedListItems(self):
        """
        Returns the selected list item
        """
        item_list = list()

        sel_item = self.GetFirstSelected()

        while sel_item > -1:
            item_list.append(self.getListItemByIndex(sel_item))
            sel_item = self.GetNextSelected(sel_item)

        return item_list

    def isItemSelected(self):
        """
        Determines if any item is selected
        """
        return self.__selIndex != -1

    def getSelectedIndex(self):
        """
        Returns the selected index
        """
        return self.__selIndex

    def setListItems(self, itemList):
        """
        Sets the data basis of the list items
        :param itemList: list of tuples, one tuple for each item
        """
        self.__itemList = itemList
        if self.__preSelectedTuple is not None and self.__preSelectedTuple in self.__filteredItemList:
            self.Select(self.__filteredItemList.index(self.__preSelectedTuple))

    def getListItems(self):
        """
        Returns the list of items
        """
        return self.__itemList

    def setPreSelected(self, preSelectedTuple):
        """
        Sets the preselected tuple
        :param preSelectedTuple:
        """
        self.__preSelectedTuple = preSelectedTuple

    def filterAndSort(self, filterValueList, clickedAtCol):
        """
        Filter and sort the list item by the given parameters

        :param filterValueList: list of filter values
        :param clickedAtCol: when clicked on a column -> its column number; else None
        """
        # get the currently selected tuple
        curSelItem = self.getSelectedListItem()

        # when clicked at a column, the sorting order has to change
        if clickedAtCol is not None:

            # when it is the same column
            if self.__sortColumn == clickedAtCol:

                # generate the sorting order
                self.__sortOrder = (self.__sortOrder + 1 + 1) % 3 - 1

            # when another column is clicked
            else:

                # set the sorting order to ascending
                self.__sortColumn = clickedAtCol
                self.__sortOrder = 1

        # if no column is clicked, keep the sorting order (do nothing)
        else:
            pass

        # now, apply the filter
        whereFilterApplies = []

        # iterate through original list
        for listItem in self.__itemList:

            # init logic value
            applies = True

            # iterate through filter values
            for (col, filterValue) in enumerate(filterValueList):
                applies &= filterValue.lower() in self.__filterValueFunc(listItem, col).lower()

            # if still applies
            if applies:
                whereFilterApplies.append(listItem)

        # sort by column
        if self.__sortColumn is None or self.__sortOrder == 0:
            self.__filteredItemList = whereFilterApplies
        else:
            self.__filteredItemList = sorted(whereFilterApplies,
                                             key=lambda item: self.__sortValueFunc(item, self.__sortColumn),
                                             reverse=True if self.__sortOrder == -1 else False)

        self.SetItemCount(len(self.__filteredItemList))
        self.Refresh()

        # assure that the currently selected items stays selected
        if curSelItem is not None and curSelItem in self.__filteredItemList:
            self.Select(self.__filteredItemList.index(curSelItem))
        elif self.getSelectedIndex() != -1:
            self.Select(self.__selIndex, 0)
            self._onItemDeselected(None)

    def getItemCount(self):
        """
        Returns the length of the original item list
        """
        return len(self.__itemList)

    def getFilteredItemCount(self):
        """
        Returns the length of the filtered item list
        """
        return len(self.__filteredItemList)


class FilterListView(wx.ScrolledWindow):
    """
    View with a listCtrl and input controls (Text or ComboBox)
    above each column to filter the shown list
    """
    INPUT_TYPE_TEXT = 0x1
    INPUT_TYPE_COMB = 0x2

    LIST_TYPE_LIST = 0x1
    LIST_TYPE_TREE = 0x2

    FilterListViewSelectionChangedEvent, EVT_FLV_SELCHANGED = wx.lib.newevent.NewCommandEvent()
    FilterListViewItemSelectedEvent, EVT_FLV_ITEM_SELECTED = wx.lib.newevent.NewCommandEvent()
    FilterListViewItemDeselectedEvent, EVT_FLV_ITEM_DESELECTED = wx.lib.newevent.NewCommandEvent()
    ItemDataChanged, EVT_ITEM_DATA_CHANGED = wx.lib.newevent.NewCommandEvent()

    LBL = "Angezeigte Elemente {}/{}"

    def __init__(self, parent, **kwargs):
        """
        The constructor
        :param parent: parent window
        :param columnList: list of column definitions
        :param itemList: list of items
        :param listMinSize: minimum size of the list
        :param listType: which list type should it be, TreeListCtrl or ListCtrl
        :param listStyle: style of the list (has to be virtual!)
        :param itemValueFunc: callable object for getting the value for the desired column
        :param itemChildrenCountFunc: callable object for getting the amount of children for a given item
        :param filterValueFunc: callable object for getting the value which is used for filtering
        :param sortValueFunc: callable object for getting the value which is used for sorting
        :param preSelectedTuple: set a preselected item or None
        """
        wx.ScrolledWindow.__init__(self, parent)

        self.__itemValueFunc = kwargs.get("itemValueFunc")
        self.__itemChildrenCountFunc = kwargs.get("itemChildrenCountFunc")
        self.__filterValueFunc = kwargs.get("filterValueFunc")
        self.__sortValueFunc = kwargs.get("sortValueFunc")
        self.__listType = kwargs.get("listType")
        self.__columnList = kwargs.get("columnList")
        self.__itemList = kwargs.get("itemList")
        self.__filteredItemList = kwargs.get("itemList")
        self.__listMinSize = kwargs.get("listMinSize")
        self.__listStyle = kwargs.get("listStyle")
        self.__preSelectedTuple = kwargs.get("preSelectedTuple")

        if not callable(self.__itemValueFunc):
            raise AttributeError("A callable object for getting the text for a list or tree item is neccessary.")

        if self.__listStyle == self.LIST_TYPE_TREE and not callable(self.__itemChildrenCountFunc):
            raise AttributeError("A callable object for getting the amount of children for a tree item is neccessary.")

        if not callable(self.__filterValueFunc):
            raise AttributeError("A callable object for getting the value for filtering is neccesary.")

        if not callable(self.__sortValueFunc):
            raise AttributeError("A callable object for getting the value for sorting is necceary.")

        self.__inputCtrlList = []

        self._createControls()
        self._setLayout()
        self._bindEvents()

        self.setListItems(self.__itemList)

    def _createControls(self):
        """
        Creates the widgets
        """
        # sizer
        self.szr = wx.GridBagSizer(hgap=5)

        self.szrFltr = wx.GridBagSizer(hgap=1)

        # list
        if self.__listType == self.LIST_TYPE_LIST:
            self.lst = FilterListCtrl(self, columnList=self.__columnList, itemList=self.__itemList,
                                      listMinSize=self.__listMinSize, listStyle=self.__listStyle,
                                      itemValueFunc=self.__itemValueFunc,
                                      filterValueFunc=self.__filterValueFunc,
                                      sortValueFunc=self.__sortValueFunc,
                                      preSelectedTuple=self.__preSelectedTuple)
        else:
            self.lst = FilterTreeListCtrl(self, columnList=self.__columnList, itemList=self.__itemList,
                                          listMinSize=self.__listMinSize, listStyle=self.__listStyle,
                                          itemValueFunc=self.__itemValueFunc,
                                          itemChildrenCountFunc=self.__itemChildrenCountFunc,
                                          filterValueFunc=self.__filterValueFunc,
                                          sortValueFunc=self.__sortValueFunc,
                                          preSelectedTuple=self.__preSelectedTuple)

        # self.lst.SetBackgroundColour(String.COL_THEME_2)

        # input
        for (i, (colHeader, initWidth, inputType, inputStyle)) in enumerate(self.__columnList):

            # create control
            if inputType == self.INPUT_TYPE_TEXT:
                ctrl = wx.TextCtrl(self, size=(self.lst.GetColumnWidth(i), 24), style=inputStyle)
                # listctrl.SetBackgroundColour(String.COL_THEME_4)
                # listctrl.SetForegroundColour(String.COL_THEME_2)
            elif inputType == self.INPUT_TYPE_COMB:
                ctrl = wx.ComboBox(self, size=(self.lst.GetColumnWidth(i), 24), style=inputStyle)
                # listctrl.SetBackgroundColour(String.COL_THEME_4)
                # listctrl.SetForegroundColour(String.COL_THEME_2)
            else:
                ctrl = None

            self.__inputCtrlList.append(ctrl)

        # label
        self.lbl = wx.StaticText(self, label=self.LBL.format(self.lst.getFilteredItemCount(), self.lst.getItemCount()))
        # self.lbl.SetForegroundColour(String.COL_THEME_3)

    def _setLayout(self):
        """
        Sets the layout of the widgets
        """
        # filter sizer
        for i, control in enumerate(self.__inputCtrlList):
            self.szrFltr.Add(control, (0, i), (1, 1))

        # main sizer
        self.szr.Add(self.szrFltr, (0, 0), (1, 1), wx.EXPAND)
        self.szr.Add(self.lst, (1, 0), (1, 1), wx.EXPAND)
        self.szr.Add(self.lbl, (2, 0), (1, 1), wx.ALIGN_LEFT | wx.TOP, 3)
        self.szr.AddGrowableCol(0)
        self.szr.AddGrowableRow(1)

        self.SetSizer(self.szr)

        self.Fit()

    def _bindEvents(self):
        """
        Associates events with actions
        """
        # filter actions
        for ctrl in self.__inputCtrlList:
            if isinstance(ctrl, wx.ComboBox):
                ctrl.Bind(wx.EVT_TEXT_ENTER, self._onFilterChanged)
                ctrl.Bind(wx.EVT_COMBOBOX, self._onFilterChanged)
            elif isinstance(ctrl, wx.TextCtrl):
                ctrl.Bind(wx.EVT_TEXT_ENTER, self._onFilterChanged)

        # column resize
        self.lst.Bind(wx.EVT_LIST_COL_END_DRAG, self._onListColumnResize)

        # column click
        self.lst.Bind(wx.EVT_LIST_COL_CLICK, self._onListColumnClick)

        # panel resize
        self.Bind(wx.EVT_SIZE, self._onPanelResize)

        # item selection
        self.lst.Bind(self.EVT_FLV_ITEM_SELECTED, self._onListItemSelected)
        self.lst.Bind(self.EVT_FLV_ITEM_DESELECTED, self._onListItemDeselected)
        self.lst.Bind(self.EVT_FLV_SELCHANGED, self._onTreeItemSelectionChanged)

    # --- END INIT ---

    def _onListItemSelected(self, event):
        wx.PostEvent(self, FilterListView.FilterListViewItemSelectedEvent(self.GetId()))

    def _onListItemDeselected(self, event):
        wx.PostEvent(self, FilterListView.FilterListViewItemDeselectedEvent(self.GetId()))

    def _onTreeItemSelectionChanged(self, event):
        wx.PostEvent(self, FilterListView.FilterListViewSelectionChangedEvent(self.GetId()))

    # --- EVENTS ---

    def _onPanelResize(self, event):
        """
        Triggered when the panel is resized
        :param event:
        """
        event.Skip()
        wx.CallAfter(self._arrangeFilterControls)

    def _onListColumnResize(self, event):
        """
        Triggered when a column is resized
        :param event:
        """
        event.Skip()
        self.lst.resizeLastColumn(self.__columnList[-1][1])
        # self.SetSize((sum([self.lst.GetColumnWidth(i) for i in xrange(self.lst.GetColumnCount())]), self.GetSize().y))
        wx.CallAfter(self._arrangeFilterControls)

    def _onListColumnClick(self, event):
        """
        Triggered when the column is clicked
        :param event: a ListEvent
        """
        self.lst.filterAndSort([ctrl.GetValue() for ctrl in self.__inputCtrlList],
                               event.GetColumn())

    def _onFilterChanged(self, event):
        """
        Triggered when enter is pressed in text or combo box control or a value is selected in combo box
        :param event:
        """
        self.lst.filterAndSort([ctrl.GetValue() for ctrl in self.__inputCtrlList],
                               None)
        self._setItemsLabel()

    def _setItemsLabel(self):
        """
        Sets label for the current filtered list
        """
        self.lbl.SetLabel(self.LBL.format(self.lst.getFilteredItemCount(), self.lst.getItemCount()))

    # --- END EVENTS ---

    def _arrangeFilterControls(self):
        """
        Set the size and position of the widgets
        """
        # iterate thorugh columns of test task list
        for col in range(self.lst.GetColumnCount()):

            # get the associated widget for this column
            curWidget = self.__inputCtrlList[col]

            # if it is not the first column...
            if col > 0:
                # get the predecessor widget
                predWidget = self.__inputCtrlList[col - 1]

                # set the position
                posX = predWidget.GetPosition().x + predWidget.GetSize().x + self.szrFltr.GetHGap()
                posY = curWidget.GetPosition().y
                curWidget.SetPosition((posX, posY))

            # set the size
            sizeX = self.lst.GetColumnWidth(col)
            sizeY = curWidget.GetSize().y
            curWidget.SetSize((sizeX, sizeY))

            # make visible
            curWidget.Refresh()

    def setComboChoices(self, columnIndex, stringList):
        """
        Sets the choices of a combo box
        :param columnIndex: the index of the column starting at 0
        :param stringList: list of strings the combobox have choices of
        """
        # check index
        if columnIndex >= self.lst.GetColumnCount():
            raise AttributeError("List has only {} columns, can not access index {}"
                                 .format(self.lst.GetColumnCount(), columnIndex))

        if not isinstance(self.__inputCtrlList[columnIndex], wx.ComboBox):
            raise AttributeError("Filter input control for column {} is not a ComboBox, but a {}"
                                 .format(columnIndex, type(self.__inputCtrlList[columnIndex])))

        self.__inputCtrlList[columnIndex].SetItems(stringList)

    def setListItems(self, itemList):
        """
        Sets the data basis of the list items
        :param itemList: list of tuples, one tuple for each item
        """
        self.__itemList = itemList
        self.lst.setListItems(itemList)
        self.lst.filterAndSort([ctrl.GetValue() for ctrl in self.__inputCtrlList], None)
        self._arrangeFilterControls()
        self._setItemsLabel()

        # set combo box values
        for (col, ctrl) in enumerate(self.__inputCtrlList):
            if isinstance(ctrl, wx.ComboBox):
                oldValue = ctrl.GetValue()
                values = list(set([self.__sortValueFunc(item, col) for item in self.lst.getListItems()]))
                values.sort()
                self.setComboChoices(col, values)
                ctrl.SetValue(oldValue)

    def setPreSelected(self, preSelectedTuple):
        """
        Sets the preselected tuple
        :param preSelectedTuple:
        """
        self.lst.setPreSelected(preSelectedTuple)

    def getItemCount(self):
        """
        Returns the length of the original item list
        """
        return self.lst.getItemCount()

    def getSelectedListItem(self):
        """
        Returns the values for the selected row
        """
        return self.lst.getSelectedListItem()

    def getSelectedListItems(self):
        """
        Returns the values for the selected row
        """
        return self.lst.getSelectedListItems()

    def getSelectedIndex(self):
        """
        Returns the selected index
        """
        return self.lst.getSelectedIndex()

    def getListItems(self):
        return self.lst.getListItems()


def getListItemColumnValues(item, column):
    return item[column]


def getTreeItemColumnValues(item, index, column):
    if len(index) == 0:
        return ""
    elif len(index) == 1:
        return str(item[column])
    elif len(index) == 2:
        if column == 0:
            return str(item[1][index[1]])

    return ""


def getTreeItemChildrenCount(item, index):
    if len(index) == 1:
        return len(item[1])
    else:
        return 0


def getFilterValue(item, col):
    str_ = str(item[col])

    if col == 0:
        str_ += "".join([str(i) for i in item[1]])

    return str_


def getSortValue(item, col):
    return str(item[col])


if __name__ == "__main__":
    # App initialisieren
    app = wx.App(False)

    # Mainframe bauen
    frame = wx.Frame(None)

    columnList = [("header1", 100, FilterListView.INPUT_TYPE_TEXT, wx.TE_PROCESS_ENTER),
                  ("header2", 200, FilterListView.INPUT_TYPE_COMB, wx.TE_PROCESS_ENTER),
                  ("header3", 200, FilterListView.INPUT_TYPE_COMB, wx.TE_PROCESS_ENTER)]
    itemList1 = [("voll", "leer", "versteckt"),
                 ("halbvoll", "voll", "ebenfalls versteckt"),
                 ("leer", "halbvoll", "nix")]
    itemList2 = [("voll", [1, 2, 3, 4], "versteckt"),
                 ("halbvoll", [5, 6, 7, 8], "ebenfalls versteckt"),
                 ("leer", [9, 10, 11], "nix")]

    # flv = FilterListView(frame,
    #                      columnList=columnList,
    #                      itemList=itemList1,
    #                      minSize=(400, 100),
    #                      listStyle=wx.LC_REPORT | wx.LC_SINGLE_SEL | wx.LC_HRULES | wx.LC_VIRTUAL,
    #                      listType=FilterListView.LIST_TYPE_LIST,
    #                      itemValueFunc=getListItemColumnValues,
    #                      filterValueFunc=getListItemColumnValues,
    #                      sortValueFunc=getListItemColumnValues,
    #                      preSelectedTuple=itemList1[1])
    flv = FilterListView(frame,
                         columnList=columnList,
                         itemList=itemList2,
                         minSize=(400, 100),
                         listStyle=wx.TR_HAS_BUTTONS | wx.TR_HIDE_ROOT | wx.TR_FULL_ROW_HIGHLIGHT | wx.TR_SINGLE,
                         listType=FilterListView.LIST_TYPE_TREE,
                         itemValueFunc=getTreeItemColumnValues,
                         itemChildrenCountFunc=getTreeItemChildrenCount,
                         filterValueFunc=getFilterValue,
                         sortValueFunc=getSortValue,
                         preSelectedTuple=itemList2[1])

    # ... und anzeigen
    frame.Show()
    app.MainLoop()
