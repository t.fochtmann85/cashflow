#  -*- coding: cp1252 german encoding -*-
__author__ = "TobiasFochtmann"

import logging

import wx
import wx.richtext


class GuiLogHandler(logging.Handler):
    """
    Custom logger which forwards the message to the logging window frame
    """

    def __init__(self, textCtrl):
        """
        Constructor
        :param textCtrl: the text control the messages get forwarded to
        :type textCtrl: wx.richtext.RichTextCtrl
        """
        logging.Handler.__init__(self, logging.INFO)
        self.__textCtrl = textCtrl

        self.levelColour = {logging.DEBUG: wx.ColourDatabase().Find("DARK GREY"),
                            logging.INFO: wx.ColourDatabase().Find("BLACK"),
                            logging.WARNING: wx.ColourDatabase().Find("ORANGE"),
                            logging.ERROR: wx.ColourDatabase().Find("RED"),
                            logging.CRITICAL: wx.ColourDatabase().Find("PURPLE"),
                            }

    def emit(self, record):
        """
        Overwrite super class method
        :param record: container with all given information
        :type record: {logging.LogRecord}
        """
        self.__textCtrl.BeginTextColour(self.levelColour.get(record.levelno))
        self.__textCtrl.WriteText("[{}] {}".format(record.levelname[0], record.message))
        self.__textCtrl.EndTextColour()
        self.__textCtrl.Newline()
