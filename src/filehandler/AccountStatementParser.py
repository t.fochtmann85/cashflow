#  -*- coding: cp1252 german encoding -*-
import copy
import json
import logging
import os.path
import re
import shutil
from datetime import datetime
from pathlib import Path

import wx

import CommonFunctions
from db.DatabaseManager import DatabaseManager as DBM
from db.DatabaseService import CategoryRuleService, SplitRuleService, CategoryService, WalletService, TransactionService
from db.DbModelClasses import Transaction
from filehandler import MarkupHelper
from gui.dlg.NumCtrlDialog import NumCtrlDialog
from resources import String

__author__ = "Tobias Fochtmann"


class FormatInfo:
    """
    Class holding information about the formatting info of a wallet
    """

    MANDATORY_FORMAT_KEYS = {"row_skipFirst", "row_skipLast", "separator", "col_date", "col_expe", "col_earn",
                             "col_srcDest"}

    def __init__(self, row_skipFirst, row_skipLast, separator, col_date, col_expe, col_earn, col_srcDest,
                 col_purpose=None, col_iban=None, col_bic=None, inverted=False):
        """
        Constructor
        :param row_skipFirst: number of lines to skip before parsing the first statement
        :type row_skipFirst: int
        :param row_skipLast: number of lines to ignore at the end
        :type row_skipLast: int
        :param separator: column separator
        :type separator: basestring
        :param col_date: column number holding the date (starting at 0)
        :type col_date: int
        :param col_expe: column number holding the expense
        :type col_expe: int
        :param col_earn: column number holding the earning
        :type col_earn: int
        :param col_srcDest: columng number holding source/destination of the transaction
        :type col_srcDest: int
        :param col_purpose: column number holding the transaction purpose
        :type col_purpose: int
        :param col_iban: column number holding the IBAN
        :type col_iban: int
        :param col_bic: column number holding the BIC
        :type col_bic: int
        """
        for arg in [row_skipFirst, row_skipLast, col_date, col_expe, col_earn, col_srcDest]:
            assert isinstance(arg, (float, int)), \
                "Argument needs to be of type �int�, but is: {} of type {}.".format(arg, type(arg))
            assert arg >= 0, "Argument needs to be greater or equal to zero, but is {}".format(arg)
        assert isinstance(separator, str), \
            "Argument �separator� needs to be of type �basestring�, but is: {} of type {}.".format(
                separator, type(separator))
        for arg in [col_purpose, col_iban, col_bic]:
            if isinstance(arg, (float, int)):
                assert arg >= 0, "Argument needs to be greater or equal to zero, but is {}".format(arg)

        self.skipFirstRows = row_skipFirst
        self.skipLastRows = row_skipLast
        self.separator = separator
        if self.separator == "\\t":
            self.separator = "\t"
        self.colDate = col_date
        self.colExpense = col_expe
        self.colEarning = col_earn
        self.colSrcDest = col_srcDest
        self.colPurpose = col_purpose
        self.colIban = col_iban
        self.colBic = col_bic
        self.inverted = inverted

    @staticmethod
    def read(formatInfo):
        """
        Reading format info
        :param formatInfo: dictionary containing formatting info
        :type formatInfo: dict
        :return: an format info instance
        :rtype: FormatInfo
        """
        assert isinstance(formatInfo, dict), \
            "Argument �formatInfo� needs to be of type �dict�, but is: {} of type {}.".format(
                formatInfo,
                type(formatInfo)
            )
        assert FormatInfo.MANDATORY_FORMAT_KEYS.issubset(set(formatInfo.keys())), \
            "Not all necessary keys present in format info. Missing: {}".format(
                FormatInfo.MANDATORY_FORMAT_KEYS - set(formatInfo.keys()))

        return FormatInfo(**formatInfo)


def parseAccountStatement(filePath, formatInfo, walletId):
    """
    Constructor
    :param filePath: absolute file path to account statement
    :type filePath: str
    :param formatInfo: dictionary containing formatting info
    :type formatInfo: dict
    :param walletId: wallet id of the account statement
    :type walletId: long
    """
    assert isinstance(filePath, str), "Argument �filePath� needs to be of type �string�"
    assert isinstance(formatInfo, dict), "Argument �formatInfo� needs to be of type �dict�"
    assert isinstance(walletId, int), "Argument �wallet� needs to be of type �int� or �long�"
    if not os.path.isfile(filePath):
        raise WindowsError("Account statement file needs to be present at os")

    fmt = FormatInfo.read(formatInfo)

    # init empty list
    transactionList = list()
    skippedLines = list()

    # iterate through lines
    lines = MarkupHelper.getCsvFileContent(filePath, fmt.separator)
    for rowId, lineCells in enumerate(lines):
        # lines of interest
        if fmt.skipFirstRows <= rowId < (len(lines) - fmt.skipLastRows):
            __processLine(rowId, lineCells, walletId, transactionList, fmt)
        elif len(lineCells) > 0:
            skippedLines.append("{}: {} ...".format(rowId, lineCells[0]))
        else:
            skippedLines.append("{}: empty".format(rowId))

    __applyCategoryRules(transactionList, CategoryRuleService.get())

    logging.info("Skipped lines:\n{}".format("\n".join(skippedLines)))

    return transactionList


def __processLine(rowId, lineCells, walletId, transactionList, fmt):
    """
    Process a single line
    :param rowId: current row number
    :type rowId: int
    :param lineCells: current line
    :type lineCells: list
    """
    # create a new transaction dummy
    transaction = Transaction()

    try:
        transaction.date = datetime.strptime(lineCells[fmt.colDate], String.FMT_DATE)
    except:
        try:
            transaction.date = datetime.strptime(lineCells[fmt.colDate], String.FMT_DATE_2)
        except:
            try:
                transaction.date = datetime.strptime(lineCells[fmt.colDate], String.FMT_DATE_3)
            except:
                transaction.date = datetime.strptime(lineCells[fmt.colDate], String.FMT_DATE_4)

    transaction.amount = CommonFunctions.fromGermanDecimalToFloat(
        lineCells[fmt.colExpense] or lineCells[fmt.colEarning])

    if fmt.inverted:
        transaction.amount *= -1

    transaction.wallet_id = walletId
    transaction.src_dest = lineCells[fmt.colSrcDest]
    transaction.category = CategoryService.get_by_name("Unbekannt")
    if fmt.colPurpose is not None:
        transaction.purpose = lineCells[fmt.colPurpose]
    if fmt.colIban is not None:
        transaction.iban = lineCells[fmt.colIban]
    if fmt.colBic is not None:
        transaction.bic = lineCells[fmt.colBic]

    if __applySplitRules(transaction, transactionList, SplitRuleService.get()):
        logging.debug("Rule split applied at row {}".format(rowId))
    else:
        transactionList.append(transaction)


def __applySplitRules(transaction, transactionList, splitRules):
    """
    Applies split rules to each line
    :param transaction: the current base transaction of this line
    :type transaction: Transaction
    :return: True if split rule was applied
    :rtype: bool
    """
    # init as no rule applied
    splitRuleApplied = False

    # try to find automatic splits
    for splitRule in splitRules:
        # get values of this transaction
        condFieldValue = getattr(transaction, splitRule.cond_field)
        parseFieldValue = getattr(transaction, splitRule.parse_field)

        # check each rule for matching conditions
        if condFieldValue is not None and splitRule.contains in condFieldValue:
            for splitMatch in re.finditer(splitRule.iter_regex, parseFieldValue):
                splitRuleApplied = True
                __processSplitMatch(transaction, splitMatch, transactionList)

            # end loop after first split match
            if splitRuleApplied:
                break

    return splitRuleApplied


def __processSplitMatch(transaction, splitMatch, transactionList):
    """
    Processing a split match
    :param transaction: the current base transaction of this line
    :type transaction: db.DbModelClasses.Transaction
    :param splitMatch: the regex match
    """
    # get a copy of the base transaction
    transactionCopy = copy.deepcopy(transaction)

    # get groups of match
    splitMatchDict = splitMatch.groupdict()
    newPurpose = splitMatchDict.get("purpose")
    amount = CommonFunctions.fromGermanDecimalToFloat(splitMatchDict.get("amount"))

    # handle none amount (if missing in regex)
    if amount is None:
        dlg = NumCtrlDialog(None,
                            String.DLG_MESSAGE_SPLIT_ENTRY.format(newPurpose,
                                                                  transaction.amount,
                                                                  transaction.purpose),
                            0)
        if dlg.ShowModal() == wx.ID_OK:
            amount = dlg.getValue()

    if transaction.amount < 0 and amount > 0:
        amount *= -1.0

    transactionCopy.purpose = newPurpose
    transactionCopy.amount = amount
    transactionCopy.comment = "automatic split from {}".format(transaction.amount)
    transactionList.append(transactionCopy)


def __applyCategoryRules(transactionList, categoryRules):
    """
    Applies category rules
    """
    # for each transaction
    for transaction in transactionList:
        # check if there is a category rule
        for categoryRule in categoryRules:
            condFieldValue = getattr(transaction, categoryRule.cond_field)
            if condFieldValue is not None and categoryRule.contains in condFieldValue:
                transaction.category = categoryRule.category
                logging.debug("Category rule applied for {}".format(condFieldValue))
                break
        transaction.updateHash()


def auto_discover() -> None:
    path_data = Path(__file__).parent.parent.parent / "data"

    # empty folder
    for item in (path_data / "_done").iterdir():
        item.unlink()

    for wallet_type in {"Miscellaneous", "Giro", "Credit Card"}:
        for wallet in WalletService.get_items_of_type(wallet_type):
            if wallet.as_format_info:
                format_info = json.loads(wallet.as_format_info)
                if "file_re" in format_info:
                    p = format_info.pop("file_re")
                    pattern = re.compile(p, re.IGNORECASE)

                    for item in path_data.iterdir():
                        if pattern.match(item.name) is not None:
                            logging.info(f"Found account statement at {item!r} for {wallet.name!r}")

                            # process
                            for transaction in parseAccountStatement(str(item), format_info, wallet.id):
                                if TransactionService.existsWithHash(transaction):
                                    logging.warning(
                                        "Transaction with same details already exists and will be skipped:\n" +
                                        "{}, {}, {}, {}".format(transaction.date.strftime(String.FMT_DATE),
                                                                transaction.amount,
                                                                transaction.src_dest,
                                                                transaction.purpose))
                                else:
                                    DBM().insert(transaction)

                            # move file to folder '_done'
                            shutil.move(item, path_data / "_done" / item.name)
