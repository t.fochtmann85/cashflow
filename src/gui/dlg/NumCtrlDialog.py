#  -*- coding: cp1252 german encoding -*-
import wx
import wx.lib.masked.numctrl

from gui.bmp import images

__author__ = "Tobias Fochtmann"


class NumCtrlDialog(wx.Dialog):
    """
    Class for showing a floating point number control
    """
    PADDING = 10

    def __init__(self, parent, caption, additionalStyle=wx.CLOSE_BOX):
        """
        Constructor
        :param parent: window parent
        :param caption: title label
        :param additionalStyle: message for input
        """
        wx.Dialog.__init__(self, parent, title="Number entry",
                           style=wx.CAPTION | wx.STAY_ON_TOP | wx.SYSTEM_MENU | additionalStyle)
        self.SetIcon(images.MONEYBAG_EURO.GetIcon())

        self.SetMinSize((300, 50))

        self.lbl = wx.StaticText(self, label=caption)
        self.num = wx.lib.masked.numctrl.NumCtrl(self, integerWidth=6, fractionWidth=2, signedForegroundColour=wx.RED)
        self.btn = wx.BitmapButton(self, bitmap=images.APPLY.GetBitmap())
        self.btn.SetMinSize((self.num.GetSize().x, self.btn.GetSize().y))

        self.szr = wx.GridBagSizer(hgap=self.PADDING)
        self.szr.Add(self.lbl, (0, 0), (1, 2), wx.LEFT | wx.TOP | wx.RIGHT, self.PADDING)
        self.szr.Add(self.num, (1, 0), (1, 1), wx.LEFT | wx.BOTTOM | wx.ALIGN_CENTER_VERTICAL, self.PADDING)
        self.szr.Add(self.btn, (1, 1), (1, 1), wx.RIGHT | wx.BOTTOM, self.PADDING)
        self.SetSizerAndFit(self.szr)

        self.CenterOnParent()

        self.btn.Bind(wx.EVT_BUTTON, self.__onApply)

    def __onApply(self, evt):
        self.EndModal(wx.ID_OK)

    def setValue(self, value):
        self.num.SetValue(value)

    def getValue(self):
        return self.num.GetValue()


if __name__ == "__main__":
    app = wx.App()
    mainFrame = NumCtrlDialog(None, "losdfgudfhgisfdghuazsgfsduzasddddddddddddddddddddgf asdasd")
    mainFrame.Show()
    app.MainLoop()
