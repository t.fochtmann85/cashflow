#  -*- coding: cp1252 german encoding -*-

import logging

import wx
import wx.adv
from wx.lib.masked import NumCtrl

import CommonFunctions
from db.DatabaseManager import DatabaseManager as DBM
from db.DatabaseService import WalletService, CategoryService, TransactionService
from db.DbModelClasses import Transaction
from gui.bmp import images
from resources import String

__author__ = "Tobias Fochtmann"


class TransactionEditDialog(wx.Dialog):
    """
    Dialog for editing transactions
    """
    PADDING = 10

    def __init__(self, parent, transaction=None):
        """
        Constructor
        :param parent: parent window
        :param transaction: transaction data
        :type transaction: Transaction
        """
        wx.Dialog.__init__(self, parent, style=wx.STAY_ON_TOP | wx.CAPTION | wx.SYSTEM_MENU)

        if transaction is None:
            self.SetTitle("Add a transaction")
        else:
            self.SetTitle("Edit transaction...")
            assert isinstance(transaction, Transaction), \
                "Argument ´transaction´ needs to be of type ´DbModelClasses.Transaction´"

        self.SetIcon(images.WALLET_CLOSED_EDIT.GetIcon())

        self.transaction = transaction

        self._createControls()
        self._setToolTips()
        self._setLayout()
        self._bindEvents()
        self._initLoad()

        self.CenterOnParent()

    def _createControls(self):
        """
        Creates all used widgets
        """
        # sizers
        self.szr_main = wx.GridBagSizer(hgap=self.PADDING, vgap=self.PADDING)

        # labels
        self.lbl_date = wx.StaticText(self, label=String.LBL_COL_DATE)
        self.lbl_amount = wx.StaticText(self, label=String.LBL_COL_AMOUNT)
        self.lbl_srcdest = wx.StaticText(self, label=String.LBL_COL_SRCDEST)
        self.lbl_iban = wx.StaticText(self, label=String.LBL_COL_IBAN)
        self.lbl_bic = wx.StaticText(self, label=String.LBL_COL_BIC)
        self.lbl_purpose = wx.StaticText(self, label=String.LBL_COL_PURPOSE)
        self.lbl_wallet = wx.StaticText(self, label=String.LBL_COL_WALLET)
        self.lbl_category = wx.StaticText(self, label=String.LBL_COL_CAT)
        self.lbl_factor = wx.StaticText(self, label=String.LBL_COL_FACTOR)
        self.lbl_comment = wx.StaticText(self, label=String.LBL_COL_COMMENT)

        # text inputs
        self.txt_date = wx.adv.DatePickerCtrl(self, style=wx.adv.DP_DROPDOWN)
        self.txt_amount = NumCtrl(self, integerWidth=6, fractionWidth=2, autoSize=False, signedForegroundColour=wx.RED)
        self.txt_amount.SetMinSize(self.txt_date.GetSize())
        self.txt_srcdest = wx.TextCtrl(self, size=(300, 24))
        self.txt_iban = wx.TextCtrl(self)
        self.txt_bic = wx.TextCtrl(self)
        self.txt_purpose = wx.TextCtrl(self, size=(300, 100), style=wx.TE_MULTILINE)
        self.cmb_wallet = wx.ComboBox(self, style=wx.CB_READONLY, choices=WalletService.getNames())
        self.cmb_category = wx.ComboBox(self, style=wx.CB_READONLY, choices=CategoryService.getLeafNames())
        self.txt_factor = NumCtrl(self, integerWidth=1, fractionWidth=2, autoSize=False, signedForegroundColour=wx.RED,
                                  min=.0, max=1.)
        self.txt_comment = wx.TextCtrl(self, size=(300, 100), style=wx.TE_MULTILINE)

        # buttons
        self.btn_apply = wx.BitmapButton(self, bitmap=images.APPLY.GetBitmap())
        self.btn_discard = wx.BitmapButton(self, bitmap=images.DISCARD.GetBitmap())

    def _setToolTips(self):
        """
        Set tool tips
        """
        self.btn_apply.SetToolTip("Write changes to database and close the dialog")
        self.btn_apply.SetToolTip("Discard changes and close the dialog")

    def _setLayout(self):
        """
        Sets the layout of the widgets
        """
        line = 0
        self.szr_main.Add(self.lbl_date, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.TOP, self.PADDING)
        self.szr_main.Add(self.txt_date, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.TOP | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_amount, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.txt_amount, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_srcdest, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.txt_srcdest, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_iban, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.txt_iban, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_bic, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.txt_bic, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_purpose, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.txt_purpose, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_wallet, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.cmb_wallet, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_category, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.cmb_category, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_factor, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.txt_factor, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.lbl_comment, (line, 0), (1, 1), wx.ALIGN_CENTER_VERTICAL | wx.LEFT, self.PADDING)
        self.szr_main.Add(self.txt_comment, (line, 1), (1, 1), wx.EXPAND | wx.RIGHT, self.PADDING)
        line += 1
        self.szr_main.Add(self.btn_apply, (line, 0), (1, 1), wx.ALIGN_RIGHT | wx.BOTTOM, self.PADDING)
        self.szr_main.Add(self.btn_discard, (line, 1), (1, 1), wx.ALIGN_LEFT | wx.BOTTOM, self.PADDING)

        self.SetSizer(self.szr_main)
        self.Fit()

    def _bindEvents(self):
        """
        Binds events to actions
        """
        self.btn_apply.Bind(wx.EVT_BUTTON, self.__onApply)
        self.btn_discard.Bind(wx.EVT_BUTTON, self.__onDiscard)

    def _initLoad(self):
        """
        Load initial values, set up initial states
        """
        # new transaction
        if self.transaction is None:
            self.setWallet("Bargeld")
            self.setCategory("Unbekannt")
            self.setFactor(1.)
        # editing transaction
        else:
            self.setDate(self.transaction.date)
            self.setAmount(self.transaction.amount)
            self.setSrcDest(self.transaction.src_dest or "")
            self.setIban(self.transaction.iban or "")
            self.setBic(self.transaction.bic or "")
            self.setPurpose(self.transaction.purpose or "")
            self.setWallet(self.transaction.wallet.name)
            self.setCategory(self.transaction.category.name)
            self.setComment(self.transaction.comment or "")
            self.setFactor(self.transaction.factor)

    def setFactor(self, value):
        self.txt_factor.SetValue(value)

    def setAmount(self, value):
        """
        Set transaction amount
        :param value: sets the transaction value
        :type value: float
        """
        self.txt_amount.SetValue(value)

    def setWallet(self, walletDesc):
        """
        Sets the wallet
        :param walletDesc: sets the wallets name in the dialog
        :type walletDesc: str
        """
        self.cmb_wallet.SetValue(walletDesc)

    def setDate(self, date):
        """
        Sets the date
        :param date: date to set
        :type date: datetime.datetime
        """
        self.txt_date.SetValue(CommonFunctions.datetimeToWxDateTime(date))

    def setSrcDest(self, srcDest):
        """
        Sets the source/destination
        :param srcDest: source/destination to set
        :type srcDest: str
        """
        self.txt_srcdest.SetValue(srcDest)

    def setIban(self, iban):
        """
        Sets the IBAN
        :param iban: IBAN to set
        :type iban: str
        """
        self.txt_iban.SetValue(iban)

    def setBic(self, bic):
        """
        Sets the BIC
        :param bic: BIC to set
        :type bic: str
        """
        self.txt_bic.SetValue(bic)

    def setPurpose(self, purpose):
        """
        Sets the purpose
        :param purpose: purpose to set
        :type purpose: basestring
        """
        self.txt_purpose.SetValue(purpose)

    def setCategory(self, category):
        """
        Sets the category
        :param category: category to set
        :type category: basestring
        """
        self.cmb_category.SetValue(category)

    def setComment(self, comment):
        """
        Sets the comment
        :param comment: comment to set
        :type comment: basestring
        """
        self.txt_comment.SetValue(comment or "")

    def _checkValues(self):
        """
        Validates user data
        :return: True if all necessary data is set
        :rtype: bool
        """
        if not self.cmb_wallet.GetValue():
            logging.warning("No wallet selected")
            return False

        if not self.cmb_category.GetValue():
            logging.warning("No valid category selected")
            return False

        if self.txt_amount.GetValue() == 0.0:
            logging.warning("Please set a value not equal to 0")
            return False

        return True

    def _getUpdatedTransactionFromUI(self):
        """
        :return: transaction with updated user values
        :rtype: Transaction
        """
        if self.transaction is None:
            transaction = Transaction()
        else:
            transaction = self.transaction

        transaction.date = CommonFunctions.wxDateTimeToDatetime(self.txt_date.GetValue())
        transaction.amount = self.txt_amount.GetValue()
        transaction.src_dest = self.txt_srcdest.GetValue()
        transaction.purpose = self.txt_purpose.GetValue()
        transaction.iban = self.txt_iban.GetValue()
        transaction.bic = self.txt_bic.GetValue()
        transaction.factor = self.txt_factor.GetValue()
        transaction.comment = self.txt_comment.GetValue()
        transaction.wallet = WalletService.getByName(self.cmb_wallet.GetValue())
        transaction.category = CategoryService.get_by_name(self.cmb_category.GetValue())
        transaction.updateHash()

        return transaction

    # ----- EVENTS -----

    def __onApply(self, evt):
        """
        Triggered when button apply is pressed
        :param evt:
        """
        if self._checkValues():
            transaction = self._getUpdatedTransactionFromUI()

            # INSERT
            if self.transaction is None:
                # if hash already exists
                if TransactionService.existsWithHash(transaction):
                    logging.warning("Transaction with same hash already exists. Please alter attribute values")
                else:
                    DBM().insert(transaction)
                    self.EndModal(wx.ID_OK)
            # UPDATE
            else:
                DBM().update(transaction)
                self.EndModal(wx.ID_OK)

    def __onDiscard(self, evt):
        """
        Triggerd when button discard is pressed
        :param evt:
        """
        self.EndModal(wx.ID_CANCEL)


if __name__ == "__main__":
    app = wx.App()
    mainFrame = TransactionEditDialog(None)
    mainFrame.Show()
    app.MainLoop()
